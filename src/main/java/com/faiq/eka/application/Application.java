package com.faiq.eka.application;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.faiq.eka.configuration.EnableLocalConfiguration;



@EnableLocalConfiguration
@EntityScan({"com.faiq.eka.persistence"})
@EnableJpaRepositories({"com.faiq.eka.persistence"})
@ComponentScan("com.faiq.eka")
@ComponentScan({"com.faiq.eka","com.faiq.eka.controller"})
@SpringBootApplication
@MapperScan("com.faiq.eka.persistence.mybatis")
public class Application extends SpringBootServletInitializer {
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
