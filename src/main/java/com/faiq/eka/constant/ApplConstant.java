package com.faiq.eka.constant;

public abstract class ApplConstant {
	
	public static final String URL_DEFAULT				= "/";
	public static final String URL_APP					= "/stokis";
	public static final String URL_DIRECTORY			= "${dir.pdf}";
	public static final String URL_LOGIN				= "/login";

	public static final String TYPE_XLSX				= "XLSX";
	public static final String TYPE_XLS					= "XLS";
	public static final String TYPE_PDF					= "PDF";
	public static final String TYPE_CSV					= "CSV";

	public static final String EXT_XLSX					= ".xlsx";
	public static final String EXT_XLS					= ".xls";
	public static final String EXT_PDF					= ".pdf";
	public static final String EXT_CSV					= ".csv";
	
	public static final String URL_PENERIMA						= URL_APP+"/penerima";
	public static final String URL_URL_PENERIMA_DETAIL			= URL_PENERIMA+"/detail";
	public static final String URL_PENERIMA_DATATABLE			= URL_PENERIMA+"/datatable";
	public static final String URL_BARANG_PENERIMA_DATATABLE	= URL_PENERIMA_DATATABLE+"/barang";
	
	public static final String URL_MASTER_BD				= URL_APP+"/masterBd";
	public static final String URL_MASTER_BD_SAVE			= URL_MASTER_BD+"/save";
	public static final String URL_MASTER_BD_SAVE_DETAIL	= URL_MASTER_BD+"/saveDetail";
	public static final String URL_MASTER_BD_EDIT			= URL_MASTER_BD+"/edit";
	public static final String URL_MASTER_BD_DELETE			= URL_MASTER_BD+"/delete";
	public static final String URL_DETAIL_MASTER_BD_DELETE	= URL_MASTER_BD+"/deleteDetailMasterBd";
	public static final String URL_MASTER_BD_DATATABLE		= URL_MASTER_BD+"/datatable";
	public static final String URL_DETAIL_MASTER_BD_DATATABLE			= URL_MASTER_BD+"/datatableDetailMasterBd";
	public static final String URL_MASTER_BD_LIST_BARANG_DATATABLE		= URL_MASTER_BD+"/datatableListBarangMasterBd";
	public static final String URL_HISTORY_BARANG_DETAIL_MASTER_BD_DATATABLE			= URL_MASTER_BD+"/datatableHistoryBarrangDetailMasterBd";
	
	public static final String URL_MASTER_BARANG			= URL_APP+"/masterBarang";
	public static final String URL_MASTER_BARANG_SAVE		= URL_MASTER_BARANG+"/save";
	public static final String URL_MASTER_BARANG_CARI		= URL_MASTER_BARANG+"/cari";
	public static final String URL_MASTER_BARANG_CARI_TRX	= URL_MASTER_BARANG_CARI+"/trx";
	public static final String URL_MASTER_BARANG_EDIT		= URL_MASTER_BARANG+"/edit";
	public static final String URL_MASTER_BARANG_DELETE		= URL_MASTER_BARANG+"/delete";
	public static final String URL_MASTER_BARANG_DATATABLE	= URL_MASTER_BARANG+"/datatable";
	public static final String URL_TAMBAH_STOK_BARANG		= URL_MASTER_BARANG+"/tambahStok";
	public static final String URL_KURANGI_STOK_BARANG		= URL_MASTER_BARANG+"/kurangiStok";

	public static final String URL_KELUAR_MASUK				= URL_APP+"/keluarMasukBarang";
	public static final String URL_KELUAR_MASUK_SAVE		= URL_KELUAR_MASUK+"/save";
	public static final String URL_KELUAR_MASUK_EDIT		= URL_KELUAR_MASUK+"/edit";
	public static final String URL_KELUAR_MASUK_DELETE		= URL_KELUAR_MASUK+"/delete";
	public static final String URL_KELUAR_MASUK_DATATABLE	= URL_KELUAR_MASUK+"/datatable";
	public static final String URL_LIST_BARANG_DATATABLE	= URL_KELUAR_MASUK+"/listBarang";
	public static final String URL_LIST_BD_DATATABLE		= URL_KELUAR_MASUK+"/listBd";
	
	public static final String URL_DAFTAR_KELUAR_MASUK				= URL_APP+"/daftarKeluarMasukBarang";
	public static final String URL_DAFTAR_KELUAR_MASUK_SAVE			= URL_DAFTAR_KELUAR_MASUK+"/save";
	public static final String URL_DAFTAR_KELUAR_MASUK_EDIT			= URL_DAFTAR_KELUAR_MASUK+"/edit";
	public static final String URL_DAFTAR_KELUAR_MASUK_KEMBALI			= URL_DAFTAR_KELUAR_MASUK+"/kembali";
	public static final String URL_DAFTAR_KELUAR_MASUK_DELETE		= URL_DAFTAR_KELUAR_MASUK+"/delete";
	public static final String URL_DAFTAR_KELUAR_MASUK_DATATABLE	= URL_DAFTAR_KELUAR_MASUK+"/datatable";

	public static final String URL_DAFTAR_BAGIAN					= URL_APP+"/datatable/bagian";
	
	public static final String URL_REALISASI_MANUAL				= URL_APP+"/realisasiManual";
	public static final String URL_REALISASI_MANUAL_SAVE		= URL_REALISASI_MANUAL+"/save";
	public static final String URL_REALISASI_MANUAL_EDIT		= URL_REALISASI_MANUAL+"/edit";
	public static final String URL_REALISASI_MANUAL_DELETE		= URL_REALISASI_MANUAL+"/delete";
	public static final String URL_REALISASI_MANUAL_DATATABLE	= URL_REALISASI_MANUAL+"/datatable";

	public static final String URL_DAFTAR_REALISASI_MANUAL				= URL_APP+"/daftarRealisasiManual";
	public static final String URL_DAFTAR_REALISASI_MANUAL_SAVE			= URL_DAFTAR_REALISASI_MANUAL+"/save";
	public static final String URL_DAFTAR_REALISASI_MANUAL_EDIT			= URL_DAFTAR_REALISASI_MANUAL+"/edit";
	public static final String URL_DAFTAR_REALISASI_MANUAL_DELETE		= URL_DAFTAR_REALISASI_MANUAL+"/delete";
	public static final String URL_DAFTAR_REALISASI_MANUAL_DATATABLE	= URL_DAFTAR_REALISASI_MANUAL+"/datatable";
	
	public static final String URL_LAPORAN						= URL_APP+"/laporan";
	public static final String URL_LAPORAN_STOK_OPNAME			= URL_LAPORAN+"/stokOpname";
	public static final String URL_LAPORAN_STOK_OPNAME_CETAK	= URL_LAPORAN_STOK_OPNAME+"/cetak";
	public static final String URL_LAPORAN_PER_BD				= URL_LAPORAN+"/perBd";
	public static final String URL_LAPORAN_PER_BD_CETAK			= URL_LAPORAN_PER_BD+"/cetak";
	public static final String URL_LAPORAN_HARIAN				= URL_LAPORAN+"/harian";
	public static final String URL_LAPORAN_HARIAN_CETAK			= URL_LAPORAN_HARIAN+"/cetak";
	public static final String URL_LAPORAN_PER_BARANG			= URL_LAPORAN+"/perBarang";
	public static final String URL_LAPORAN_PER_BARANG_CETAK		= URL_LAPORAN_PER_BARANG+"/cetak";
    public static final String URL_LAPORAN_PER_BARANG_TOTAL		= URL_LAPORAN_PER_BARANG+"/total";

	public static final String URL_DASHBOARD						= URL_APP+"/dashboard";
	public static final String URL_DASHBOARD_TOTAL_PENGELUARAN		= URL_DASHBOARD+"/total_pengeluaran";
	public static final String URL_DASHBOARD_TOTAL_PENGELUARAN_BY_BULAN		= URL_DASHBOARD_TOTAL_PENGELUARAN+"/by_bulan";
	public static final String URL_DASHBOARD_TOTAL_PENGELUARAN_BY_PERBARANG	= URL_DASHBOARD_TOTAL_PENGELUARAN_BY_BULAN+"/perbarang";

	public static final String URL_LAPORAN_REKAPAN				= URL_LAPORAN+"/rekapan";
	public static final String URL_LAPORAN_REKAPAN_CETAK		= URL_LAPORAN_REKAPAN+"/cetak";
	
	public static final String[] HEADER_MASTER_BD		= {"a.id", "a.namBd", "a.deskripsiBd"};
	public static final String[] HEADER_PENERIMA		= {"a.penerima"};
	public static final String[] HEADER_MASTER_BARANG	= {"a.id", "a.kodeBarang", "a.namaBarang", "a.stok"};
	public static final String[] HEADER_KELUAR_MASUK_BARANG	= {"id", "namaBd", "kodeBarang", "namaBarang", "jumlah", "satuan", "penerima", "tanggal", "status"};
	
}