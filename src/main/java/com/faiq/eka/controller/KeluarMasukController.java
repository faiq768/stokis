package com.faiq.eka.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.faiq.eka.persistence.entity.MasterBagian;
import com.faiq.eka.persistence.repository.MasterBagianRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.faiq.eka.constant.ApplConstant;
import com.faiq.eka.form.MasterForm;
import com.faiq.eka.persistence.entity.KeluarMasukBarang;
import com.faiq.eka.persistence.entity.MasterBarang;
import com.faiq.eka.persistence.entity.MasterBd;
import com.faiq.eka.persistence.repository.MasterBarangRepository;
import com.faiq.eka.persistence.service.KeluarMasukBarangService;
import com.faiq.eka.persistence.service.MasterBarangService;
import com.faiq.eka.persistence.service.MasterBdService;
import com.faiq.eka.util.bean.DataTableResultInfo;

@Controller
public class KeluarMasukController {
	
	@Autowired
	private KeluarMasukBarangService service;
	
	@Autowired
	private MasterBdService bdservice;
	
	@Autowired
	private MasterBarangRepository barangRepo;

	@Autowired
    private MasterBagianRepository masterBagianRepository;
	
	@RequestMapping(ApplConstant.URL_KELUAR_MASUK)
	public String indexKeluarMasuk(HttpServletRequest request, HttpServletResponse response,
			Model model) {
        List<MasterBagian> lBagian = new ArrayList<MasterBagian>();
        this.masterBagianRepository.findAll().forEach(action -> lBagian.add(action));
        model.addAttribute("lBagian",lBagian);
		return "transaksi/keluar_masuk_barang";
	}
	
	@RequestMapping(value= ApplConstant.URL_LIST_BARANG_DATATABLE)
    @CrossOrigin(origins = "*")
    public @ResponseBody DataTableResultInfo<MasterForm> dtListBarangKeluarMasuk(
            HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "iSortCol_0", required = false, defaultValue="0") Integer cols,
            @RequestParam(value = "sSortDir_0", required = false, defaultValue="asc") String order,
            @RequestParam(value = "iDisplayStart", required = false, defaultValue="0") Integer start,
            @RequestParam(value = "iDisplayLength", required = false, defaultValue="5") Integer length,
            @RequestParam(value = "sEcho",required = false,defaultValue = "0") Integer draw,
            @RequestParam(value = "sSearch",required = false,defaultValue = "") String search,
            @RequestParam(value = "namaBd",required = false,defaultValue = "") String namaBd
    ){
        cols = cols>ApplConstant.HEADER_MASTER_BARANG.length?0:cols;
        List<MasterForm> list = new ArrayList<MasterForm>();
        Pageable pageable = new PageRequest(0,length);
        Page<MasterForm> records = new PageImpl<MasterForm>(list,pageable,0);
        DataTableResultInfo<MasterForm> result = new DataTableResultInfo<MasterForm>();
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(0);
            
        records = this.service.listAllBarangStokNotNull(namaBd, search, cols, order, length, start);
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(records.getTotalElements());
        return result;
    }
	
	@RequestMapping(value= ApplConstant.URL_LIST_BD_DATATABLE)
    @CrossOrigin(origins = "*")
    public @ResponseBody DataTableResultInfo<MasterBd> dtListBdKeluarMasuk(
            HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "iSortCol_0", required = false, defaultValue="0") Integer cols,
            @RequestParam(value = "sSortDir_0", required = false, defaultValue="asc") String order,
            @RequestParam(value = "iDisplayStart", required = false, defaultValue="0") Integer start,
            @RequestParam(value = "iDisplayLength", required = false, defaultValue="5") Integer length,
            @RequestParam(value = "sEcho",required = false,defaultValue = "0") Integer draw,
            @RequestParam(value = "sSearch",required = false,defaultValue = "") String search
    ){
        cols = cols>ApplConstant.HEADER_MASTER_BD.length?0:cols;
        List<MasterBd> list = new ArrayList<MasterBd>();
        Pageable pageable = new PageRequest(0,length);
        Page<MasterBd> records = new PageImpl<MasterBd>(list,pageable,0);
        DataTableResultInfo<MasterBd> result = new DataTableResultInfo<MasterBd>();
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(0);
            
        records = this.bdservice.listAllBd(search, cols, order, length, start);
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(records.getTotalElements());
        return result;
    }
	
	@RequestMapping(value = ApplConstant.URL_KELUAR_MASUK_SAVE)
	public ResponseEntity<?> createKeluarMasukRepository(@RequestBody KeluarMasukBarang form, Errors errors, Model model) {
		
    	try {
    		Long idCkm;
    		try {
    			KeluarMasukBarang dt = this.service.findLastId();
    			idCkm = dt.getId();
    		}catch (NullPointerException e) {
    			idCkm = 0L;
    		}
    			idCkm++;
    			this.service.addKeluarMasukBarang(idCkm, form.getIdBarang(), form.getIdBd(), form.getJumlahBarang(), form.getPenerima(), form.getCatatan(), form.getIdBagian(), "KELUAR");
    			this.service.addHistoryKeluarMasuk(idCkm, "KELUAR", form.getJumlahBarang());
    			MasterBarang dt = this.barangRepo.findOne(form.getIdBarang());
    			double stok = dt.getStok()-form.getJumlahBarang();
    			double stokIndukSatuan = stok / dt.getNominalIndukSatuan();
    			dt.setStok(stok);
    			dt.setStokIndukSatuan(stokIndukSatuan);
    			this.barangRepo.save(dt);
    			return new ResponseEntity<>(form,HttpStatus.OK);
    	}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<> (errors.getFieldError(), HttpStatus.BAD_REQUEST);
		}
    }
}
