package com.faiq.eka.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import com.faiq.eka.persistence.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.faiq.eka.constant.ApplConstant;
import com.faiq.eka.form.MasterForm;
import com.faiq.eka.persistence.service.MasterBdService;
import com.faiq.eka.util.bean.DataTableResultInfo;

@Controller
@CrossOrigin(origins = "*")
public class MasterBdController {
	
	@Autowired
	private MasterBdService service;
	
	@RequestMapping(ApplConstant.URL_MASTER_BD)
	public String indexMasterBd(@ModelAttribute("masterBdForm") MasterBd form, HttpServletRequest request, HttpServletResponse response,
			Model model) {

//        UserDetailsStokis userDetailsStokis = (UserDetailsStokis) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		System.out.println(userDetailsStokis);
		return "master/master_bd";
	}
	
	@RequestMapping(value = ApplConstant.URL_MASTER_BD_SAVE)
	public ResponseEntity<?> createBdRepository(@RequestBody MasterBd form, Errors errors, Model model) {
		
    	try {
    		Optional<MasterBd> listBd = this.service.findByNamaBd(form.getNamaBd()); 
    		if(listBd.isPresent()) {
    			return new ResponseEntity<>(1,HttpStatus.BAD_REQUEST);
    		}else {
    			this.service.addMasterBd(form.getNamaBd().toUpperCase(), form.getDeskripsiBd());
    			return new ResponseEntity<>(form,HttpStatus.OK);
    		}
    	}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<> (errors.getFieldError(), HttpStatus.BAD_REQUEST);
		}
    }
	
	@RequestMapping(value= ApplConstant.URL_MASTER_BD_DATATABLE)
    @CrossOrigin(origins = "*")
    public @ResponseBody DataTableResultInfo<MasterBd> dtListBarang(
            HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "iSortCol_0", required = false, defaultValue="0") Integer cols,
            @RequestParam(value = "sSortDir_0", required = false, defaultValue="asc") String order,
            @RequestParam(value = "iDisplayStart", required = false, defaultValue="0") Integer start,
            @RequestParam(value = "iDisplayLength", required = false, defaultValue="5") Integer length,
            @RequestParam(value = "sEcho",required = false,defaultValue = "0") Integer draw,
            @RequestParam(value = "sSearch",required = false,defaultValue = "") String search
    ){
        cols = cols>ApplConstant.HEADER_MASTER_BD.length?0:cols;
        List<MasterBd> list = new ArrayList<MasterBd>();
        Pageable pageable = new PageRequest(0,length);
        Page<MasterBd> records = new PageImpl<MasterBd>(list,pageable,0);
        DataTableResultInfo<MasterBd> result = new DataTableResultInfo<MasterBd>();
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(0);
            
        records = this.service.listAllBd(search, cols, order, length, start);
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(records.getTotalElements());
        return result;
    }
	
	@RequestMapping(value = ApplConstant.URL_MASTER_BD_EDIT)
	public ResponseEntity<?> editBdRepository(@RequestBody MasterBd form, Errors errors, Model model) {
		
    	try {
			this.service.editMasterBd(form.getId(), form.getNamaBd().toUpperCase(), form.getDeskripsiBd());
			return new ResponseEntity<>(form,HttpStatus.OK);
    	}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<> (errors.getFieldError(), HttpStatus.BAD_REQUEST);
		}
    }
	
	@RequestMapping(value = ApplConstant.URL_MASTER_BD_DELETE)
	public ResponseEntity<?> deleteBdRepository(@RequestBody MasterBd form, Errors errors, Model model) {
		
    	try {
			this.service.deleteMasterBd(form.getId());
			return new ResponseEntity<>(form,HttpStatus.OK);
    	}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<> (errors.getFieldError(), HttpStatus.BAD_REQUEST);
		}
    }
	
	@RequestMapping(value= ApplConstant.URL_MASTER_BD_LIST_BARANG_DATATABLE)
    @CrossOrigin(origins = "*")
    public @ResponseBody DataTableResultInfo<MasterForm> dtListBarangMasterBd(
            HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "iSortCol_0", required = false, defaultValue="0") Integer cols,
            @RequestParam(value = "sSortDir_0", required = false, defaultValue="asc") String order,
            @RequestParam(value = "iDisplayStart", required = false, defaultValue="0") Integer start,
            @RequestParam(value = "iDisplayLength", required = false, defaultValue="5") Integer length,
            @RequestParam(value = "sEcho",required = false,defaultValue = "0") Integer draw,
            @RequestParam(value = "sSearch",required = false,defaultValue = "") String search,
            @RequestParam(value = "idBd",required = false,defaultValue = "") Long idBd
    ){
        cols = cols>ApplConstant.HEADER_MASTER_BARANG.length?0:cols;
        List<MasterForm> list = new ArrayList<MasterForm>();
        Pageable pageable = new PageRequest(0,length);
        Page<MasterForm> records = new PageImpl<MasterForm>(list,pageable,0);
        DataTableResultInfo<MasterForm> result = new DataTableResultInfo<MasterForm>();
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(0);
            
        records = this.service.listBarangMasterBd(idBd, search, cols, order, length, start);
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(records.getTotalElements());
        return result;
    }
	
	@RequestMapping(value = ApplConstant.URL_MASTER_BD_SAVE_DETAIL)
	public ResponseEntity<?> createBdDetailRepository(@RequestBody DetailMasterBd form, Errors errors, Model model) {
		
    	try {
    		this.service.addDetailMasterBd(form.getIdBd(), form.getIdBarang(), form.getSrJumlahBarang());
    		return new ResponseEntity<>(form,HttpStatus.OK);
    	}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<> (errors.getFieldError(), HttpStatus.BAD_REQUEST);
		}
    }
	
	@RequestMapping(value = ApplConstant.URL_DETAIL_MASTER_BD_DELETE)
	public ResponseEntity<?> deleteBdDetailRepository(@RequestBody DetailMasterBd form, Errors errors, Model model) {
		
    	try {
    		this.service.deleteDetailMasterBd(form.getId());
    		return new ResponseEntity<>(form,HttpStatus.OK);
    	}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<> (errors.getFieldError(), HttpStatus.BAD_REQUEST);
		}
    }
	
	@RequestMapping(value= ApplConstant.URL_DETAIL_MASTER_BD_DATATABLE)
    @CrossOrigin(origins = "*")
    public @ResponseBody DataTableResultInfo<MasterForm> dtListDetailMasterBd(
            HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "iSortCol_0", required = false, defaultValue="0") Integer cols,
            @RequestParam(value = "sSortDir_0", required = false, defaultValue="asc") String order,
            @RequestParam(value = "iDisplayStart", required = false, defaultValue="0") Integer start,
            @RequestParam(value = "iDisplayLength", required = false, defaultValue="5") Integer length,
            @RequestParam(value = "sEcho",required = false,defaultValue = "0") Integer draw,
            @RequestParam(value = "sSearch",required = false,defaultValue = "") String search,
            @RequestParam(value = "idBd",required = false,defaultValue = "") Long idBd
    ){
        cols = cols>ApplConstant.HEADER_MASTER_BARANG.length?0:cols;
        List<MasterForm> list = new ArrayList<MasterForm>();
        Pageable pageable = new PageRequest(0,length);
        Page<MasterForm> records = new PageImpl<MasterForm>(list,pageable,0);
        DataTableResultInfo<MasterForm> result = new DataTableResultInfo<MasterForm>();
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(0);
            
        records = this.service.listDetailMasterBd(idBd, search, cols, order, length, start);
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(records.getTotalElements());
        return result;
    }

    @RequestMapping(value= ApplConstant.URL_HISTORY_BARANG_DETAIL_MASTER_BD_DATATABLE)
    @CrossOrigin(origins = "*")
    public @ResponseBody DataTableResultInfo<MasterForm> dtHistoryBrangDetailMasterBd(
            HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "iSortCol_0", required = false, defaultValue="0") Integer cols,
            @RequestParam(value = "sSortDir_0", required = false, defaultValue="asc") String order,
            @RequestParam(value = "iDisplayStart", required = false, defaultValue="0") Integer start,
            @RequestParam(value = "iDisplayLength", required = false, defaultValue="5") Integer length,
            @RequestParam(value = "sEcho",required = false,defaultValue = "0") Integer draw,
            @RequestParam(value = "sSearch",required = false,defaultValue = "") String search,
            @RequestParam(value = "idBd",required = false,defaultValue = "") Long idBd,
            @RequestParam(value = "idBarang",required = false,defaultValue = "") Long idBarang
    ){
        cols = cols>ApplConstant.HEADER_MASTER_BARANG.length?0:cols;
        List<MasterForm> list = new ArrayList<MasterForm>();
        Pageable pageable = new PageRequest(0,length);
        Page<MasterForm> records = new PageImpl<MasterForm>(list,pageable,0);
        DataTableResultInfo<MasterForm> result = new DataTableResultInfo<MasterForm>();
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(0);

        records = this.service.listHistoryBarangDetailMasterBd(idBd, idBarang, search, cols, order, length, start);
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(records.getTotalElements());
        return result;
    }
	
}
