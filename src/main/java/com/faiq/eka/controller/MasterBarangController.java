package com.faiq.eka.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.faiq.eka.persistence.entity.*;
import com.faiq.eka.persistence.service.DaftarKeluarMasukService;
import com.faiq.eka.persistence.service.HistoryEditCatatanKeluarMasukService;
import com.faiq.eka.persistence.service.KeluarMasukBarangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.faiq.eka.constant.ApplConstant;
import com.faiq.eka.form.MasterForm;
import com.faiq.eka.persistence.repository.MasterBarangRepository;
import com.faiq.eka.persistence.repository.MasterSatuanRepository;
import com.faiq.eka.persistence.service.MasterBarangService;
import com.faiq.eka.util.bean.DataTableResultInfo;

@Controller
public class MasterBarangController {
	
	@Autowired
	private MasterBarangService service;
	
	@Autowired
	private MasterBarangRepository repo;
	
	@Autowired
	private MasterSatuanRepository repoSatuan;

	@Autowired
	private DaftarKeluarMasukService daftarKeluarMasukService;

	@Autowired
	private KeluarMasukBarangService keluarMasukBarangService;

	@Autowired
	private HistoryEditCatatanKeluarMasukService historyEditCatatanKeluarMasukService;
	
	@RequestMapping(ApplConstant.URL_MASTER_BARANG)
	public String indexMasterBarang(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		List<MasterSatuan> lSatuan = new ArrayList<MasterSatuan>();
		this.repoSatuan.findAll().forEach(action -> lSatuan.add(action));
		model.addAttribute("lSatuan",lSatuan);
		return "master/master_barang";
	}
	
	@RequestMapping(value= ApplConstant.URL_MASTER_BARANG_DATATABLE)
    @CrossOrigin(origins = "*")
    public @ResponseBody DataTableResultInfo<MasterForm> dtListBarang(
            HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "iSortCol_0", required = false, defaultValue="0") Integer cols,
            @RequestParam(value = "sSortDir_0", required = false, defaultValue="asc") String order,
            @RequestParam(value = "iDisplayStart", required = false, defaultValue="0") Integer start,
            @RequestParam(value = "iDisplayLength", required = false, defaultValue="5") Integer length,
            @RequestParam(value = "sEcho",required = false,defaultValue = "0") Integer draw,
            @RequestParam(value = "sSearch",required = false,defaultValue = "") String search
    ){
        cols = cols>ApplConstant.HEADER_MASTER_BARANG.length?0:cols;
        List<MasterForm> list = new ArrayList<MasterForm>();
        Pageable pageable = new PageRequest(0,length);
        Page<MasterForm> records = new PageImpl<MasterForm>(list,pageable,0);
        DataTableResultInfo<MasterForm> result = new DataTableResultInfo<MasterForm>();
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(0);
            
        records = this.service.listAllBarang(search, cols, order, length, start);
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(records.getTotalElements());
        return result;
    }
	
	@RequestMapping(value = ApplConstant.URL_MASTER_BARANG_SAVE)
	public ResponseEntity<?> createBarangRepository(@RequestBody MasterBarang form, Errors errors, Model model) {
		
    	try {
    			MasterBarang masterBarangList = this.repo.findByKodeBarang(form.getKodeBarang());
    			if (null == masterBarangList){
					this.service.addMasterBarang(form.getKodeBarang(), form.getNamaBarang(), form.getStok(), form.getIdSatuan(), form.getHargaSatuan(), form.getIdIndukSatuan(),form.getStokIndukSatuan(), form.getNominalIndukSatuan(), form.getMoving());
					MasterBarang masterBarang = this.repo.findByKodeBarangAndNamaBarangIgnoreCaseAndStok(form.getKodeBarang(), form.getNamaBarang(), form.getStok());
					this.service.addHistoryMasterBarang(masterBarang.getId(), masterBarang.getStok());
					return new ResponseEntity<>(form,HttpStatus.OK);
    			}else{
					return new ResponseEntity<>(HttpStatus.FOUND);
				}
    	}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<> (errors.getFieldError(), HttpStatus.BAD_REQUEST);
		}
    }
	
	@RequestMapping(value = ApplConstant.URL_MASTER_BARANG_DELETE)
	public ResponseEntity<?> deleteBarangRepository(@RequestBody MasterBarang form, Errors errors, Model model) {
		
    	try {
    		List<MasterBarang> masterBarangList = this.service.getDataBarangById(form.getId());
    		if (masterBarangList.size() > 0){
				return new ResponseEntity<> (form, HttpStatus.FOUND);
			}

			this.service.deleteMasterBarang(form.getId());
			this.service.deleteHistoryMasterBarang(form.getId());
			return new ResponseEntity<>(form,HttpStatus.OK);
    	}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<> (errors.getFieldError(), HttpStatus.BAD_REQUEST);
		}
    }

	@RequestMapping(value = ApplConstant.URL_MASTER_BARANG_CARI_TRX)
	public ResponseEntity<?> cariTrxBarangRepository(@RequestBody MasterBarang form, Errors errors, Model model) {

		try {
			List<KeluarMasukBarang> keluarMasukBarangList = this.daftarKeluarMasukService.getDataByIdBarang(form.getId());
			return new ResponseEntity<>(keluarMasukBarangList,HttpStatus.OK);

		}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<> (errors.getFieldError(), HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value = ApplConstant.URL_MASTER_BARANG_EDIT)
	public ResponseEntity<?> editBarangRepository(@RequestBody MasterBarang form, Errors errors, Model model) {
		
    	try {
			MasterBarang masterBarangList = this.repo.findByKodeBarang(form.getKodeBarang());
			if (null == masterBarangList || (null != masterBarangList && masterBarangList.getId() == form.getId())){
				List<KeluarMasukBarang> keluarMasukBarangList = this.daftarKeluarMasukService.getDataByIdBarang(form.getId());
				this.service.editMasterBarang(form.getId(), form.getKodeBarang(), form.getNamaBarang(), form.getIdSatuan(), form.getIdIndukSatuan(), form.getNominalIndukSatuan(),form.getHargaSatuan());
				if (keluarMasukBarangList.size() > 0){
					for (KeluarMasukBarang keluarMasukBarang : keluarMasukBarangList){
						HistoryEditCatatanKeluarMasuk historyEditCatatanKeluarMasuk = this.historyEditCatatanKeluarMasukService.getDataByIdCkm(keluarMasukBarang.getId());
						Double jumlahBarang = 0.0;
						if (null == historyEditCatatanKeluarMasuk){
							this.historyEditCatatanKeluarMasukService.addHistoryEditCatatanKeluarMasuk(keluarMasukBarang.getId(),keluarMasukBarang.getJumlahBarang());
							jumlahBarang = keluarMasukBarang.getJumlahBarang();
						}else{
							jumlahBarang = historyEditCatatanKeluarMasuk.getJumlahBarang();
						}

						MasterBarang masterBarang = this.repo.findOne(keluarMasukBarang.getIdBarang());
						Double totalKeluar = jumlahBarang / masterBarang.getNominalIndukSatuan();
						KeluarMasukBarang editKeluarMasukBarang = this.daftarKeluarMasukService.getDataById(keluarMasukBarang.getId());
						editKeluarMasukBarang.setJumlahBarang(totalKeluar);
						this.keluarMasukBarangService.saveEditKeluarMasukBarang(editKeluarMasukBarang);

					}
				}

				this.service.editMasterBarang(form.getId(), form.getKodeBarang(), form.getNamaBarang(), form.getIdSatuan(), form.getIdIndukSatuan(), form.getNominalIndukSatuan(),form.getHargaSatuan());
				return new ResponseEntity<>(form,HttpStatus.OK);
			}else{
				return new ResponseEntity<>(HttpStatus.FOUND);
			}
    	}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<> (errors.getFieldError(), HttpStatus.BAD_REQUEST);
		}
    }
	
	@RequestMapping(value = ApplConstant.URL_TAMBAH_STOK_BARANG)
	public ResponseEntity<?> editTambahStokBarangRepository(@RequestBody MasterBarang form, Errors errors, Model model) {
		
    	try {
    			MasterBarang dt = this.repo.findOne(form.getId());
    			double stok = dt.getStok() + form.getStok();
    			double stokIndukSatuan = stok / dt.getNominalIndukSatuan();
    			dt.setStok(stok);
    			dt.setStokIndukSatuan(stokIndukSatuan);
    			this.service.addHistoryMasterBarang(dt.getId(), form.getStok());
    			return new ResponseEntity<>(form,HttpStatus.OK);
    	}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<> (errors.getFieldError(), HttpStatus.BAD_REQUEST);
		}
    }
	
	@RequestMapping(value = ApplConstant.URL_KURANGI_STOK_BARANG)
	public ResponseEntity<?> editKurangiStokBarangRepository(@RequestBody MasterBarang form, Errors errors, Model model) {
		
    	try {
    			MasterBarang dt = this.repo.findOne(form.getId());
    			double stok = dt.getStok() - form.getStok();
				double stokIndukSatuan = stok / dt.getNominalIndukSatuan();
    			dt.setStok(stok);
				dt.setStokIndukSatuan(stokIndukSatuan);
    			this.service.addHistoryMasterBarang(dt.getId(), -form.getStok());
    			return new ResponseEntity<>(form,HttpStatus.OK);
    	}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<> (errors.getFieldError(), HttpStatus.BAD_REQUEST);
		}
    }
}
