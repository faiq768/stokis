package com.faiq.eka.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.faiq.eka.constant.ApplConstant;
import com.faiq.eka.form.MasterForm;
import com.faiq.eka.persistence.service.RealisasiManualService;
import com.faiq.eka.util.bean.DataTableResultInfo;

@Controller
public class DaftarRealisasiManual {

	@Autowired
	private RealisasiManualService service;
	
	@RequestMapping(ApplConstant.URL_DAFTAR_REALISASI_MANUAL)
	public String indexDaftarRealisasiManual(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		
		return "transaksi/daftar_realisasi_manual";
	}
	
	@RequestMapping(value= ApplConstant.URL_DAFTAR_REALISASI_MANUAL_DATATABLE)
    @CrossOrigin(origins = "*")
    public @ResponseBody DataTableResultInfo<MasterForm> dtListRealisasiManual(
            HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "iSortCol_0", required = false, defaultValue="0") Integer cols,
            @RequestParam(value = "sSortDir_0", required = false, defaultValue="asc") String order,
            @RequestParam(value = "iDisplayStart", required = false, defaultValue="0") Integer start,
            @RequestParam(value = "iDisplayLength", required = false, defaultValue="5") Integer length,
            @RequestParam(value = "sEcho",required = false,defaultValue = "0") Integer draw,
            @RequestParam(value = "sSearch",required = false,defaultValue = "") String search
    ){
        cols = cols>ApplConstant.HEADER_KELUAR_MASUK_BARANG.length?0:cols;
        List<MasterForm> list = new ArrayList<MasterForm>();
        Pageable pageable = new PageRequest(0,length);
        Page<MasterForm> records = new PageImpl<MasterForm>(list,pageable,0);
        DataTableResultInfo<MasterForm> result = new DataTableResultInfo<MasterForm>();
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(0);
            
        records = this.service.listAllRealisasiManual(search, cols, order, length, start);
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(records.getTotalElements());
        return result;
    }
}
