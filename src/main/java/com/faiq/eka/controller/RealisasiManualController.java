package com.faiq.eka.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.faiq.eka.constant.ApplConstant;
import com.faiq.eka.persistence.entity.KeluarMasukBarang;
import com.faiq.eka.persistence.entity.MasterBarang;
import com.faiq.eka.persistence.entity.MasterSatuan;
import com.faiq.eka.persistence.entity.RealisasiManual;
import com.faiq.eka.persistence.repository.MasterSatuanRepository;
import com.faiq.eka.persistence.service.MasterBdService;
import com.faiq.eka.persistence.service.RealisasiManualService;

@Controller
public class RealisasiManualController {
	
	@Autowired
	private RealisasiManualService service;
	
	@Autowired
	private MasterBdService bdService;

	@Autowired
	private MasterSatuanRepository repoSatuan;
	
	@RequestMapping(ApplConstant.URL_REALISASI_MANUAL)
	public String indexRealisasiManual(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		List<MasterSatuan> lSatuan = new ArrayList<MasterSatuan>();
		this.repoSatuan.findAll().forEach(action -> lSatuan.add(action));
		model.addAttribute("lSatuan",lSatuan);
		return "transaksi/realisasi_manual";
	}
	
	@RequestMapping(value = ApplConstant.URL_REALISASI_MANUAL_SAVE)
	public ResponseEntity<?> createRealisasiManualRepository(@RequestBody RealisasiManual form, Errors errors, Model model) {
		
    	try {
    		this.service.addRealisasiManual(form.getIdBd(), form.getIdBarang(), form.getJumlahBarang(), form.getIdSatuan(), form.getPenerima(), form.getCatatan());
			this.bdService.updateRealisasiManualPerBd(form.getIdBd(), form.getIdBarang(), 'Y');
    		return new ResponseEntity<>(form,HttpStatus.OK);
    	}catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<> (errors.getFieldError(), HttpStatus.BAD_REQUEST);
		}
    }
	
}
