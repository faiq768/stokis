package com.faiq.eka.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.faiq.eka.persistence.entity.*;
import com.faiq.eka.persistence.repository.*;
import com.faiq.eka.persistence.service.KeluarMasukBarangService;
import com.faiq.eka.persistence.service.MasterBarangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import com.faiq.eka.constant.ApplConstant;
import com.faiq.eka.form.MasterForm;
import com.faiq.eka.persistence.service.DaftarKeluarMasukService;
import com.faiq.eka.util.bean.DataTableResultInfo;

@Controller
public class DaftarKeluarMasukController {
	
	@Autowired
	private DaftarKeluarMasukService service;

    @Autowired
    private KeluarMasukBarangService keluarMasukBarangService;

    @Autowired
    private KeluarMasukBarangRepository keluarMasukBarangRepository;

    @Autowired
    private MasterBarangRepository masterBarangRepository;

    @Autowired
    private MasterBarangService masterBarangService;

    @Autowired
    private HistoryKeluarMasukRepository historyKeluarMasukRepository;

    @Autowired
    private MasterBagianRepository masterBagianRepository;
	
	@RequestMapping(ApplConstant.URL_DAFTAR_KELUAR_MASUK)
	public String indexKeluarMasuk(HttpServletRequest request, HttpServletResponse response,
			Model model) {
        List<MasterBagian> lBagian = new ArrayList<MasterBagian>();
        this.masterBagianRepository.findAll().forEach(action -> lBagian.add(action));
        model.addAttribute("lBagian",lBagian);
		return "transaksi/daftar_keluar_masuk_barang";
	}
	
	@RequestMapping(value= ApplConstant.URL_DAFTAR_KELUAR_MASUK_DATATABLE)
    @CrossOrigin(origins = "*")
    public @ResponseBody DataTableResultInfo<MasterForm> dtListKeluarMasukBarang(
            HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "iSortCol_0", required = false, defaultValue="0") Integer cols,
            @RequestParam(value = "sSortDir_0", required = false, defaultValue="asc") String order,
            @RequestParam(value = "iDisplayStart", required = false, defaultValue="0") Integer start,
            @RequestParam(value = "iDisplayLength", required = false, defaultValue="5") Integer length,
            @RequestParam(value = "sEcho",required = false,defaultValue = "0") Integer draw,
            @RequestParam(value = "sSearch",required = false,defaultValue = "") String search
    ){
        cols = cols>ApplConstant.HEADER_KELUAR_MASUK_BARANG.length?0:cols;
        List<MasterForm> list = new ArrayList<MasterForm>();
        Pageable pageable = new PageRequest(0,length);
        Page<MasterForm> records = new PageImpl<MasterForm>(list,pageable,0);
        DataTableResultInfo<MasterForm> result = new DataTableResultInfo<MasterForm>();
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(0);
            
        records = this.service.listAllKeluarMasukBarang(search, cols, order, length, start);
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(records.getTotalElements());
        return result;
    }

    @RequestMapping(value = ApplConstant.URL_DAFTAR_KELUAR_MASUK_EDIT)
    public ResponseEntity<?> updateDaftarKeluarMasukRepository(@RequestBody KeluarMasukBarang form, Errors errors, Model model) {

        try {
            this.service.editDaftarKeluarMasukBarang(form.getId(),form.getIdBd(),form.getCatatan(),form.getTanggalOrder(),form.getPenerima(), form.getIdBagian());
            return new ResponseEntity<>(form, HttpStatus.OK);
        }catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<> (errors.getFieldError(), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = ApplConstant.URL_DAFTAR_KELUAR_MASUK_KEMBALI)
    public ResponseEntity<?> kembaliDaftarKeluarMasukRepository(@RequestBody KeluarMasukBarang form, Errors errors, Model model) {
        try {
            KeluarMasukBarang keluarMasukBarang = this.keluarMasukBarangRepository.findOne(form.getId());
            MasterBarang masterBarang = this.masterBarangRepository.findOne(keluarMasukBarang.getIdBarang());


            if (form.getJumlahBarang() > keluarMasukBarang.getJumlahBarang()) return new ResponseEntity<>(HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE);

            double stok = masterBarang.getStok() + form.getJumlahBarang();
            double stokIndukSatuan = stok / masterBarang.getNominalIndukSatuan();

            masterBarang.setStok(stok);
            masterBarang.setStokIndukSatuan(stokIndukSatuan);
            this.masterBarangRepository.save(masterBarang);

            this.masterBarangService.addHistoryMasterBarang(masterBarang.getId(), form.getJumlahBarang());

            double stokKeluarMasuk = keluarMasukBarang.getJumlahBarang() - form.getJumlahBarang();

            keluarMasukBarang.setJumlahBarang(keluarMasukBarang.getJumlahBarang() - form.getJumlahBarang());
            if (stokKeluarMasuk < 1){
                keluarMasukBarang.setStatus("KEMBALI");
            }
            this.keluarMasukBarangRepository.save(keluarMasukBarang);

            HistoryKeluarMasuk historyKeluarMasuk = new HistoryKeluarMasuk();
            historyKeluarMasuk.setStatus("KEMBALI");
            historyKeluarMasuk.setStok(form.getJumlahBarang());
            historyKeluarMasuk.setTanggalDibuat(new Timestamp(System.currentTimeMillis()));
            this.historyKeluarMasukRepository.save(historyKeluarMasuk);
            return new ResponseEntity<>(form, HttpStatus.OK);
        }catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<> (errors.getFieldError(), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value= ApplConstant.URL_DAFTAR_BAGIAN)
    @CrossOrigin(origins = "*")
    public @ResponseBody DataTableResultInfo<MasterForm> dtListBagian(
            HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "iSortCol_0", required = false, defaultValue="0") Integer cols,
            @RequestParam(value = "sSortDir_0", required = false, defaultValue="asc") String order,
            @RequestParam(value = "iDisplayStart", required = false, defaultValue="0") Integer start,
            @RequestParam(value = "iDisplayLength", required = false, defaultValue="5") Integer length,
            @RequestParam(value = "sEcho",required = false,defaultValue = "0") Integer draw,
            @RequestParam(value = "sSearch",required = false,defaultValue = "") String search
    ){
        cols = cols>ApplConstant.HEADER_PENERIMA.length?0:cols;
        List<MasterForm> list = new ArrayList<MasterForm>();
        Pageable pageable = new PageRequest(0,length);
        Page<MasterForm> records = new PageImpl<MasterForm>(list,pageable,0);
        DataTableResultInfo<MasterForm> result = new DataTableResultInfo<MasterForm>();
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(0);

        records = this.service.listDatatableBagian(search, cols, order, length, start);
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(records.getTotalElements());
        return result;
    }
}
