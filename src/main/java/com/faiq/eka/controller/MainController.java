package com.faiq.eka.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.faiq.eka.form.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.faiq.eka.constant.ApplConstant;
import com.faiq.eka.persistence.mybatis.MasterMapper;
import com.faiq.eka.persistence.repository.MasterBarangRepository;
import com.faiq.eka.persistence.service.LaporanService;
import com.faiq.eka.util.bean.DataTableResultInfo;

@Controller
public class MainController {
	
	@Autowired
	private LaporanService service;
	
	@Autowired
	private MasterBarangRepository barangRepo;
	
	@Autowired
	private MasterMapper myBatis;
	
	@RequestMapping(ApplConstant.URL_DEFAULT)
	public String index(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		
		return "redirect:"+ApplConstant.URL_APP;
	}
	
	@RequestMapping(ApplConstant.URL_APP)
	public String indexApp(HttpServletRequest request, HttpServletResponse response, Model model) {
		
		Long countAllBarang = this.barangRepo.count();
		Integer countPenerima = this.myBatis.countAllPenerima();
		model.addAttribute("totalBarang",countAllBarang);
		model.addAttribute("totalPenerima",countPenerima);
		
		return "general/home";
	}

    @RequestMapping(value = ApplConstant.URL_LOGIN, method = RequestMethod.GET)
    public String login(@ModelAttribute("loginForm") LoginForm loginForm,
                        HttpServletRequest request, HttpServletResponse response,
                        Model model, String logout) {

        return "general/login";
    }
	
	@RequestMapping(ApplConstant.URL_PENERIMA)
	public String indexPenerima(HttpServletRequest request, HttpServletResponse response, Model model) {
		
		return "general/penerima";
	}
	
	@RequestMapping(value= ApplConstant.URL_PENERIMA_DATATABLE)
    @CrossOrigin(origins = "*")
    public @ResponseBody DataTableResultInfo<MasterForm> dtListPenerima(
            HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "iSortCol_0", required = false, defaultValue="0") Integer cols,
            @RequestParam(value = "sSortDir_0", required = false, defaultValue="asc") String order,
            @RequestParam(value = "iDisplayStart", required = false, defaultValue="0") Integer start,
            @RequestParam(value = "iDisplayLength", required = false, defaultValue="5") Integer length,
            @RequestParam(value = "sEcho",required = false,defaultValue = "0") Integer draw,
            @RequestParam(value = "sSearch",required = false,defaultValue = "") String search
    ){
        cols = cols>ApplConstant.HEADER_PENERIMA.length?0:cols;
        List<MasterForm> list = new ArrayList<MasterForm>();
        Pageable pageable = new PageRequest(0,length);
        Page<MasterForm> records = new PageImpl<MasterForm>(list,pageable,0);
        DataTableResultInfo<MasterForm> result = new DataTableResultInfo<MasterForm>();
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(0);
            
        records = this.service.listAllPenerima(search, cols, order, length, start);
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(records.getTotalElements());
        return result;
    }
	
	@RequestMapping(value= ApplConstant.URL_BARANG_PENERIMA_DATATABLE)
    @CrossOrigin(origins = "*")
    public @ResponseBody DataTableResultInfo<MasterForm> dtListBarangPenerima(
            HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "iSortCol_0", required = false, defaultValue="0") Integer cols,
            @RequestParam(value = "sSortDir_0", required = false, defaultValue="asc") String order,
            @RequestParam(value = "iDisplayStart", required = false, defaultValue="0") Integer start,
            @RequestParam(value = "iDisplayLength", required = false, defaultValue="5") Integer length,
            @RequestParam(value = "sEcho",required = false,defaultValue = "0") Integer draw,
            @RequestParam(value = "sSearch",required = false,defaultValue = "") String search,
            @RequestParam(value = "penerima",required = false,defaultValue = "") String penerima
    ){
        cols = cols>ApplConstant.HEADER_MASTER_BARANG.length?0:cols;
        List<MasterForm> list = new ArrayList<MasterForm>();
        Pageable pageable = new PageRequest(0,length);
        Page<MasterForm> records = new PageImpl<MasterForm>(list,pageable,0);
        DataTableResultInfo<MasterForm> result = new DataTableResultInfo<MasterForm>();
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(0);
            
        records = this.service.listAllBarangPenerima(penerima, search, cols, order, length, start);
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(records.getTotalElements());
        return result;
    }
}
