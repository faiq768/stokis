package com.faiq.eka.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.faiq.eka.form.GenerateLaporan;
import com.faiq.eka.persistence.entity.*;
import com.faiq.eka.util.bean.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ResourceUtils;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import com.faiq.eka.constant.ApplConstant;
import com.faiq.eka.form.MasterForm;
import com.faiq.eka.persistence.service.LaporanService;

@Controller
public class LaporanController {
	
	@Value(ApplConstant.URL_DIRECTORY+"//program_eka//Stok Opname/")
    private String FOLDER_LAPORAN_STOK_OPNAME;

	@Value(ApplConstant.URL_DIRECTORY+"//program_eka//Laporan Harian/")
	private String FOLDER_LAPORAN_HARIAN;

	@Value(ApplConstant.URL_DIRECTORY+"//program_eka//Laporan BD/")
	private String FOLDER_LAPORAN_BD;
	
	@Autowired
	private LaporanService service;

	@Autowired
	DataSource dataSource;

	@Value("${report.file.stock.opname}")
	private String REPORT_STOK_OPNAME;

	@Value("${report.file.laporan.harian}")
	private String REPORT_HARIAN;

	@Value("${report.file.laporan.bd}")
	private String REPORT_BD;

	@Value("${jasper.stok.opname}")
	private String FILE_JASPER_STOK_OPNAME;

	@Value("${jasper.laporan.harian}")
	private String FILE_JASPER_LAPORAN_HARIAN;

	@Value("${jasper.laporan.bd}")
	private String FILE_JASPER_LAPORAN_BD;
	
	@RequestMapping(ApplConstant.URL_LAPORAN_STOK_OPNAME)
	public String indexLaporanStokOpname(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		
		return "report/stok_opname";
	}
	
	@RequestMapping(value = ApplConstant.URL_LAPORAN_STOK_OPNAME_CETAK)
	public ResponseEntity<Resource> cetakLaporanStokOpname(
			HttpServletRequest request, HttpServletResponse response,
			@RequestParam(name="pTanggal", required=false) String pTanggal,
			@RequestParam(name="pBD", required=false, value = "") String pBd
	)throws SQLException, JRException, IOException {
		GenerateLaporan generateLaporan = generateLaporan(FOLDER_LAPORAN_STOK_OPNAME,REPORT_STOK_OPNAME,pTanggal,pBd,FILE_JASPER_STOK_OPNAME);

		InputStreamResource resource = new InputStreamResource(new FileInputStream(generateLaporan.getOutputFile()));
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", generateLaporan.getContentType());
		headers.add("Content-Disposition", "inline;filename="+generateLaporan.getFileName()+generateLaporan.getExt());
		return ResponseEntity.ok()
				.headers(headers)
				.contentLength(generateLaporan.getOutputFile().length())
				.contentType(MediaType.parseMediaType(generateLaporan.getContentType()))
				.body(resource);
    }
	
	@RequestMapping(ApplConstant.URL_LAPORAN_PER_BD)
	public String indexLaporanPerBd(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		
		return "report/perBd";
	}
	
	@RequestMapping(value = ApplConstant.URL_LAPORAN_PER_BD_CETAK)
	public ResponseEntity<Resource> cetakLaporanPerBd(HttpServletRequest request, HttpServletResponse response,
													  @RequestParam(name="pTanggal", required=false, value = "") String pTanggal,
													  @RequestParam(name="pBd", required=false) String pBd
	)throws SQLException, JRException, IOException {
		GenerateLaporan generateLaporan = generateLaporan(FOLDER_LAPORAN_BD,REPORT_BD,pTanggal,pBd,FILE_JASPER_LAPORAN_BD);

		InputStreamResource resource = new InputStreamResource(new FileInputStream(generateLaporan.getOutputFile()));
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", generateLaporan.getContentType());
		headers.add("Content-Disposition", "inline;filename="+generateLaporan.getFileName()+generateLaporan.getExt());
		return ResponseEntity.ok()
				.headers(headers)
				.contentLength(generateLaporan.getOutputFile().length())
				.contentType(MediaType.parseMediaType(generateLaporan.getContentType()))
				.body(resource);
    }
	
	@RequestMapping(ApplConstant.URL_LAPORAN_HARIAN)
	public String indexLaporanHarian(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		
		return "report/harian";
	}
	
	@RequestMapping(value = ApplConstant.URL_LAPORAN_HARIAN_CETAK)
	public ResponseEntity<Resource> cetakLaporanHarian(HttpServletRequest request, HttpServletResponse response,
												@RequestParam(name="pTanggal", required=false) String pTanggal,
													   @RequestParam(name="pBD", required=false, value = "") String pBd
	) throws SQLException, JRException, IOException{
		GenerateLaporan generateLaporan = generateLaporan(FOLDER_LAPORAN_HARIAN,REPORT_HARIAN,pTanggal,pBd,FILE_JASPER_LAPORAN_HARIAN);

		InputStreamResource resource = new InputStreamResource(new FileInputStream(generateLaporan.getOutputFile()));
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", generateLaporan.getContentType());
		headers.add("Content-Disposition", "inline;filename="+generateLaporan.getFileName()+generateLaporan.getExt());
		return ResponseEntity.ok()
				.headers(headers)
				.contentLength(generateLaporan.getOutputFile().length())
				.contentType(MediaType.parseMediaType(generateLaporan.getContentType()))
				.body(resource);
    }

    public GenerateLaporan generateLaporan(String path, String fileName, String pTanggal, String namaBd, String fileJasper)throws SQLException, JRException, IOException{
//		String path = this.DIRECTORY_PDF+"//program_eka//Stok Opname//";
		if (!Files.exists(Paths.get(path))) {
			Files.createDirectories(Paths.get(path));
		}

		Map<String, Object> parameters = new HashMap<String, Object>();
		try{
			fileName = fileName.concat(" ").concat(namaBd);
			parameters.put("namaBd", namaBd);
		}catch (NullPointerException e){
			fileName = fileName.concat(" ").concat(pTanggal);
			parameters.put("tanggal", pTanggal);
		}

		JasperPrint jasperPrint = JasperFillManager.fillReport(getClass().getResourceAsStream(fileJasper),parameters, dataSource.getConnection());
		String ext = ApplConstant.EXT_XLSX;
		String contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		JRXlsxExporter exporter = new JRXlsxExporter();
		SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();

		File outputFile = new File(path+fileName+ext);
		exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputFile));
		configuration.setDetectCellType(true);//Set configuration as you like it!!
		configuration.setCollapseRowSpan(false);
		exporter.setConfiguration(configuration);
		exporter.exportReport();

		GenerateLaporan generateLaporan = new GenerateLaporan();
		generateLaporan.setExt(ext);
		generateLaporan.setFileName(fileName);
		generateLaporan.setOutputFile(outputFile);
		generateLaporan.setContentType(contentType);
		return generateLaporan;
	}

	@RequestMapping(ApplConstant.URL_LAPORAN_PER_BARANG)
	public String indexLaporanPerBarang(HttpServletRequest request, HttpServletResponse response,
										 Model model) {

		return "report/per_barang";
	}

	@RequestMapping(value= ApplConstant.URL_LAPORAN_PER_BARANG_CETAK)
	@CrossOrigin(origins = "*")
	public @ResponseBody
	DataTableResultInfo<MasterForm> indexLaporanPerBarangCetak(
			HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "iSortCol_0", required = false, defaultValue="0") Integer cols,
			@RequestParam(value = "sSortDir_0", required = false, defaultValue="asc") String order,
			@RequestParam(value = "iDisplayStart", required = false, defaultValue="0") Integer start,
			@RequestParam(value = "iDisplayLength", required = false, defaultValue="5") Integer length,
			@RequestParam(value = "sEcho",required = false,defaultValue = "0") Integer draw,
			@RequestParam(value = "idBarang",required = false,defaultValue = "") Long idBarang,
			@RequestParam(value = "tanggalMulai",required = false,defaultValue = "") String tanggalMulai,
			@RequestParam(value = "tanggalAkhir",required = false,defaultValue = "") String tanggalAkhir
	){
		cols = cols>ApplConstant.HEADER_MASTER_BARANG.length?0:cols;
		List<MasterForm> list = new ArrayList<MasterForm>();
		Pageable pageable = new PageRequest(0,length);
		Page<MasterForm> records = new PageImpl<MasterForm>(list,pageable,0);
		DataTableResultInfo<MasterForm> result = new DataTableResultInfo<MasterForm>();
		result.setData(records);
		result.setsEcho(draw);
		result.setiTotalRecords(records.getNumberOfElements());
		result.setiTotalDisplayRecords(0);

		MasterForm masterForm = new MasterForm();
		masterForm.setIdBarang(idBarang);
		masterForm.setTanggalMulai(tanggalMulai);
		masterForm.setTanggalAkhir(tanggalAkhir);

		records = this.service.laporanPerBarang(masterForm, cols, order, length, start);
		result.setData(records);
		result.setsEcho(draw);
		result.setiTotalRecords(records.getNumberOfElements());
		result.setiTotalDisplayRecords(records.getTotalElements());
		return result;
	}

    @RequestMapping(value= ApplConstant.URL_LAPORAN_PER_BARANG_TOTAL)
    @CrossOrigin(origins = "*")
    public @ResponseBody
    DataTableResultInfo<MasterForm> indexTotalLaporanPerBarangCetak(
            HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "iSortCol_0", required = false, defaultValue="0") Integer cols,
            @RequestParam(value = "sSortDir_0", required = false, defaultValue="asc") String order,
            @RequestParam(value = "iDisplayStart", required = false, defaultValue="0") Integer start,
            @RequestParam(value = "iDisplayLength", required = false, defaultValue="5") Integer length,
            @RequestParam(value = "sEcho",required = false,defaultValue = "0") Integer draw,
            @RequestParam(value = "idBarang",required = false,defaultValue = "") Long idBarang,
            @RequestParam(value = "tanggalMulai",required = false,defaultValue = "") String tanggalMulai,
            @RequestParam(value = "tanggalAkhir",required = false,defaultValue = "") String tanggalAkhir
    ){
        cols = cols>ApplConstant.HEADER_MASTER_BARANG.length?0:cols;
        List<MasterForm> list = new ArrayList<MasterForm>();
        Pageable pageable = new PageRequest(0,length);
        Page<MasterForm> records = new PageImpl<MasterForm>(list,pageable,0);
        DataTableResultInfo<MasterForm> result = new DataTableResultInfo<MasterForm>();
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(0);

        MasterForm masterForm = new MasterForm();
        masterForm.setIdBarang(idBarang);
        masterForm.setTanggalMulai(tanggalMulai);
        masterForm.setTanggalAkhir(tanggalAkhir);

        records = this.service.totalLaporanPerBarang(masterForm, cols, order, length, start);
        result.setData(records);
        result.setsEcho(draw);
        result.setiTotalRecords(records.getNumberOfElements());
        result.setiTotalDisplayRecords(records.getTotalElements());
        return result;
    }

	@RequestMapping(ApplConstant.URL_LAPORAN_REKAPAN)
	public String indexLaporanRekapan(HttpServletRequest request, HttpServletResponse response,
										Model model) {

		return "report/bulanan";
	}

	@RequestMapping(value= ApplConstant.URL_LAPORAN_REKAPAN_CETAK)
	@CrossOrigin(origins = "*")
	public @ResponseBody
	DataTableResultInfo<MasterForm> indexLaporanRekapanCetak(
			HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "iSortCol_0", required = false, defaultValue="0") Integer cols,
			@RequestParam(value = "sSortDir_0", required = false, defaultValue="asc") String order,
			@RequestParam(value = "iDisplayStart", required = false, defaultValue="0") Integer start,
			@RequestParam(value = "iDisplayLength", required = false, defaultValue="5") Integer length,
			@RequestParam(value = "sEcho",required = false,defaultValue = "0") Integer draw,
			@RequestParam(value = "tanggalMulai",required = false,defaultValue = "") String tanggalMulai,
			@RequestParam(value = "tanggalAkhir",required = false,defaultValue = "") String tanggalAkhir
	){
		cols = cols>ApplConstant.HEADER_MASTER_BARANG.length?0:cols;
		List<MasterForm> list = new ArrayList<MasterForm>();
		Pageable pageable = new PageRequest(0,length);
		Page<MasterForm> records = new PageImpl<MasterForm>(list,pageable,0);
		DataTableResultInfo<MasterForm> result = new DataTableResultInfo<MasterForm>();
		result.setData(records);
		result.setsEcho(draw);
		result.setiTotalRecords(records.getNumberOfElements());
		result.setiTotalDisplayRecords(0);

		MasterForm masterForm = new MasterForm();
		masterForm.setTanggalMulai(tanggalMulai);
		masterForm.setTanggalAkhir(tanggalAkhir);

		records = this.service.laporanRekapan(masterForm, cols, order, length, start);
		result.setData(records);
		result.setsEcho(draw);
		result.setiTotalRecords(records.getNumberOfElements());
		result.setiTotalDisplayRecords(records.getTotalElements());
		return result;
	}

}
