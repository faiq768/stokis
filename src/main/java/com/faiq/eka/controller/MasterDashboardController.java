package com.faiq.eka.controller;

import com.faiq.eka.constant.ApplConstant;
import com.faiq.eka.form.MasterDashboardForm;
import com.faiq.eka.persistence.service.MasterDashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@Controller
public class MasterDashboardController {

    @Autowired
    private MasterDashboardService masterDashboardService;

    @RequestMapping(value = ApplConstant.URL_DASHBOARD_TOTAL_PENGELUARAN)
    @CrossOrigin(origins = "*")
    public ResponseEntity<?> totalPengeluaranPertahun(@Valid @RequestBody MasterDashboardForm form, Errors errors, HttpServletRequest request,
                                             HttpServletResponse response, BindingResult messages, Model model) {
        try {
            List<MasterDashboardForm> masterDashboardForms = this.masterDashboardService.getTotalPengeluaranByTahun(form.getTahun());
            return new ResponseEntity<>(masterDashboardForms,HttpStatus.OK);
        }catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<> (HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = ApplConstant.URL_DASHBOARD_TOTAL_PENGELUARAN_BY_BULAN)
    @CrossOrigin(origins = "*")
    public ResponseEntity<?> totalPengeluaranPerbulan(@Valid @RequestBody MasterDashboardForm form, Errors errors, HttpServletRequest request,
                                             HttpServletResponse response, BindingResult messages, Model model) {
        try {
            List<MasterDashboardForm> masterDashboardForms = this.masterDashboardService.getTotalPengeluaranByBulanAndTahun(form.getTahun(),form.getBulan());
            return new ResponseEntity<>(masterDashboardForms,HttpStatus.OK);
        }catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<> (HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = ApplConstant.URL_DASHBOARD_TOTAL_PENGELUARAN_BY_PERBARANG)
    @CrossOrigin(origins = "*")
    public ResponseEntity<?> totalPengeluaranPerBarang(@Valid @RequestBody MasterDashboardForm form, Errors errors, HttpServletRequest request,
                                                      HttpServletResponse response, BindingResult messages, Model model) {
        try {
            List<MasterDashboardForm> masterDashboardForms = this.masterDashboardService.getTotalPengeluaranByBulanAndTahunAndNamaBarang(form.getTahun(),form.getBulan(),form.getNamaBarang());
            return new ResponseEntity<>(masterDashboardForms,HttpStatus.OK);
        }catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<> (HttpStatus.BAD_REQUEST);
        }
    }

}
