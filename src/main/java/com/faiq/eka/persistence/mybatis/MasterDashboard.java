package com.faiq.eka.persistence.mybatis;

import com.faiq.eka.form.MasterDashboardForm;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Map;

@Repository
public interface MasterDashboard {
    Collection<MasterDashboardForm> getTotalPengeluaranByYear(Map map);
    Collection<MasterDashboardForm> getTotalPengeluaranByYearAndMonth(Map map);
    Collection<MasterDashboardForm> getTotalPengeluaranByYearAndMonthAndNamaBarang(Map map);
}
