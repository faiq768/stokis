package com.faiq.eka.persistence.mybatis;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.faiq.eka.form.MasterForm;


@Repository
public interface MasterMapper {
	List<MasterForm> listBarangMasterBd(Map map);
	Integer getCountlistBarangMasterBd(Map map);
	
	List<MasterForm> listDetailMasterBd(Map map);
	Integer getCountlistDetailMasterBd(Map map);

	List<MasterForm> listHistoryBarangDetailMasterBd(Map map);
	Integer getCountlistHistoryBarangDetailMasterBd(Map map);
	
	List<MasterForm> listAllBarangStokNotAndNamaBdNotNull(Map map);
	Integer countAllBarangStokNotNullAndNamaBdNotNull(Map map);
	
	List<MasterForm> listAllBarangStokNotNullAndNamaBdNull(Map map);
	Integer countAllBarangStokNotNullAndNamaBdNull(Map map);
	
	List<MasterForm> listAllBarang(Map map);
	Integer countAllBarang(Map map);
	
	List<MasterForm> listAllKeluarMasukBarang(Map map);
	Integer countAllKeluarMasukBarang(Map map);
	
	List<MasterForm> listAllRealisasiManual(Map map);
	Integer countAllRealisasiManual(Map map);
	
	List<MasterForm> listAllPenerima(Map map);
	Integer countAllPenerimaDt(Map map);
	
	List<MasterForm> listAllBarangPenerima(Map map);
	Integer countAllBarangPenerima(Map map);

	List<MasterForm> laporanPerBarang(Map map);
	Integer countLaporanPerBarang(Map map);

	List<MasterForm> totalLaporanPerBarang(Map map);
	Integer countTotalLaporanPerBarang(Map map);

	List<MasterForm> listDatatableBagian(Map map);
	Integer countListDatatableBagian(Map map);

	List<MasterForm> laporanRekapan(Map map);
	Integer countLaporanRekapan(Map map);
	
	Integer countAllPenerima();
}
