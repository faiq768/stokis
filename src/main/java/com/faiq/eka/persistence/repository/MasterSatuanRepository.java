package com.faiq.eka.persistence.repository;

import java.util.Collection;

import com.faiq.eka.persistence.entity.MasterSatuan;
import com.faiq.eka.util.repository.BaseCrudRepository;
import com.faiq.eka.util.repository.BaseRepository;

public interface MasterSatuanRepository extends BaseRepository<MasterSatuan, Long>, BaseCrudRepository<MasterSatuan,Long> {
	
}
