package com.faiq.eka.persistence.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import com.faiq.eka.persistence.entity.HistoryBarang;
import com.faiq.eka.persistence.entity.KeluarMasukBarang;
import com.faiq.eka.util.repository.BaseCrudRepository;
import com.faiq.eka.util.repository.BaseRepository;

@Repository
public interface HistoryBarangRepository extends BaseRepository<HistoryBarang, Long>, BaseCrudRepository<HistoryBarang,Long> {
	@Modifying
    @Transactional
	void deleteByIdBarang(Long idBarang);
}
