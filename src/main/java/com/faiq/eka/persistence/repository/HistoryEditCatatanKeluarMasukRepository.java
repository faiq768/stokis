package com.faiq.eka.persistence.repository;

import com.faiq.eka.persistence.entity.HistoryEditCatatanKeluarMasuk;
import com.faiq.eka.util.repository.BaseCrudRepository;
import com.faiq.eka.util.repository.BaseRepository;

public interface HistoryEditCatatanKeluarMasukRepository extends BaseCrudRepository<HistoryEditCatatanKeluarMasuk, Long>, BaseRepository<HistoryEditCatatanKeluarMasuk, Long> {
    HistoryEditCatatanKeluarMasuk findByIdCkm(Long idCkm);
}
