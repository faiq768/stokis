package com.faiq.eka.persistence.repository;

import org.springframework.stereotype.Repository;

import com.faiq.eka.persistence.entity.KeluarMasukBarang;
import com.faiq.eka.util.repository.BaseCrudRepository;
import com.faiq.eka.util.repository.BaseRepository;

import java.util.List;

@Repository
public interface KeluarMasukBarangRepository extends BaseRepository<KeluarMasukBarang, Long>, BaseCrudRepository<KeluarMasukBarang,Long>{
	KeluarMasukBarang findTopByOrderByIdDesc();
	List<KeluarMasukBarang> findByIdBarang(Long idBarang);
}
