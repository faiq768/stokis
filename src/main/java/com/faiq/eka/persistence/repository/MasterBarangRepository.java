package com.faiq.eka.persistence.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.faiq.eka.persistence.entity.MasterBarang;
import com.faiq.eka.util.repository.BaseCrudRepository;
import com.faiq.eka.util.repository.BaseRepository;

@Repository
public interface MasterBarangRepository extends BaseRepository<MasterBarang, Long>, BaseCrudRepository<MasterBarang,Long>{
	Optional<MasterBarang> findByKodeBarangAndNamaBarangIgnoreCase(String kodeBarang, String namaBarang);
	MasterBarang findByKodeBarangAndNamaBarangIgnoreCaseAndStok(String kodeBarang, String namaBarang, double stok);
	MasterBarang findByKodeBarang(String kodeBarang);
	List<MasterBarang> findAllByKodeBarang(String kodeBarang);
	List<MasterBarang> findById(Long idBarang);
}
