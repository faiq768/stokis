package com.faiq.eka.persistence.repository;

import org.springframework.stereotype.Repository;

import com.faiq.eka.persistence.entity.RealisasiManual;
import com.faiq.eka.util.repository.BaseCrudRepository;
import com.faiq.eka.util.repository.BaseRepository;

@Repository
public interface RealisasiManualRepository extends BaseRepository<RealisasiManual,Long>, BaseCrudRepository<RealisasiManual,Long> {

}
