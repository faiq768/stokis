package com.faiq.eka.persistence.repository;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.faiq.eka.persistence.entity.MasterBd;
import com.faiq.eka.util.repository.BaseCrudRepository;
import com.faiq.eka.util.repository.BaseRepository;

@Repository
public interface MasterBdRepository extends BaseRepository<MasterBd, Long>, BaseCrudRepository<MasterBd,Long>{
	Optional<MasterBd> findByNamaBdIgnoreCase(String namaBd);
}
