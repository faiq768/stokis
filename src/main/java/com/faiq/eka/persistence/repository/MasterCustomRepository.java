package com.faiq.eka.persistence.repository;

import java.util.List;

import com.faiq.eka.form.MasterForm;
import com.faiq.eka.persistence.entity.MasterBarang;
import com.faiq.eka.persistence.entity.MasterBd;


public interface MasterCustomRepository {
	Long countAllBd(String search);
	List<MasterBd> listAllBd(String search, int colsNo, String order, int limit, int offset);
	
	
//	Long countAllBarangStokNotNull(String namaBd, String search);
//	List<MasterForm> listAllBarangStokNotNull(String namaBd, String search, int colsNo, String order, int limit, int offset);

	Long countAllBarangMasterBd(String idBd, String search);
	List<MasterBarang> listAllBarangMasterBd(String idBd, String search, int colsNo, String order, int limit, int offset);
}
