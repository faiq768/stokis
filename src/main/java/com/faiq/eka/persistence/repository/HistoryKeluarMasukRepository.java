package com.faiq.eka.persistence.repository;

import org.springframework.stereotype.Repository;

import com.faiq.eka.persistence.entity.HistoryBarang;
import com.faiq.eka.persistence.entity.HistoryKeluarMasuk;
import com.faiq.eka.util.repository.BaseCrudRepository;
import com.faiq.eka.util.repository.BaseRepository;

import java.util.*;

@Repository
public interface HistoryKeluarMasukRepository extends BaseRepository<HistoryKeluarMasuk, Long>, BaseCrudRepository<HistoryKeluarMasuk,Long>  {
    HistoryKeluarMasuk findByIdCkm(long idCkm);
    HistoryKeluarMasuk findByIdCkmAndTanggalDibuat(long idCkm, Date tanggalDibuat);
}
