package com.faiq.eka.persistence.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.faiq.eka.form.MasterForm;
import com.faiq.eka.persistence.entity.MasterBarang;
import com.faiq.eka.persistence.entity.MasterBd;
import com.faiq.eka.util.Strings;

public class MasterCustomRepositoryImpl implements MasterCustomRepository{
	
	@PersistenceContext
    private EntityManager entityManager;

	@Override
	public Long countAllBd(String search) {
		String query = "SELECT count(1) " + 
				"FROM MasterBd a "+
				"WHERE 1=1";		
						
		if(!Strings.isNoE(search)) query += " AND (a.namaBd like '%"+search.toUpperCase()+"%'"
				+ " OR a.deskripsiBd like '%"+search+"%')";
		
		return (Long) this.entityManager.createQuery(query).getSingleResult();
	}

	@Override
	public List<MasterBd> listAllBd(String search, int colsNo, String order, int limit, int offset) {
		String query = "SELECT " + 
				"new com.faiq.eka.persistence.entity.MasterBd(a.id, a.namaBd, a.deskripsiBd) "  
				+ "FROM MasterBd a "
				+ "WHERE 1=1";
		
		if(!Strings.isNoE(search)) query += " AND (a.namaBd like '%"+search.toUpperCase()+"%'"
				+ " OR a.deskripsiBd like '%"+search+"%')";
		
		Query q = this.entityManager.createQuery(query);
		q.setFirstResult(offset);
		q.setMaxResults(limit);
		return q.getResultList();
	}

	

//	@Override
//	public Long countAllBarangStokNotNull(String namaBd, String search) {
//		String query = "SELECT count(1) " + 
//				"FROM MasterBarang a "+
//				"WHERE 1=1 AND a.stok != 0 ";		
//						
//		if(!Strings.isNoE(search)) query += " AND (a.kodeBarang like '%"+search.toUpperCase()+"%'"
//				+ " OR a.namaBarang like '%"+search+"%')";
//		
//		return (Long) this.entityManager.createQuery(query).getSingleResult();
//	}
//
//	@Override
//	public List<MasterForm> listAllBarangStokNotNull(String namaBd, String search, int colsNo, String order, int limit, int offset) {
//		String query = "";
//		if(!Strings.isNoE(namaBd)) {
//			query = "SELECT " + 
//					"new com.faiq.eka.form.MasterForm(a.id, a.kodeBarang, a.namaBarang, a.stok) "  
//					+ "FROM MasterBarang a "
//					+ "INNER JOIN DetailMasterBd b ON a.id = b.idBarang "
//					+ "INNER JOIN MasterBd c ON b.idBd = c.id "
//					+ "WHERE c.namaBd = '"+namaBd+"' AND b.stok != 0 ";
//		}else {
//			query = "SELECT " + 
//					"new com.faiq.eka.form.MasterForm(a.id, a.kodeBarang, a.namaBarang, a.stok) "  
//					+ "FROM MasterBarang a "
//					+ "WHERE 1=1 AND a.stok != 0 ";
//		}
//		
//		if(!Strings.isNoE(search)) query += " AND (a.kodeBarang like '%"+search.toUpperCase()+"%'"
//				+ " OR a.namaBarang like '%"+search+"%')";
//		
//		Query q = this.entityManager.createQuery(query);
//		q.setFirstResult(offset);
//		q.setMaxResults(limit);
//		return q.getResultList();
//	}

	@Override
	public Long countAllBarangMasterBd(String idBd, String search) {
		String query = "SELECT count(1) " + 
				"FROM MasterBarang a "+
				"WHERE 1=1 AND a.stok != 0 AND a.id NOT IN ( SELECT e.idBarang FROM DetailMasterBd e JOIN MasterBd c ON e.idBd = c.id WHERE c.namaBd = 'BD670') ";		
						
		if(!Strings.isNoE(search)) query += " AND (a.kodeBarang like '%"+search.toUpperCase()+"%'"
				+ " OR a.namaBarang like '%"+search+"%')";
		
		return (Long) this.entityManager.createQuery(query).getSingleResult();
	}

	@Override
	public List<MasterBarang> listAllBarangMasterBd(String idBd, String search, int colsNo, String order, int limit, int offset) {
		String query = "SELECT " + 
				"new com.faiq.eka.persistence.entity.MasterBarang(a.id, a.kodeBarang, a.namaBarang) "  
				+ "FROM MasterBarang a "
				+ "WHERE 1=1 AND a.stok != 0 AND a.id NOT IN ( SELECT e.idBarang FROM DetailMasterBd e JOIN MasterBd c ON e.idBd = c.id WHERE c.namaBd = 'BD670' ) ";
		
		if(!Strings.isNoE(search)) query += " AND (a.kodeBarang like '%"+search.toUpperCase()+"%'"
				+ " OR a.namaBarang like '%"+search+"%')";
		
		Query q = this.entityManager.createQuery(query);
		q.setFirstResult(offset);
		q.setMaxResults(limit);
		return q.getResultList();
	}

}
