package com.faiq.eka.persistence.repository;

import com.faiq.eka.persistence.entity.MasterBagian;
import com.faiq.eka.util.repository.BaseCrudRepository;
import com.faiq.eka.util.repository.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterBagianRepository extends BaseRepository<MasterBagian, Long>, BaseCrudRepository<MasterBagian,Long> {
}
