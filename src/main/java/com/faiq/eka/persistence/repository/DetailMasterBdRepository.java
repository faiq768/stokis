package com.faiq.eka.persistence.repository;

import org.springframework.stereotype.Repository;

import com.faiq.eka.persistence.entity.DetailMasterBd;
import com.faiq.eka.util.repository.BaseCrudRepository;
import com.faiq.eka.util.repository.BaseRepository;

@Repository
public interface DetailMasterBdRepository extends BaseRepository<DetailMasterBd, Long>, BaseCrudRepository<DetailMasterBd,Long> {
	DetailMasterBd findByIdBdAndIdBarang(long idBd, long idBarang);
}
