package com.faiq.eka.persistence.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="HistoryKeluarMasuk")
@Table(name="HISTORY_KELUAR_MASUK")
public class HistoryKeluarMasuk {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private long idCkm;
	private String status;
	private double stok;
	@Temporal(TemporalType.TIMESTAMP)
	private Date tanggalDibuat;
	
	public HistoryKeluarMasuk() {
		super();
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the idCkm
	 */
	public long getIdCkm() {
		return idCkm;
	}

	/**
	 * @param idCkm the idCkm to set
	 */
	public void setIdCkm(long idCkm) {
		this.idCkm = idCkm;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the stok
	 */
	public double getStok() {
		return stok;
	}

	/**
	 * @param stok the stok to set
	 */
	public void setStok(double stok) {
		this.stok = stok;
	}

	/**
	 * @return the tanggalDibuat
	 */
	public Date getTanggalDibuat() {
		return tanggalDibuat;
	}

	/**
	 * @param tanggalDibuat the tanggalDibuat to set
	 */
	public void setTanggalDibuat(Date tanggalDibuat) {
		this.tanggalDibuat = tanggalDibuat;
	}
	
}
