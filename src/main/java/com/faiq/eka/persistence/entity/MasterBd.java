package com.faiq.eka.persistence.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="MasterBd")
@Table(name="MASTER_BD")
public class MasterBd {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String namaBd;
	private String deskripsiBd;
	@Temporal(TemporalType.TIMESTAMP)
	private Date tanggal;
	
	public MasterBd() {
		super();
	}

	/**
	 * @param id
	 * @param namaBd
	 * @param deskripsiBd
	 */
	public MasterBd(long id, String namaBd, String deskripsiBd) {
		super();
		this.id = id;
		this.namaBd = namaBd;
		this.deskripsiBd = deskripsiBd;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the namaBd
	 */
	public String getNamaBd() {
		return namaBd;
	}

	/**
	 * @param namaBd the namaBd to set
	 */
	public void setNamaBd(String namaBd) {
		this.namaBd = namaBd;
	}

	/**
	 * @return the deskripsiBd
	 */
	public String getDeskripsiBd() {
		return deskripsiBd;
	}

	/**
	 * @param deskripsiBd the deskripsiBd to set
	 */
	public void setDeskripsiBd(String deskripsiBd) {
		this.deskripsiBd = deskripsiBd;
	}

	/**
	 * @param tanggal the tanggal to set
	 */
	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}
	
	
}
