package com.faiq.eka.persistence.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="DetailMasterBd")
@Table(name="DETAIL_MASTER_BD")
public class DetailMasterBd {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Long idBd;
	private Long idBarang;
	private double srJumlahBarang;
	private char realisasiManual;
	@Temporal(TemporalType.TIMESTAMP)
	private Date tanggal;
	/**
	 * 
	 */
	public DetailMasterBd() {
		super();
	}
	/**
	 * @param idBarang
	 */
	public DetailMasterBd(Long idBarang) {
		super();
		this.idBarang = idBarang;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the idBd
	 */
	public Long getIdBd() {
		return idBd;
	}
	/**
	 * @param idBd the idBd to set
	 */
	public void setIdBd(Long idBd) {
		this.idBd = idBd;
	}
	/**
	 * @return the idBarang
	 */
	public Long getIdBarang() {
		return idBarang;
	}
	/**
	 * @param idBarang the idBarang to set
	 */
	public void setIdBarang(Long idBarang) {
		this.idBarang = idBarang;
	}
	/**
	 * @return the srJumlahBarang
	 */
	public double getSrJumlahBarang() {
		return srJumlahBarang;
	}
	/**
	 * @param srJumlahBarang the srJumlahBarang to set
	 */
	public void setSrJumlahBarang(double srJumlahBarang) {
		this.srJumlahBarang = srJumlahBarang;
	}
	
	/**
	 * @return the realisasiManual
	 */
	public char getRealisasiManual() {
		return realisasiManual;
	}
	/**
	 * @param realisasiManual the realisasiManual to set
	 */
	public void setRealisasiManual(char realisasiManual) {
		this.realisasiManual = realisasiManual;
	}
	/**
	 * @return the tanggal
	 */
	public Date getTanggal() {
		return tanggal;
	}
	/**
	 * @param tanggal the tanggal to set
	 */
	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}
	
	
}
