package com.faiq.eka.persistence.entity;

import javax.persistence.*;

@Entity(name="MasterBagian")
@Table(name="MASTER_BAGIAN")
public class MasterBagian {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String deskripsi;

    public MasterBagian() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
