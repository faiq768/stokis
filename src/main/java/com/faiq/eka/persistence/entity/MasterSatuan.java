package com.faiq.eka.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "MasterSatuan")
@Table(name = "MASTER_SATUAN")
public class MasterSatuan {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String satuan;
	private String dibuatOleh;
	/**
	 * 
	 */
	public MasterSatuan() {
		super();
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the satuan
	 */
	public String getSatuan() {
		return satuan;
	}
	/**
	 * @param satuan the satuan to set
	 */
	public void setSatuan(String satuan) {
		this.satuan = satuan;
	}
	/**
	 * @return the dibuatOleh
	 */
	public String getDibuatOleh() {
		return dibuatOleh;
	}
	/**
	 * @param dibuatOleh the dibuatOleh to set
	 */
	public void setDibuatOleh(String dibuatOleh) {
		this.dibuatOleh = dibuatOleh;
	}
	
	
}
