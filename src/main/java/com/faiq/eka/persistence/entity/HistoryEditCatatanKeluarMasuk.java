package com.faiq.eka.persistence.entity;

import javax.persistence.*;

@Entity(name="HistoryEditCatatanKeluarMasuk")
@Table(name="HISTORY_EDIT_CATATAN_KELUAR_MASUK")
public class HistoryEditCatatanKeluarMasuk {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private long idCkm;

    private double jumlahBarang;

    public HistoryEditCatatanKeluarMasuk() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdCkm() {
        return idCkm;
    }

    public void setIdCkm(long idCkm) {
        this.idCkm = idCkm;
    }

    public double getJumlahBarang() {
        return jumlahBarang;
    }

    public void setJumlahBarang(double jumlahBarang) {
        this.jumlahBarang = jumlahBarang;
    }
}
