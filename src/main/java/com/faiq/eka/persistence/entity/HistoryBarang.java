package com.faiq.eka.persistence.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="HistoryBarang")
@Table(name="HISTORY_BARANG")
public class HistoryBarang {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private long idBarang;
	private double stok;
	@Temporal(TemporalType.TIMESTAMP)
	private Date tanggalDibuat;
	/**
	 * 
	 */
	public HistoryBarang() {
		super();
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the idBarang
	 */
	public long getIdBarang() {
		return idBarang;
	}
	/**
	 * @param idBarang the idBarang to set
	 */
	public void setIdBarang(long idBarang) {
		this.idBarang = idBarang;
	}
	/**
	 * @return the stok
	 */
	public double getStok() {
		return stok;
	}
	/**
	 * @param stok the stok to set
	 */
	public void setStok(double stok) {
		this.stok = stok;
	}
	/**
	 * @return the tanggalDibuat
	 */
	public Date getTanggalDibuat() {
		return tanggalDibuat;
	}
	/**
	 * @param tanggalDibuat the tanggalDibuat to set
	 */
	public void setTanggalDibuat(Date tanggalDibuat) {
		this.tanggalDibuat = tanggalDibuat;
	}
	
}
