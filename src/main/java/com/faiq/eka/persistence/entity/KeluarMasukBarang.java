package com.faiq.eka.persistence.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="KeluarMasukBarang")
@Table(name="CATATAN_KELUAR_MASUK")
public class KeluarMasukBarang {
	@Id
	private long id;
	private long idBarang;
	private long idBd;
	private double jumlahBarang;
	private String penerima;
	@Temporal(TemporalType.TIMESTAMP)
	private Date tanggalOrder;
	private String catatan;
	private long idBagian;
	private String status;
	
	public KeluarMasukBarang() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getIdBarang() {
		return idBarang;
	}

	public void setIdBarang(long idBarang) {
		this.idBarang = idBarang;
	}

	public long getIdBd() {
		return idBd;
	}

	public void setIdBd(long idBd) {
		this.idBd = idBd;
	}

	public double getJumlahBarang() {
		return jumlahBarang;
	}

	public void setJumlahBarang(double jumlahBarang) {
		this.jumlahBarang = jumlahBarang;
	}

	public String getPenerima() {
		return penerima;
	}

	public void setPenerima(String penerima) {
		this.penerima = penerima;
	}

	public Date getTanggalOrder() {
		return tanggalOrder;
	}

	public void setTanggalOrder(Date tanggalOrder) {
		this.tanggalOrder = tanggalOrder;
	}

	public String getCatatan() {
		return catatan;
	}

	public void setCatatan(String catatan) {
		this.catatan = catatan;
	}

	public long getIdBagian() {
		return idBagian;
	}

	public void setIdBagian(long idBagian) {
		this.idBagian = idBagian;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
