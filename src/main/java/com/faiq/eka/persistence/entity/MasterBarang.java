package com.faiq.eka.persistence.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="MasterBarang")
@Table(name="MASTER_BARANG")
public class MasterBarang {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String kodeBarang;
	private String namaBarang;
	private double stok;
	private long idSatuan;
	private double hargaSatuan;
	private long idIndukSatuan;
	private double stokIndukSatuan;
	private double nominalIndukSatuan;
	private char moving;
	@Temporal(TemporalType.TIMESTAMP)
	private Date tanggalDibuat;
	/**
	 * 
	 */
	public MasterBarang() {
		super();
	}

	public MasterBarang(String kodeBarang, String namaBarang, double stok, long idSatuan, double hargaSatuan, long idIndukSatuan, double stokIndukSatuan, double nominalIndukSatuan, char moving) {
		this.kodeBarang = kodeBarang;
		this.namaBarang = namaBarang;
		this.stok = stok;
		this.idSatuan = idSatuan;
		this.hargaSatuan = hargaSatuan;
		this.idIndukSatuan = idIndukSatuan;
		this.stokIndukSatuan = stokIndukSatuan;
		this.nominalIndukSatuan = nominalIndukSatuan;
		this.moving = moving;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the namaBarang
	 */
	public String getNamaBarang() {
		return namaBarang;
	}
	/**
	 * @param namaBarang the namaBarang to set
	 */
	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}
	/**
	 * @return the kodeBarang
	 */
	public String getKodeBarang() {
		return kodeBarang;
	}
	/**
	 * @param kodeBarang the kodeBarang to set
	 */
	public void setKodeBarang(String kodeBarang) {
		this.kodeBarang = kodeBarang;
	}
	
	/**
	 * @return the stok
	 */
	public double getStok() {
		return stok;
	}
	/**
	 * @param stok the stok to set
	 */
	public void setStok(double stok) {
		this.stok = stok;
	}
	/**
	 * @return the idSatuan
	 */
	public long getIdSatuan() {
		return idSatuan;
	}
	/**
	 * @param idSatuan the idSatuan to set
	 */
	public void setIdSatuan(long idSatuan) {
		this.idSatuan = idSatuan;
	}

	public double getHargaSatuan() {
		return hargaSatuan;
	}

	public void setHargaSatuan(double hargaSatuan) {
		this.hargaSatuan = hargaSatuan;
	}

	public long getIdIndukSatuan() {
		return idIndukSatuan;
	}

	public void setIdIndukSatuan(long idIndukSatuan) {
		this.idIndukSatuan = idIndukSatuan;
	}

	public double getStokIndukSatuan() {
		return stokIndukSatuan;
	}

	public void setStokIndukSatuan(double stokIndukSatuan) {
		this.stokIndukSatuan = stokIndukSatuan;
	}

	public double getNominalIndukSatuan() {
		return nominalIndukSatuan;
	}

	public void setNominalIndukSatuan(double nominalIndukSatuan) {
		this.nominalIndukSatuan = nominalIndukSatuan;
	}

	/**
	 * @return the tanggalDibuat
	 */

	public Date getTanggalDibuat() {
		return tanggalDibuat;
	}
	/**
	 * @param tanggalDibuat the tanggalDibuat to set
	 */
	public void setTanggalDibuat(Date tanggalDibuat) {
		this.tanggalDibuat = tanggalDibuat;
	}

	public char getMoving() {
		return moving;
	}

	public void setMoving(char moving) {
		this.moving = moving;
	}
}
