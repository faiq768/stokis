package com.faiq.eka.persistence.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="RealisasiManual")
@Table(name="REALISASI_MANUAL")
public class RealisasiManual {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private long idBd;
	private long idBarang;
	private double jumlahBarang;
	private long idSatuan;
	private String penerima;
	@Temporal(TemporalType.TIMESTAMP)
	private Date tanggalOrder;
	private String catatan;
	/**
	 * 
	 */
	public RealisasiManual() {
		super();
	}
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * @return the idBd
	 */
	public long getIdBd() {
		return idBd;
	}
	/**
	 * @param idBd the idBd to set
	 */
	public void setIdBd(long idBd) {
		this.idBd = idBd;
	}
	/**
	 * @return the idBarang
	 */
	public long getIdBarang() {
		return idBarang;
	}
	/**
	 * @param idBarang the idBarang to set
	 */
	public void setIdBarang(long idBarang) {
		this.idBarang = idBarang;
	}
	/**
	 * @return the jumlahBarang
	 */
	public double getJumlahBarang() {
		return jumlahBarang;
	}
	/**
	 * @param jumlahBarang the jumlahBarang to set
	 */
	public void setJumlahBarang(double jumlahBarang) {
		this.jumlahBarang = jumlahBarang;
	}
	/**
	 * @return the idSatuan
	 */
	public long getIdSatuan() {
		return idSatuan;
	}
	/**
	 * @param idSatuan the idSatuan to set
	 */
	public void setIdSatuan(long idSatuan) {
		this.idSatuan = idSatuan;
	}
	/**
	 * @return the penerima
	 */
	public String getPenerima() {
		return penerima;
	}
	/**
	 * @param penerima the penerima to set
	 */
	public void setPenerima(String penerima) {
		this.penerima = penerima;
	}
	/**
	 * @return the tanggalOrder
	 */
	public Date getTanggalOrder() {
		return tanggalOrder;
	}
	/**
	 * @param tanggalOrder the tanggalOrder to set
	 */
	public void setTanggalOrder(Date tanggalOrder) {
		this.tanggalOrder = tanggalOrder;
	}
	/**
	 * @return the catatan
	 */
	public String getCatatan() {
		return catatan;
	}
	/**
	 * @param catatan the catatan to set
	 */
	public void setCatatan(String catatan) {
		this.catatan = catatan;
	}
	
	
}
