package com.faiq.eka.persistence.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.faiq.eka.persistence.entity.*;
import com.faiq.eka.persistence.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.faiq.eka.form.MasterForm;
import com.faiq.eka.persistence.mybatis.MasterMapper;

public class DaftarKeluarMasukServiceImpl implements DaftarKeluarMasukService {
	
	@Autowired
	private MasterMapper myBatis;

	@Autowired
	private KeluarMasukBarangRepository keluarMasukBarangRepository;

	@Autowired
	private HistoryKeluarMasukRepository historyKeluarMasukRepository;

	@Override
	public Page<MasterForm> listAllKeluarMasukBarang(String search, int colsNo, String order, int limit,
			int offset) {
		Map<String, Object> map = new HashMap<>();
		Page<MasterForm> records = new PageImpl<MasterForm>(new ArrayList<MasterForm>());
		int pgNo = (offset / limit);
		List<MasterForm> list = null;
		map.put("p_search", search);
		
//		int length = limit+offset;
//		if(offset>=limit) {
//			length = length-1;
//		}
//		if(length > limit) {
//			offset = offset+1;
//			length = length+1;
//		}
		map.put("p_start", offset);
		map.put("p_offset", limit);
		
		Integer count = null;
        count = this.myBatis.countAllKeluarMasukBarang(map);
		
		Pageable pageable = new PageRequest(pgNo,limit);
		records = new PageImpl<MasterForm>(this.myBatis.listAllKeluarMasukBarang(map), pageable, count);
		return records;
	}

	@Override
	public void editDaftarKeluarMasukBarang(Long id, Long idBd, String catatan, Date tanggal, String penerima, Long idBagian) throws ParseException {
		KeluarMasukBarang dt = this.keluarMasukBarangRepository.findOne(id);
		HistoryKeluarMasuk historyKeluarMasuk = this.historyKeluarMasukRepository.findByIdCkmAndTanggalDibuat(dt.getId(),dt.getTanggalOrder());

		String[] tgl = dt.getTanggalOrder().toString().split(" ");
		String newtgl = new SimpleDateFormat("yyyy-MM-dd").format(tanggal);
		newtgl = newtgl.concat(" ").concat(tgl[1]);
		tanggal = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(newtgl);

		dt.setIdBd(idBd);
		dt.setCatatan(catatan);
		dt.setTanggalOrder(tanggal);
		dt.setPenerima(penerima);
		dt.setIdBagian(idBagian);

		this.keluarMasukBarangRepository.save(dt);
	}

	@Override
	public Page<MasterForm> listDatatableBagian(String search, int colsNo, String order, int limit, int offset) {
		Map<String, Object> map = new HashMap<>();
		Page<MasterForm> records = new PageImpl<MasterForm>(new ArrayList<MasterForm>());
		int pgNo = (offset / limit);
		List<MasterForm> list = null;
		map.put("p_search", search);
		map.put("p_start", offset);
		map.put("p_offset", limit);

		Integer count = null;
		count = this.myBatis.countListDatatableBagian(map);

		Pageable pageable = new PageRequest(pgNo,limit);
		records = new PageImpl<MasterForm>(this.myBatis.listDatatableBagian(map), pageable, count);
		return records;
	}

	@Override
	public List<KeluarMasukBarang> getDataByIdBarang(Long idBarang) {
		List<KeluarMasukBarang> keluarMasukBarangList = this.keluarMasukBarangRepository.findByIdBarang(idBarang);
		return keluarMasukBarangList;
	}

	@Override
	public KeluarMasukBarang getDataById(Long id) {
		return this.keluarMasukBarangRepository.findOne(id);
	}
}
