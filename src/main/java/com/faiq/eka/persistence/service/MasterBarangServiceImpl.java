package com.faiq.eka.persistence.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.faiq.eka.form.MasterForm;
import com.faiq.eka.persistence.entity.HistoryBarang;
import com.faiq.eka.persistence.entity.MasterBarang;
import com.faiq.eka.persistence.mybatis.MasterMapper;
import com.faiq.eka.persistence.repository.HistoryBarangRepository;
import com.faiq.eka.persistence.repository.MasterBarangRepository;
import com.faiq.eka.persistence.repository.MasterCustomRepository;

public class MasterBarangServiceImpl implements MasterBarangService {
	
	@Autowired
	private MasterCustomRepository custom;
	
	@Autowired
	private MasterBarangRepository repo;
	
	@Autowired
	private HistoryBarangRepository hisRepo;
	
	@Autowired
	private MasterMapper myBatis;

	@Override
	public Page<MasterForm> listAllBarang(String search, int colsNo, String order, int limit, int offset) {
		Map<String, Object> map = new HashMap<>();
		Page<MasterForm> records = new PageImpl<MasterForm>(new ArrayList<MasterForm>());
		int pgNo = (offset / limit);
		List<MasterForm> list = null;
		map.put("p_search", search);
		
//		int length = limit+offset;
//		if(offset>=limit) {
//			length = length-1;
//		}
//		if(length > limit) {
//			offset = offset+1;
//			length = length+1;
//		}
		map.put("p_start", offset);
		map.put("p_offset", limit);
		
		Integer count = null;
        count = this.myBatis.countAllBarang(map);
		
		Pageable pageable = new PageRequest(pgNo,limit);
		records = new PageImpl<MasterForm>(this.myBatis.listAllBarang(map), pageable, count);
		return records;
	}

	@Override
	public Optional<MasterBarang> findByKodeBarangAndNamaBarang(String kodeBarang, String namaBarang) {
		return this.findByKodeBarangAndNamaBarang(kodeBarang, namaBarang);
	}

	@Override
	public void addMasterBarang(String kodeBarang, String namaBarang, double stok, Long idSatuan, double hargaSatuan, Long idIndukSatuan, double stokIndukSatuan, double nominalIndukSatuan,char moving) {
		MasterBarang dt = new MasterBarang();
		dt.setKodeBarang(kodeBarang);
		dt.setNamaBarang(namaBarang);
		dt.setStok(stok);
		dt.setIdSatuan(idSatuan);
		dt.setHargaSatuan(hargaSatuan);
		dt.setIdIndukSatuan(idIndukSatuan);
		dt.setStokIndukSatuan(stokIndukSatuan);
		dt.setNominalIndukSatuan(nominalIndukSatuan);
		dt.setMoving(moving);
		this.repo.save(dt);
	}

	@Override
	public List<MasterBarang> getDataBarangById(Long idBarang) {
		return this.repo.findById(idBarang);
	}

	@Override
	public void editMasterBarang(Long id, String kodeBarang, String namaBarang, Long idSatuan, Long idIndukSatuan, double nominalIndukSatuan, double hargaSatuan) {
		MasterBarang dt = this.repo.findOne(id);
		double stokIndukSatuan = dt.getStok() / nominalIndukSatuan;
		dt.setKodeBarang(kodeBarang);
		dt.setNamaBarang(namaBarang);
		dt.setStokIndukSatuan(stokIndukSatuan);
		dt.setNominalIndukSatuan(nominalIndukSatuan);
		dt.setHargaSatuan(hargaSatuan);
		dt.setIdSatuan(idSatuan);
		dt.setIdIndukSatuan(idIndukSatuan);
		this.repo.save(dt);
	}

	@Override
	public void addHistoryMasterBarang(Long idBarang, double stok) {
		HistoryBarang dt = new HistoryBarang();
		dt.setIdBarang(idBarang);
		dt.setStok(stok);
		this.hisRepo.save(dt);
	}

	@Override
	public void tambahStokBarang(Long id, double stok) {
		MasterBarang dt = this.repo.findOne(id);
		dt.setStok(stok);
		this.repo.save(dt);		
	}

	@Override
	public void deleteMasterBarang(Long id) {
//		MasterBarang dt = this.repo.findOne(id);
		this.repo.delete(id);
	}

	@Override
	public void deleteHistoryMasterBarang(Long id) {
		this.hisRepo.deleteByIdBarang(id);
	}

}
