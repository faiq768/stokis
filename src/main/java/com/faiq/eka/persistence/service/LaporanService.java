package com.faiq.eka.persistence.service;

import java.io.FileNotFoundException;

import org.springframework.data.domain.Page;

import com.faiq.eka.form.MasterForm;

import net.sf.jasperreports.engine.JRException;

public interface LaporanService {
	String laporanStokOpname(String tanggal)throws FileNotFoundException, JRException;
	String laporanStokPerBd(String tanggalMulai,String tanggalAkhir, String namaBd)throws FileNotFoundException, JRException;
	String laporanHarian(String tanggal)throws FileNotFoundException, JRException;
	Page<MasterForm> listAllPenerima(String search, int colsNo, String order, int limit, int offset);
	Page<MasterForm> listAllBarangPenerima(String penerima, String search, int colsNo, String order, int limit, int offset);
	Page<MasterForm> laporanPerBarang(MasterForm masterForm, int colsNo, String order, int limit, int offset);
    Page<MasterForm> totalLaporanPerBarang(MasterForm masterForm, int colsNo, String order, int limit, int offset);
	Page<MasterForm> laporanRekapan(MasterForm masterForm, int colsNo, String order, int limit, int offset);
}
