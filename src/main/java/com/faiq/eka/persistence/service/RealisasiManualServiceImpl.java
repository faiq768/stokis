package com.faiq.eka.persistence.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.faiq.eka.form.MasterForm;
import com.faiq.eka.persistence.entity.RealisasiManual;
import com.faiq.eka.persistence.mybatis.MasterMapper;
import com.faiq.eka.persistence.repository.RealisasiManualRepository;

public class RealisasiManualServiceImpl implements RealisasiManualService {
	
	@Autowired
	private RealisasiManualRepository repo;

	@Autowired
	private MasterMapper myBatis;
	
	@Override
	public void addRealisasiManual(long idBd, long idBarang, double jumlahBarang, long idSatuan, String penerima,
			String catatan) {
		RealisasiManual dt = new RealisasiManual();
		dt.setIdBd(idBd);
		dt.setIdBarang(idBarang);
		dt.setJumlahBarang(jumlahBarang);
		dt.setIdSatuan(idSatuan);
		dt.setPenerima(penerima);
		dt.setCatatan(catatan);
		this.repo.save(dt);
	}

	@Override
	public Page<MasterForm> listAllRealisasiManual(String search, int colsNo, String order, int limit, int offset) {
		
			Map<String, Object> map = new HashMap<>();
			Page<MasterForm> records = new PageImpl<MasterForm>(new ArrayList<MasterForm>());
			int pgNo = (offset / limit);
			List<MasterForm> list = null;
			map.put("p_search", search);
			
//			int length = limit+offset;
//			if(offset>=limit) {
//				length = length-1;
//			}
//			if(length > limit) {
//				offset = offset+1;
//				length = length+1;
//			}
			map.put("p_start", offset);
			map.put("p_offset", limit);
			
			Integer count = null;
	        count = this.myBatis.countAllRealisasiManual(map);
			
			Pageable pageable = new PageRequest(pgNo,limit);
			records = new PageImpl<MasterForm>(this.myBatis.listAllRealisasiManual(map), pageable, count);
			return records;
		}

}
