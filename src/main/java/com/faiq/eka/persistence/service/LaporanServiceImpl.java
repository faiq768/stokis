package com.faiq.eka.persistence.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.util.ResourceUtils;

import com.faiq.eka.constant.ApplConstant;
import com.faiq.eka.form.MasterForm;
import com.faiq.eka.persistence.mybatis.MasterMapper;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

public class LaporanServiceImpl implements LaporanService {
	
	@Value(ApplConstant.URL_DIRECTORY)
    private String DIRECTORY_PDF;
	
	@Autowired
    DataSource dataSource;
	
	@Autowired
	private MasterMapper myBatis;
	
	@Override
	public String laporanStokOpname(String tanggal)
			throws FileNotFoundException, JRException {
		String fileName="";
		try {
			File file = ResourceUtils.getFile("classpath:jasper/STOK_OPNAME.jrxml");
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
			String path = this.DIRECTORY_PDF+"//program_eka//Stok Opname//";
						
			fileName = "Stok Opname "+tanggal;
			SimpleDateFormat formatDate = new SimpleDateFormat("dd-MMM-yy");
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("tanggal", tanggal);

			if (!Files.exists(Paths.get(path))) {
                Files.createDirectories(Paths.get(path));
	        }

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,parameters, dataSource.getConnection());
			JasperExportManager.exportReportToPdfFile(jasperPrint,path+"//"+fileName+".pdf");

		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
		return "Stok Opname/"+fileName;
	}

	@Override
	public String laporanStokPerBd(String tanggalMulai, String tanggalAkhir, String namaBd)
			throws FileNotFoundException, JRException {
		String fileName="";
		try {
			File file = ResourceUtils.getFile("classpath:jasper/STOKIS_PER_BD.jrxml");
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
			String path = this.DIRECTORY_PDF+"//program_eka//Per BD//";
			
			if(!namaBd.isEmpty()) {
				fileName = "Laporan "+namaBd+" Per Tanggal "+tanggalMulai+" SD "+tanggalAkhir;
			}else {
				fileName = "Laporan perBd Tanggal "+tanggalMulai+" SD "+tanggalAkhir;
			}
			
			SimpleDateFormat formatDate = new SimpleDateFormat("dd-MMM-yy");
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("tanggal_mulai", tanggalMulai);
			parameters.put("tanggal_akhir", tanggalAkhir);
			parameters.put("namaBd", namaBd);
			
			if (!Files.exists(Paths.get(path))) {
                Files.createDirectories(Paths.get(path));
	        }
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,parameters, dataSource.getConnection());
			JasperExportManager.exportReportToPdfFile(jasperPrint,path+"//"+fileName+".pdf");
			
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
		return "Per BD/"+fileName;
	}

	@Override
	public String laporanHarian(String tanggal) throws FileNotFoundException, JRException {
		String fileName="";
		try {
			File file = ResourceUtils.getFile("classpath:jasper/STOKIS.jrxml");
			JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
			String path = this.DIRECTORY_PDF+"//program_eka//Harian//";
						
			fileName = "Laporan Per Tanggal "+tanggal;
			SimpleDateFormat formatDate = new SimpleDateFormat("dd-MMM-yy");
			
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("tanggal", tanggal);
			
			if (!Files.exists(Paths.get(path))) {
                Files.createDirectories(Paths.get(path));
	        }
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,parameters, dataSource.getConnection());
			JasperExportManager.exportReportToPdfFile(jasperPrint,path+"//"+fileName+".pdf");
			
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
		return "Stok Opname/"+fileName;
	}

	@Override
	public Page<MasterForm> listAllPenerima(String search, int colsNo, String order, int limit, int offset) {
		Map<String, Object> map = new HashMap<>();
		Page<MasterForm> records = new PageImpl<MasterForm>(new ArrayList<MasterForm>());
		int pgNo = (offset / limit);
		List<MasterForm> list = null;
		map.put("p_search", search);
		
//		int length = limit+offset;
//		if(offset>=limit) {
//			length = length-1;
//		}
//		if(length > limit) {
//			offset = offset+1;
//			length = length+1;
//		}
		map.put("p_start", offset);
		map.put("p_offset", limit);
		
		Integer count = null;
        count = this.myBatis.countAllPenerimaDt(map);
		
		Pageable pageable = new PageRequest(pgNo,limit);
		records = new PageImpl<MasterForm>(this.myBatis.listAllPenerima(map), pageable, count);
		return records;
	}

	@Override
	public Page<MasterForm> listAllBarangPenerima(String penerima, String search, int colsNo, String order, int limit,
			int offset) {
		Map<String, Object> map = new HashMap<>();
		Page<MasterForm> records = new PageImpl<MasterForm>(new ArrayList<MasterForm>());
		int pgNo = (offset / limit);
		List<MasterForm> list = null;
		map.put("penerima", penerima);
		map.put("p_search", search);
		map.put("p_start", offset);
		map.put("p_offset", limit);
		
		Integer count = null;
        count = this.myBatis.countAllBarangPenerima(map);
		
		Pageable pageable = new PageRequest(pgNo,limit);
		records = new PageImpl<MasterForm>(this.myBatis.listAllBarangPenerima(map), pageable, count);
		return records;
	}

	@Override
	public Page<MasterForm> laporanPerBarang(MasterForm masterForm, int colsNo, String order, int limit, int offset) {
		Map<String, Object> map = new HashMap<>();
		Page<MasterForm> records = new PageImpl<MasterForm>(new ArrayList<MasterForm>());
		int pgNo = (offset / limit);
		List<MasterForm> list = null;
		map.put("idBarang", masterForm.getIdBarang());
		map.put("tanggalMulai", masterForm.getTanggalMulai());
		map.put("tanggalAkhir", masterForm.getTanggalAkhir());
		map.put("p_start", offset);
		map.put("p_offset", limit);

		Integer count = null;
		count = this.myBatis.countLaporanPerBarang(map);

		Pageable pageable = new PageRequest(pgNo,limit);
		records = new PageImpl<MasterForm>(this.myBatis.laporanPerBarang(map), pageable, count);
		return records;
	}

    @Override
    public Page<MasterForm> totalLaporanPerBarang(MasterForm masterForm, int colsNo, String order, int limit, int offset) {
        Map<String, Object> map = new HashMap<>();
        Page<MasterForm> records = new PageImpl<MasterForm>(new ArrayList<MasterForm>());
        int pgNo = (offset / limit);
        List<MasterForm> list = null;
        map.put("idBarang", masterForm.getIdBarang());
        map.put("tanggalMulai", masterForm.getTanggalMulai());
        map.put("tanggalAkhir", masterForm.getTanggalAkhir());
        map.put("p_start", offset);
        map.put("p_offset", limit);

        Integer count = null;
        count = this.myBatis.countTotalLaporanPerBarang(map);

        Pageable pageable = new PageRequest(pgNo,limit);
        records = new PageImpl<MasterForm>(this.myBatis.totalLaporanPerBarang(map), pageable, count);
        return records;
    }

	@Override
	public Page<MasterForm> laporanRekapan(MasterForm masterForm, int colsNo, String order, int limit, int offset) {
		Map<String, Object> map = new HashMap<>();
		Page<MasterForm> records = new PageImpl<MasterForm>(new ArrayList<MasterForm>());
		int pgNo = (offset / limit);
		List<MasterForm> list = null;
		map.put("tanggalMulai", masterForm.getTanggalMulai());
		map.put("tanggalAkhir", masterForm.getTanggalAkhir());
		map.put("p_start", offset);
		map.put("p_offset", limit);

		Integer count = null;
		count = this.myBatis.countLaporanRekapan(map);

		Pageable pageable = new PageRequest(pgNo,limit);
		records = new PageImpl<MasterForm>(this.myBatis.laporanRekapan(map), pageable, count);
		return records;
	}
}
