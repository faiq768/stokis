package com.faiq.eka.persistence.service;

import com.faiq.eka.form.MasterDashboardForm;
import com.faiq.eka.persistence.mybatis.MasterDashboard;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MasterDashboardServiceImpl implements MasterDashboardService {

    @Autowired
    private MasterDashboard masterDashboard;

    @Override
    public List<MasterDashboardForm> getTotalPengeluaranByTahun(String tahun) {

        List<MasterDashboardForm> masterDashboardForms = new ArrayList<MasterDashboardForm>();
        Map<String, Object> map = new HashMap<>();
        map.put("tahun",tahun);
        this.masterDashboard.getTotalPengeluaranByYear(map).forEach(action->masterDashboardForms.add(action));
        return masterDashboardForms;
    }

    @Override
    public List<MasterDashboardForm> getTotalPengeluaranByBulanAndTahun(String tahun, String bulan) {
        List<MasterDashboardForm> masterDashboardForms = new ArrayList<MasterDashboardForm>();
        Map<String, Object> map = new HashMap<>();
        map.put("tahun",tahun);
        map.put("bulan",bulan);
        this.masterDashboard.getTotalPengeluaranByYearAndMonth(map).forEach(action->masterDashboardForms.add(action));
        return masterDashboardForms;
    }

    @Override
    public List<MasterDashboardForm> getTotalPengeluaranByBulanAndTahunAndNamaBarang(String tahun, String bulan, String namaBarang) {
        List<MasterDashboardForm> masterDashboardForms = new ArrayList<MasterDashboardForm>();
        Map<String, Object> map = new HashMap<>();
        map.put("tahun",tahun);
        map.put("bulan",bulan);
        map.put("namaBarang",namaBarang);
        this.masterDashboard.getTotalPengeluaranByYearAndMonthAndNamaBarang(map).forEach(action->masterDashboardForms.add(action));
//        for (int i = 0;i<masterDashboardForms.size();i++){
//            System.out.println(masterDashboardForms.get(i).getNamaBarang());
//        }
        System.out.println("total = "+masterDashboardForms.size());
        return masterDashboardForms;
    }
}
