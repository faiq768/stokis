package com.faiq.eka.persistence.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Modifying;

import com.faiq.eka.form.MasterForm;
import com.faiq.eka.persistence.entity.MasterBarang;


public interface MasterBarangService {
	Page<MasterForm> listAllBarang(String search, int colsNo, String order, int limit, int offset);
	Optional<MasterBarang> findByKodeBarangAndNamaBarang(String kodeBarang, String namaBarang);
	List<MasterBarang> getDataBarangById(Long idBarang);
	void addMasterBarang(String kodeBarang, String namaBarang, double stok, Long idSatuan, double hargaSatuan, Long idIndukSatuan, double stokIndukSatuan, double nominalIndukSatuan, char moving);
	void addHistoryMasterBarang(Long idBarang, double stok);
	void tambahStokBarang(Long id, double stok);
	void editMasterBarang(Long id, String kodeBarang, String namaBarang, Long idSatuan, Long idIndukSatuan, double nominalIndukSatuan, double hargaSatuan);
	void deleteMasterBarang(Long id);
	void deleteHistoryMasterBarang(Long id);
}
