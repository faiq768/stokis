package com.faiq.eka.persistence.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.faiq.eka.form.MasterForm;
import com.faiq.eka.persistence.entity.HistoryKeluarMasuk;
import com.faiq.eka.persistence.entity.KeluarMasukBarang;
import com.faiq.eka.persistence.entity.MasterBarang;
import com.faiq.eka.persistence.mybatis.MasterMapper;
import com.faiq.eka.persistence.repository.HistoryKeluarMasukRepository;
import com.faiq.eka.persistence.repository.KeluarMasukBarangRepository;
import com.faiq.eka.persistence.repository.MasterCustomRepository;

public class KeluarMasukBarangServiceImpl implements KeluarMasukBarangService {
	
	@Autowired
	private MasterCustomRepository custom;
	
	@Autowired
	private KeluarMasukBarangRepository repo;
	
	@Autowired
	private HistoryKeluarMasukRepository repoHist;
	
	@Autowired
	private MasterMapper myBatis;

	@Override
	public Page<MasterForm> listAllBarangStokNotNull(String namaBd, String search, int colsNo, String order, int limit, int offset) {
		
		Map<String, Object> map = new HashMap<>();
		Page<MasterForm> records = new PageImpl<MasterForm>(new ArrayList<MasterForm>());
		int pgNo = (offset / limit);
		List<MasterForm> list = null;
		map.put("p_search", search);
		map.put("namaBd",namaBd);
		
//		int length = limit+offset;
//		if(offset>=limit) {
//			length = length-1;
//		}
//		if(length > limit) {
//			offset = offset+1;
//			length = length+1;
//		}
		map.put("p_start", offset);
		map.put("p_offset", limit);
		
		Integer count = null;
		if(namaBd.length() > 0) {
			count = this.myBatis.countAllBarangStokNotNullAndNamaBdNotNull(map);
			Pageable pageable = new PageRequest(pgNo,limit);
			records = new PageImpl<MasterForm>(this.myBatis.listAllBarangStokNotAndNamaBdNotNull(map), pageable, count);
		}else {
			count = this.myBatis.countAllBarangStokNotNullAndNamaBdNull(map);
			Pageable pageable = new PageRequest(pgNo,limit);
			records = new PageImpl<MasterForm>(this.myBatis.listAllBarangStokNotNullAndNamaBdNull(map), pageable, count);
		}
		
		return records;
	}

	@Override
	public void addKeluarMasukBarang(Long id, Long idBarang, Long idBd, double stok, String penerima, String catatan, long idBagian, String status) {
		KeluarMasukBarang dt = new KeluarMasukBarang();
		dt.setId(id);
		dt.setIdBarang(idBarang);
		dt.setIdBd(idBd);
		dt.setJumlahBarang(stok);
		dt.setPenerima(penerima);
		dt.setCatatan(catatan);
		dt.setIdBagian(idBagian);
		dt.setStatus(status);
		this.repo.save(dt);
		
	}

	@Override
	public void addHistoryKeluarMasuk(Long id, String status, double stok) {
		HistoryKeluarMasuk dt = new HistoryKeluarMasuk();
		dt.setIdCkm(id);
		dt.setStatus(status);
		dt.setStok(stok);
		this.repoHist.save(dt);
	}

	@Override
	public KeluarMasukBarang findLastId() {
		KeluarMasukBarang dt = this.repo.findTopByOrderByIdDesc();
		return dt;
	}

	@Override
	public void saveEditKeluarMasukBarang(KeluarMasukBarang keluarMasukBarang) {
		this.repo.save(keluarMasukBarang);
	}
}
