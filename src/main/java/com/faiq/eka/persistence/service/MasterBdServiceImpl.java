package com.faiq.eka.persistence.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.faiq.eka.form.MasterForm;
import com.faiq.eka.persistence.entity.DetailMasterBd;
import com.faiq.eka.persistence.entity.MasterBarang;
import com.faiq.eka.persistence.entity.MasterBd;
import com.faiq.eka.persistence.mybatis.MasterMapper;
import com.faiq.eka.persistence.repository.DetailMasterBdRepository;
import com.faiq.eka.persistence.repository.MasterBdRepository;
import com.faiq.eka.persistence.repository.MasterCustomRepository;

public class MasterBdServiceImpl implements MasterBdService{
	
	@Autowired
	private MasterBdRepository repo;
	
	@Autowired
	private MasterCustomRepository custom;
	
	@Autowired
	private DetailMasterBdRepository dmbRepo;
	
	@Autowired
	private MasterMapper myBatis;

	@Override
	public Optional<MasterBd> findByNamaBd(String namaBd) {
		return this.repo.findByNamaBdIgnoreCase(namaBd);
	}

	@Override
	public MasterBd addMasterBd(String namaBd, String deskripsiBd) {
		MasterBd dt = new MasterBd();
		dt.setNamaBd(namaBd);
		dt.setDeskripsiBd(deskripsiBd);
		return this.repo.save(dt);
	}

	@Override
	public MasterBd editMasterBd(Long id, String namaBd, String deskripsiBd) {
		MasterBd dt = this.repo.findOne(id);
		dt.setNamaBd(namaBd);
		dt.setDeskripsiBd(deskripsiBd);
		return this.repo.save(dt);
	}

	@Override
	public void deleteMasterBd(Long id) {
		MasterBd dt = this.repo.findOne(id);
		this.repo.delete(dt);
	}
	
	@Override
	public Page<MasterBd> listAllBd(String search, int colsNo, String order, int limit, int offset) {
		Page<MasterBd> records = new PageImpl<MasterBd>(new ArrayList<MasterBd>());
        int pgNo = (offset / limit);
        long total = this.custom.countAllBd(search);

        Pageable pageable = new PageRequest(pgNo, limit);
        records = new PageImpl<MasterBd>(this.custom.listAllBd(search, colsNo, order, limit, offset), pageable, total);
        return records;
	}

	@Override
	public Page<MasterForm> listBarangMasterBd(Long idBd, String search, int colsNo, String order, int limit,
			int offset) {
		Map<String, Object> map = new HashMap<>();
		Page<MasterForm> records = new PageImpl<MasterForm>(new ArrayList<MasterForm>());
		int pgNo = (offset / limit);
		List<MasterForm> list = null;
		map.put("p_search", search);
		map.put("idBd",idBd);
		
//		int length = limit+offset;
//		if(offset>=limit) {
//			length = length-1;
//		}
//		if(length > limit) {
//			offset = offset+1;
//			length = length+1;
//		}
		map.put("p_start", offset);
		map.put("p_offset", limit);
		
		Integer count = null;
        count = this.myBatis.getCountlistBarangMasterBd(map);
		
		Pageable pageable = new PageRequest(pgNo,limit);
		records = new PageImpl<MasterForm>(this.myBatis.listBarangMasterBd(map), pageable, count);
		return records;
	}

	@Override
	public Page<MasterForm> listDetailMasterBd(Long idBd, String search, int colsNo, String order, int limit,
			int offset) {
		Map<String, Object> map = new HashMap<>();
		Page<MasterForm> records = new PageImpl<MasterForm>(new ArrayList<MasterForm>());
		int pgNo = (offset / limit);
		List<MasterForm> list = null;
		map.put("p_search", search);
		map.put("idBd",idBd);
		
		int length = limit+offset;
		if(offset>=limit) {
			length = length-1;
		}
		if(length > limit) {
			offset = offset+1;
			length = length+1;
		}
		map.put("p_start", offset);
		map.put("p_offset", length);
		
		Integer count = null;
        count = this.myBatis.getCountlistDetailMasterBd(map);
		
		Pageable pageable = new PageRequest(pgNo,limit);
		records = new PageImpl<MasterForm>(this.myBatis.listDetailMasterBd(map), pageable, count);
		return records;
	}

	@Override
	public Page<MasterForm> listHistoryBarangDetailMasterBd(Long idBd, Long idBarang, String search, int colsNo, String order, int limit, int offset) {
		Map<String, Object> map = new HashMap<>();
		Page<MasterForm> records = new PageImpl<MasterForm>(new ArrayList<MasterForm>());
		int pgNo = (offset / limit);
		List<MasterForm> list = null;
		map.put("p_search", search);
		map.put("idBd",idBd);
		map.put("idBarang",idBarang);

		int length = limit+offset;
		if(offset>=limit) {
			length = length-1;
		}
		if(length > limit) {
			offset = offset+1;
			length = length+1;
		}
		map.put("p_start", offset);
		map.put("p_offset", length);

		Integer count = null;
		count = this.myBatis.getCountlistHistoryBarangDetailMasterBd(map);

		Pageable pageable = new PageRequest(pgNo,limit);
		records = new PageImpl<MasterForm>(this.myBatis.listHistoryBarangDetailMasterBd(map), pageable, count);
		return records;
	}

	@Override
	public DetailMasterBd addDetailMasterBd(Long idBd, Long idBarang, double jumlah) {
		DetailMasterBd  dt = new DetailMasterBd();
		dt.setIdBd(idBd);
		dt.setIdBarang(idBarang);
		dt.setSrJumlahBarang(jumlah);
		dt.setRealisasiManual('N');
		return this.dmbRepo.save(dt);
	}

	@Override
	public void deleteDetailMasterBd(Long id) {
		DetailMasterBd dt = this.dmbRepo.findOne(id);
		this.dmbRepo.delete(dt);
	}

	@Override
	public void updateRealisasiManualPerBd(long idBd, long idBarang, char realisasiManual) {
		DetailMasterBd dt = this.dmbRepo.findByIdBdAndIdBarang(idBd, idBarang);
		dt.setRealisasiManual(realisasiManual);
		this.dmbRepo.save(dt);
	}
	
}
