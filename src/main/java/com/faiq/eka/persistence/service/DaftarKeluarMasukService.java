package com.faiq.eka.persistence.service;

import com.faiq.eka.persistence.entity.KeluarMasukBarang;
import org.springframework.data.domain.Page;

import com.faiq.eka.form.MasterForm;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface DaftarKeluarMasukService {
	Page<MasterForm> listAllKeluarMasukBarang(String search, int colsNo, String order, int limit, int offset);
	void editDaftarKeluarMasukBarang(Long id, Long idBd, String catatan, Date tanggal, String penerima, Long idBagian) throws ParseException;
	Page<MasterForm> listDatatableBagian(String search, int colsNo, String order, int limit, int offset);
	List<KeluarMasukBarang> getDataByIdBarang(Long idBarang);
	KeluarMasukBarang getDataById(Long id);
}
