package com.faiq.eka.persistence.service;

import com.faiq.eka.form.MasterDashboardForm;

import java.util.List;

public interface MasterDashboardService {
    List<MasterDashboardForm> getTotalPengeluaranByTahun(String tahun);
    List<MasterDashboardForm> getTotalPengeluaranByBulanAndTahun(String tahun, String bulan);
    List<MasterDashboardForm> getTotalPengeluaranByBulanAndTahunAndNamaBarang(String tahun, String bulan, String namaBarang);
}
