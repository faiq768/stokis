package com.faiq.eka.persistence.service;

import org.springframework.data.domain.Page;

import com.faiq.eka.form.MasterForm;

public interface RealisasiManualService {
	void addRealisasiManual(long idBd, long idBarang, double jumlahBarang, long idSatuan, String penerima, String catatan);
	Page<MasterForm> listAllRealisasiManual(String search, int colsNo, String order, int limit, int offset);
}
