package com.faiq.eka.persistence.service;

import com.faiq.eka.persistence.entity.HistoryEditCatatanKeluarMasuk;

public interface HistoryEditCatatanKeluarMasukService {

    HistoryEditCatatanKeluarMasuk getDataByIdCkm(Long idCkm);
    void addHistoryEditCatatanKeluarMasuk(Long idCkm, Double jumlahBarang);

}
