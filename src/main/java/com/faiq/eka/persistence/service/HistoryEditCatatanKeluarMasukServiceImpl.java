package com.faiq.eka.persistence.service;

import com.faiq.eka.persistence.entity.HistoryEditCatatanKeluarMasuk;
import com.faiq.eka.persistence.repository.HistoryEditCatatanKeluarMasukRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class HistoryEditCatatanKeluarMasukServiceImpl implements HistoryEditCatatanKeluarMasukService {

    @Autowired
    private HistoryEditCatatanKeluarMasukRepository historyEditCatatanKeluarMasukRepository;

    @Override
    public HistoryEditCatatanKeluarMasuk getDataByIdCkm(Long idCkm) {
        return this.historyEditCatatanKeluarMasukRepository.findByIdCkm(idCkm);
    }

    @Override
    public void addHistoryEditCatatanKeluarMasuk(Long idCkm, Double jumlahBarang) {
        HistoryEditCatatanKeluarMasuk historyEditCatatanKeluarMasuk = new HistoryEditCatatanKeluarMasuk();
        historyEditCatatanKeluarMasuk.setIdCkm(idCkm);
        historyEditCatatanKeluarMasuk.setJumlahBarang(jumlahBarang);
        this.historyEditCatatanKeluarMasukRepository.save(historyEditCatatanKeluarMasuk);
    }
}
