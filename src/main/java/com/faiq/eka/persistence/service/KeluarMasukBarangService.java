package com.faiq.eka.persistence.service;

import org.springframework.data.domain.Page;

import com.faiq.eka.form.MasterForm;
import com.faiq.eka.persistence.entity.KeluarMasukBarang;
import com.faiq.eka.persistence.entity.MasterBarang;
import com.faiq.eka.persistence.entity.MasterBd;

public interface KeluarMasukBarangService {
	Page<MasterForm> listAllBarangStokNotNull(String namaBd, String search, int colsNo, String order, int limit, int offset);
	void addKeluarMasukBarang(Long id, Long idBarang, Long idBd, double stok, String penerima, String catatan, long idBagian, String status);
	void addHistoryKeluarMasuk(Long id, String status, double stok);
	KeluarMasukBarang findLastId();
	void saveEditKeluarMasukBarang(KeluarMasukBarang keluarMasukBarang);
}
