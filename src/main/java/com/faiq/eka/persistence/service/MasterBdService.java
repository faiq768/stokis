package com.faiq.eka.persistence.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.faiq.eka.form.MasterForm;
import com.faiq.eka.persistence.entity.DetailMasterBd;
import com.faiq.eka.persistence.entity.MasterBarang;
import com.faiq.eka.persistence.entity.MasterBd;

public interface MasterBdService {

	Optional<MasterBd> findByNamaBd(String namaBd);
	
	MasterBd addMasterBd(String namaBd, String deskripsiBd);
	DetailMasterBd addDetailMasterBd(Long idBd, Long idBarang, double jumlah);
	MasterBd editMasterBd(Long id, String namaBd, String deskripsiBd);
	void deleteMasterBd(Long id);
	void deleteDetailMasterBd(Long id);
	void updateRealisasiManualPerBd(long idBd, long idBarang, char realisasiManual);
	Page<MasterBd> listAllBd(String search, int colsNo, String order, int limit, int offset);
	Page<MasterForm> listBarangMasterBd(Long idBd, String search, int colsNo, String order, int limit, int offset);
	Page<MasterForm> listDetailMasterBd(Long idBd, String search, int colsNo, String order, int limit, int offset);
	Page<MasterForm> listHistoryBarangDetailMasterBd(Long idBd, Long idBarang, String search, int colsNo, String order, int limit, int offset);
}
