package com.faiq.eka.form;

public class MasterForm {
	private Long id;
	private Long idBd;
	private Long idBarang;
	private String namaBarang;
	private String deskripsi;
	private double jumlah;
	private double realisasiJumlah;
	private String namaBd;
	private String kodeBarang;
	private double stok;
	private long idSatuan;
	private long idIndukSatuan;
	private long idBagian;
	private String bagian;
	private String hargaSatuan;
	private String indukSatuan;
	private String stokIndukSatuan;
	private String nominalIndukSatuan;
	private String moving;
	private String tanggal;
	private String tanggalMulai;
	private String tanggalAkhir;
	private String satuan;
	private String penerima;
	private String catatan;
	private String status;
	private Integer pageNo = 1;
    private Integer startPage;
    private Integer endPage;
    private Integer rowPerPage = 10;
    private Integer number;

	public MasterForm() {
		super();
	}

	public MasterForm(Long id, String kodeBarang, String namaBarang, int stok) {
		super();
		this.id = id;
		this.namaBarang = namaBarang;
		this.kodeBarang = kodeBarang;
		this.stok = stok;
	}
	

	public MasterForm(Long idBarang, String namaBarang, String kodeBarang, double stok, String satuan) {
		this.idBarang = idBarang;
		this.namaBarang = namaBarang;
		this.kodeBarang = kodeBarang;
		this.stok = stok;
		this.satuan = satuan;
	}

	public MasterForm(Long id, Long idBd, Long idBarang, String namaBarang, String deskripsi, double jumlah,
			int realisasiJumlah, String namaBd, String kodeBarang, double stok) {
		super();
		this.id = id;
		this.idBd = idBd;
		this.idBarang = idBarang;
		this.namaBarang = namaBarang;
		this.deskripsi = deskripsi;
		this.jumlah = jumlah;
		this.realisasiJumlah = realisasiJumlah;
		this.namaBd = namaBd;
		this.kodeBarang = kodeBarang;
		this.stok = stok;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdBd() {
		return idBd;
	}

	public void setIdBd(Long idBd) {
		this.idBd = idBd;
	}

	public Long getIdBarang() {
		return idBarang;
	}

	public void setIdBarang(Long idBarang) {
		this.idBarang = idBarang;
	}

	public String getNamaBarang() {
		return namaBarang;
	}

	public void setNamaBarang(String namaBarang) {
		this.namaBarang = namaBarang;
	}

	public String getDeskripsi() {
		return deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public double getJumlah() {
		return jumlah;
	}

	public void setJumlah(double jumlah) {
		this.jumlah = jumlah;
	}

	public double getRealisasiJumlah() {
		return realisasiJumlah;
	}

	public void setRealisasiJumlah(double realisasiJumlah) {
		this.realisasiJumlah = realisasiJumlah;
	}

	public String getNamaBd() {
		return namaBd;
	}

	public void setNamaBd(String namaBd) {
		this.namaBd = namaBd;
	}

	public String getKodeBarang() {
		return kodeBarang;
	}

	public void setKodeBarang(String kodeBarang) {
		this.kodeBarang = kodeBarang;
	}

	public double getStok() {
		return stok;
	}

	public void setStok(double stok) {
		this.stok = stok;
	}

	public long getIdSatuan() {
		return idSatuan;
	}

	public void setIdSatuan(long idSatuan) {
		this.idSatuan = idSatuan;
	}

	public long getIdIndukSatuan() {
		return idIndukSatuan;
	}

	public void setIdIndukSatuan(long idIndukSatuan) {
		this.idIndukSatuan = idIndukSatuan;
	}

	public long getIdBagian() {
		return idBagian;
	}

	public void setIdBagian(long idBagian) {
		this.idBagian = idBagian;
	}

	public String getBagian() {
		return bagian;
	}

	public void setBagian(String bagian) {
		this.bagian = bagian;
	}

	public String getHargaSatuan() {
		return hargaSatuan;
	}

	public void setHargaSatuan(String hargaSatuan) {
		this.hargaSatuan = hargaSatuan;
	}

	public String getIndukSatuan() {
		return indukSatuan;
	}

	public void setIndukSatuan(String indukSatuan) {
		this.indukSatuan = indukSatuan;
	}

	public String getStokIndukSatuan() {
		return stokIndukSatuan;
	}

	public void setStokIndukSatuan(String stokIndukSatuan) {
		this.stokIndukSatuan = stokIndukSatuan;
	}

	public String getNominalIndukSatuan() {
		return nominalIndukSatuan;
	}

	public void setNominalIndukSatuan(String nominalIndukSatuan) {
		this.nominalIndukSatuan = nominalIndukSatuan;
	}

	public String getMoving() {
		return moving;
	}

	public void setMoving(String moving) {
		this.moving = moving;
	}

	public String getTanggal() {
		return tanggal;
	}

	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}

	public String getTanggalMulai() {
		return tanggalMulai;
	}

	public void setTanggalMulai(String tanggalMulai) {
		this.tanggalMulai = tanggalMulai;
	}

	public String getTanggalAkhir() {
		return tanggalAkhir;
	}

	public void setTanggalAkhir(String tanggalAkhir) {
		this.tanggalAkhir = tanggalAkhir;
	}

	public String getSatuan() {
		return satuan;
	}

	public void setSatuan(String satuan) {
		this.satuan = satuan;
	}

	public String getPenerima() {
		return penerima;
	}

	public void setPenerima(String penerima) {
		this.penerima = penerima;
	}

	public String getCatatan() {
		return catatan;
	}

	public void setCatatan(String catatan) {
		this.catatan = catatan;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getStartPage() {
		return startPage;
	}

	public void setStartPage(Integer startPage) {
		this.startPage = startPage;
	}

	public Integer getEndPage() {
		return endPage;
	}

	public void setEndPage(Integer endPage) {
		this.endPage = endPage;
	}

	public Integer getRowPerPage() {
		return rowPerPage;
	}

	public void setRowPerPage(Integer rowPerPage) {
		this.rowPerPage = rowPerPage;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}
}
