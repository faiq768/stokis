package com.faiq.eka.form;

public class MasterDashboardForm {
    private String tahun;
    private String bulan;
    private String jumlahBarang;
    private String satuan;
    private String totalPengeluaran;
    private String namaBarang;
    private String tanggalOrder;

    public MasterDashboardForm() {
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getBulan() {
        return bulan;
    }

    public void setBulan(String bulan) {
        this.bulan = bulan;
    }

    public String getJumlahBarang() {
        return jumlahBarang;
    }

    public void setJumlahBarang(String jumlahBarang) {
        this.jumlahBarang = jumlahBarang;
    }

    public String getTotalPengeluaran() {
        return totalPengeluaran;
    }

    public void setTotalPengeluaran(String totalPengeluaran) {
        this.totalPengeluaran = totalPengeluaran;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public String getTanggalOrder() {
        return tanggalOrder;
    }

    public void setTanggalOrder(String tanggalOrder) {
        this.tanggalOrder = tanggalOrder;
    }

    public String getSatuan() {
        return satuan;
    }

    public void setSatuan(String satuan) {
        this.satuan = satuan;
    }
}
