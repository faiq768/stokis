package com.faiq.eka.configuration;

import java.util.Locale;

import javax.sql.DataSource;

import com.faiq.eka.persistence.service.*;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.tomcat.util.http.LegacyCookieProcessor;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.*;

import com.faiq.eka.persistence.repository.MasterCustomRepository;
import com.faiq.eka.persistence.repository.MasterCustomRepositoryImpl;


public class BeanConfiguration extends WebMvcConfigurerAdapter {

	@Bean
	public EmbeddedServletContainerCustomizer customizer() {
		return container -> {
			if (container instanceof TomcatEmbeddedServletContainerFactory) {
				TomcatEmbeddedServletContainerFactory tomcat = (TomcatEmbeddedServletContainerFactory) container;
				tomcat.addContextCustomizers(context -> context.setCookieProcessor(new LegacyCookieProcessor()));
			}
		};
	}
	
	@Bean
    public SqlSessionFactory sqlSessionFactory(DataSource dataSource, ApplicationContext applicationContext)
            throws Exception {
        SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource);
        sessionFactoryBean.setMapperLocations(applicationContext.getResources("classpath:mybatis/mapper/*.xml"));
        return sessionFactoryBean.getObject();
    }

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**")
				.addResourceLocations("/resources/");
	}
	
	@Bean
	public MasterCustomRepository masterCustomRepository(){
		return new MasterCustomRepositoryImpl();
	}
	
	@Bean
	public MasterBdService masterBdService() {
		return new MasterBdServiceImpl();
	}
	
	@Bean
	public MasterBarangService masterBarangService() {
		return new MasterBarangServiceImpl();
	}
	
	@Bean
	public KeluarMasukBarangService keluarMasukBarangService() {
		return new KeluarMasukBarangServiceImpl();
	}
	
	@Bean
	public RealisasiManualService realisasiManualService() {
		return new RealisasiManualServiceImpl();
	}
	
	@Bean
	public DaftarKeluarMasukService daftarKeluarMasukService() {
		return new DaftarKeluarMasukServiceImpl();
	}
	
	@Bean
	public LaporanService laporanService() {
		return new LaporanServiceImpl();
	}

	@Bean
	public HistoryEditCatatanKeluarMasukService historyEditCatatanKeluarMasukService(){return new HistoryEditCatatanKeluarMasukServiceImpl();}

	@Bean
	public MasterDashboardService masterDashboardService(){return new MasterDashboardServiceImpl();}
}
