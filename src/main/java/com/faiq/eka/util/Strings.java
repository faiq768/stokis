package com.faiq.eka.util;

public class Strings {
	public static boolean isNoE(String string) {
		if(null==string) return true;
		else if("".equals(string.replaceAll(" ", ""))) return true;
		return false;
	}
	
	public static String toCamelCase(String value){
        String retValue = null;
        if(!Strings.isNoE(value)){
           retValue = "";
           String[] data = value.split(" ");
           for (int i = 0; i < data.length; i++) {
                   String string = data[i];
               String temp = "";
               if(string.length()>0){
                  String prefix = String.valueOf(string.charAt(0));
                  temp = temp+prefix.toUpperCase();
                  temp = temp+string.substring(1).toLowerCase();
                  temp = Strings.clearingStrip(temp);
               }
               retValue = retValue+temp+" ";
           }
           if(retValue.endsWith(" ")){
             retValue = retValue.substring(0,retValue.length()-1);
           }
        }
        return retValue;
    }
	
	public static String clearingStrip(String string) {
        if (string.contains("-")) {
            String[] strings = string.split("-");
            if (strings.length > 0) {
                for (int j = 0; j < strings.length; j++) {
                    String dataTemp = strings[j];
                    String prefix = String.valueOf(dataTemp.charAt(0)).toUpperCase();
                    if (j == 0) {
                        string = prefix + dataTemp.substring(1).toLowerCase() + "-";
                    } else if (j == strings.length - 1) {
                        string = string + prefix + dataTemp.substring(1).toLowerCase();
                    } else {
                        string = string + prefix + dataTemp.substring(1).toLowerCase() + "-";
                    }
                }
            }
        }
        return string;
    }
}
