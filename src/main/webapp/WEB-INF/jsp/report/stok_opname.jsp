<!DOCTYPE html>
<%@page import="com.faiq.eka.constant.ApplConstant"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String resourceLink = "/"; %>

<html lang="en">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="muh nailur rohman faiq">
    <meta name="keyword" content="Dashboard">

    <title>EKA STOKIS APPS</title>
    <jsp:include page="../general/header_import.jsp"></jsp:include>
</head>
<body class="theme-blue-grey">

<jsp:include page="../general/header.jsp"></jsp:include>
<jsp:include page="../general/menu.jsp">
	<jsp:param name="menuact" value="stokOpname" />
</jsp:include>
<section class="content">
    <div class="container-fluid">
        <ol class="breadcrumb breadcrumb-bg-blue">
            <li><a href="javascript:void(0);"><i class="fa fa-home fa-lg m-t-10" aria-hidden="true"></i> Home</a></li>
            <li class="active"><i class="fa fa-archive fa-lg m-t-10" aria-hidden="true"></i> Laporan Stok Opname</li>
        </ol>

        <div class="row clearfix">
            <div class="col-lg-12">
            	<div class="card">
            		<div class="header bg-blue">
                         <h2>
                             Stok Opname
                         </h2>
                     </div>
                     <div class="body">
                     	<div class="row">
                     		<div class="col-lg-3">
                     			<label>Per Tanggal</label>
                     			<div class="form-line" id="bs_datepicker_container">
                                    <input type="text" id="tanggal" class="form-control align-center" placeholder="Pilih Tanggal">
                                </div>
		            		</div>
		            		<div class="col-lg-1">
		            			<button type="button" id="cetakStokOpname" class="btn bg-blue m-t-25">Cetak</button>
		            		</div>
                     	</div>
                     </div>
            	</div>
            </div>
        </div>
    </div>
</section>
 	
 	<jsp:include page="../general/bottom_import.jsp"></jsp:include>
<script type="text/javascript">
	$(document).ready(function(){
		//Bootstrap datepicker plugin
	    $('#bs_datepicker_container input').datepicker({
	    	format: 'dd-mm-yyyy',
	        autoclose: true,
	        container: '#bs_datepicker_container'
	    });
		
		$("#cetakStokOpname").click(function(e){
			e.preventDefault();
			if($("#tanggal").val().length == 0){
				$("#tanggal").focus();
				return false;
			}
			Swal.fire({
		      	  title: 'Are you sure?',
		      	  icon: 'warning',
		      	  showCancelButton: true,
		      	  confirmButtonColor: '#3085d6',
		      	  cancelButtonColor: '#d33',
		      	  confirmButtonText: 'Cetak',
		      	  cancelButtonText: 'Batal'
		      	}).then((result) => {
		      	  if (result.isConfirmed) {
		      		// $(".page-loader-wrapper").css("display","");
                    var link1 = "<%= ApplConstant.URL_LAPORAN_STOK_OPNAME_CETAK.concat("?pTanggal=")%>"+$("#tanggal").val();
                    window.open(link1,'_blank');

			    	// var datas = {
			    	// 		"catatan": $("#tanggal").val()
		            //     }
		      		
		      		<%--$.ajax({--%>
		              	<%--type: 'POST',--%>
		              	<%--contentType: "application/json",--%>
		              	<%--url: "<%= ApplConstant.URL_LAPORAN_STOK_OPNAME_CETAK%>",--%>
		              	<%--data: JSON.stringify(datas),--%>
		                  <%--dataType: 'json',--%>
		                  <%--cache: false,--%>
		              	<%--success : function(data){--%>
		              		<%----%>
		              	  <%--$(".page-loader-wrapper").css("display","none");--%>
		              		<%--Swal.fire({--%>
		            			  <%--icon: 'success',--%>
		            			  <%--text: "Berhasil Dicetak"--%>
		            		<%--});--%>
		              		<%----%>
		              		<%--$("#tanggal").val("");--%>
		              		<%--window.open("/pdf/"+data.catatan);--%>
		              	<%--},--%>
		              	<%--error : function(e){--%>
		              		<%--$(".page-loader-wrapper").css("display","none");--%>
		              		 <%----%>
		              			<%--Swal.fire({--%>
			              			  <%--icon: 'error',--%>
			              			  <%--title: 'Oops...',--%>
			              			  <%--text: "Error "--%>
			              			<%--});--%>
		              			<%----%>
		              	<%--}--%>
		              <%--});--%>
		      	  }
		    });
		});
	});
</script>
	
</body>
</html>