<%--
  Created by IntelliJ IDEA.
  User: faiq
  Date: 28/06/2022
  Time: 19:59:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@page import="com.faiq.eka.constant.ApplConstant"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String resourceLink = "/"; %>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="muh nailur rohman faiq">
    <meta name="keyword" content="Dashboard">

    <title>EKA STOKIS APPS</title>
    <jsp:include page="../general/header_import.jsp"></jsp:include>
</head>
<body class="theme-blue-grey">

<jsp:include page="../general/header.jsp"></jsp:include>
<jsp:include page="../general/menu.jsp">
    <jsp:param name="menuact" value="rekapan" />
</jsp:include>
<jsp:include page="../lookup/lookup-list-barang.jsp"></jsp:include>
<section class="content">
    <div class="container-fluid">
        <ol class="breadcrumb breadcrumb-bg-blue">
            <li><a href="javascript:void(0);"><i class="fa fa-home fa-lg m-t-10" aria-hidden="true"></i> Home</a></li>
            <li class="active"><i class="fa fa-archive fa-lg m-t-10" aria-hidden="true"></i> Laporan Rekapan</li>
        </ol>

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header bg-blue">
                        <h2>
                            Per Rekapan
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="col-lg-3">
                                    <label>Dari Tanggal</label>
                                    <div class="form-line" id="bs_datepicker_container">
                                        <input type="text" id="tanggalMulai" class="form-control align-center" placeholder="Pilih Tanggal" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <label>Sampai Tanggal</label>
                                    <div class="form-line" id="bs_datepicker_container2">
                                        <input type="text" id="tanggalAkhir" class="form-control align-center" placeholder="Pilih Tanggal" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-1">
                                <button type="button" id="cetakLaporanRekapan" class="btn bg-blue m-t-25">Generate</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header bg-blue">
                        <h2>
                            Hasil
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover" id="table-rekapan" style="width: 100%;">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>BD</th>
                                            <th>Keterangan BD</th>
                                            <th>Kode Barang</th>
                                            <th>Nama Barang</th>
                                            <th>Total Pengeluaran</th>
                                            <th>Satuan</th>
                                            <th>Catatan</th>
                                            <th>Total Harga</th>
                                            <th>Bulan</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <%--<div class="row">--%>
                            <%--<div class="col-lg-12">--%>
                                <%--&lt;%&ndash;<div class="table-responsive">&ndash;%&gt;--%>
                                <%--<table class="table table-bordered table-striped table-hover" id="table-total-barang">--%>
                                    <%--<thead>--%>
                                    <%--<tr>--%>
                                        <%--<th>Kode Barang</th>--%>
                                        <%--<th>Nama Barang</th>--%>
                                        <%--<th>Total</th>--%>
                                        <%--<th>Satuan</th>--%>
                                    <%--</tr>--%>
                                    <%--</thead>--%>
                                    <%--<tbody>--%>
                                    <%--</tbody>--%>
                                <%--</table>--%>
                                <%--&lt;%&ndash;</div>&ndash;%&gt;--%>
                            <%--</div>--%>
                        <%--</div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<jsp:include page="../general/bottom_import.jsp"></jsp:include>
<script type="text/javascript">

    $(document).ready(function(){
        var oTableHasil = $("#table-rekapan").dataTable();

        $('#bs_datepicker_container input').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            container: '#bs_datepicker_container'
        });

        $('#bs_datepicker_container2 input').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            container: '#bs_datepicker_container2'
        });


        $("#cetakLaporanRekapan").click(function(e){
            e.preventDefault();

            var tanggalMulai = $("#tanggalMulai").val();
            var tanggalAkhir = $("#tanggalAkhir").val();

            Swal.fire({
                title: 'Are you sure?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Cetak',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.isConfirmed) {
                // $(".page-loader-wrapper").css("display","");

                    oTableHasil.fnClearTable();
                    oTableHasil.fnDestroy();
                    oTableHasil = $('#table-rekapan').dataTable(configHasil(tanggalMulai,tanggalAkhir));

                }
            });
        });

    });

    function configHasil(tanggalMulai,tanggalAkhir) {
        var config = {
            "responsive": true,
            "bFilter": false, //hide Search bar
            "bInfo": false,
            // "aLengthMenu": [[5, 10, 25], [5, 10, 25]],
            "iDisplayLength":999999,
            "sDom": 'Bfrtip',
            "buttons": [
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: ':visible'
                    },
                    messageTop: tanggalMulai+' s/d '+tanggalAkhir,
                    title: 'REKAPAN - EKA STOKIS APPS'
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: ':visible'
                    },
                    messageTop: tanggalMulai+' s/d '+tanggalAkhir,
                    title: 'REKAPAN - EKA STOKIS APPS'
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: ':visible'
                    },
                    messageTop: tanggalMulai+' s/d '+tanggalAkhir,
                    title: 'REKAPAN - EKA STOKIS APPS'
                },
                'colvis'
            ],
            "bLengthChange": true,
            /* "bFilter":false, */
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<%= ApplConstant.URL_LAPORAN_REKAPAN_CETAK %>?tanggalMulai="+tanggalMulai+"&tanggalAkhir="+tanggalAkhir,
            "sAjaxDataProp": "data.content",
            "sServerMethod": "POST",
            // "fnCreatedRow": function (row, data, index) {
            //     $( row ).find('td:eq(0)').html(index + 1);
            // },
            "aoColumns": [
                { "mData": "number", "sClass":"dt-head-center dt-body-center vertical-middle"},
                { "mData": "namaBd", "sClass":"dt-head-center dt-body-center vertical-middle"},
                { "mData": "deskripsi", "sClass":"dt-head-center dt-body-center vertical-middle"},
                { "mData": "kodeBarang", "sClass":"dt-head-center dt-body-center vertical-middle"},
                { "mData": "namaBarang", "sClass":"dt-head-center dt-body-left vertical-middle"},
                { "mData": "jumlah", "sClass":"dt-head-center dt-body-center vertical-middle"},
                { "mData": "satuan", "sClass":"dt-head-center dt-body-center vertical-middle"},
                { "mData": "catatan", "sClass":"dt-head-center dt-body-center vertical-middle"},
                { "mData": "hargaSatuan", "sClass":"dt-head-center dt-body-center vertical-middle"},
                { "mData": "tanggal", "sClass":"dt-head-center dt-body-center vertical-middle"},
            ],
        };

        return config;
    }


</script>

</body>
</html>
