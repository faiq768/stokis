<!DOCTYPE html>
<%@page import="com.faiq.eka.constant.ApplConstant"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String resourceLink = "/"; %>

<html lang="en">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="muh nailur rohman faiq">
    <meta name="keyword" content="Dashboard">

    <title>EKA STOKIS APPS</title>
    <jsp:include page="../general/header_import.jsp"></jsp:include>
</head>
<body class="theme-blue-grey">

<jsp:include page="../general/header.jsp"></jsp:include>
<jsp:include page="../general/menu.jsp">
	<jsp:param name="menuact" value="laporanPerBd" />
</jsp:include>
<section class="content">
    <div class="container-fluid">
    <jsp:include page="../lookup/lookup-list-bd.jsp"></jsp:include>
        <ol class="breadcrumb breadcrumb-bg-blue">
            <li><a href="javascript:void(0);"><i class="fa fa-home fa-lg m-t-10" aria-hidden="true"></i> Home</a></li>
            <li class="active"><i class="fa fa-archive fa-lg m-t-10" aria-hidden="true"></i> Laporan Per BD</li>
        </ol>

        <div class="row clearfix">
            <div class="col-lg-12">
            	<div class="card">
            		<div class="header bg-blue">
                         <h2>
                             Per BD
                         </h2>
                     </div>
                     <div class="body">
                     	<div class="row">
                     		<div class="col-lg-3">
                     			<div class="form-group form-float">
		                            <div class="form-line">
		                            <input type="hidden" id="idBd" >
		                                <input type="text" id="bd" class="form-control" maxLength="5" autocomplete="off" disabled>
		                                <label class="form-label">BD</label>
		                            </div>
		                        </div>
		            		</div>
		            		<div class="col-lg-2">
		            			<button type="button" class="btn btn-sm btn-danger hidden" id="cancelBd" ><i class="fa fa-close" aria-hidden="true"></i></button>
		            			<button type="button" class="btn btn-sm btn-warning" id="searchBd" data-toggle="modal" data-target="#listBdKeluarMasukModal"><i class="fa fa-search" aria-hidden="true"></i></button>
		            		</div>
                     	</div>
                     	<div class="row">
                     		<div class="col-lg-1">
		            			<button type="button" id="cetakLaporanPerBd" class="btn bg-blue m-t-25">Cetak</button>
		            		</div>
                     	</div>
                     </div>
            	</div>
            </div>
        </div>
    </div>
</section>
 	
 	<jsp:include page="../general/bottom_import.jsp"></jsp:include>
<script type="text/javascript">
	$(document).ready(function(){
		
		var configBd = { 
				"responsive": true,
				"aLengthMenu": [[5, 10, 25], [5, 10, 25]],
				"iDisplayLength":5,
		       	"bLengthChange": true,
		       	/* "bFilter":false, */
		       	"bProcessing": true,
		       	"bServerSide": true,
		       	"sAjaxSource": "<%= ApplConstant.URL_LIST_BD_DATATABLE %>",
		       	"sAjaxDataProp": "data.content",
		       	"sServerMethod": "POST",
			   "fnCreatedRow": function (row, data, index) {
		            $( row ).find('td:eq(0)').html(index + 1);
		        },
		       	"aoColumns": [
		       		{ "mData": null, "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"5%" },
		       		{ "mData": "id", "sClass":"dt-head-center dt-body-center vertical-middle hidden","sWidth":"20%"},
		            { "mData": "namaBd", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"20%"},
		            { "mData": "deskripsiBd", "sClass":"dt-head-center dt-body-left vertical-middle","sWidth":"50%"}
		        ],
			};
		
		oTableBd = $("#table-list-bd").dataTable();
		
		//Bootstrap datepicker plugin
	    $('#bs_datepicker_container input').datepicker({
	    	format: 'dd-mm-yyyy',
	        autoclose: true,
	        container: '#bs_datepicker_container'
	    });
		
	    $('#bs_datepicker_range_container').datepicker({
	    	format: 'dd-mm-yyyy',
	        autoclose: true,
	        container: '#bs_datepicker_range_container'
	    });
		
		$("#searchBd").click(function(){
			oTableBd.fnClearTable();
			oTableBd.fnDestroy();
			oTableBd = $('#table-list-bd').dataTable(configBd);
      	  	$("#table-list-bd").css("width","100%");
		});
		
		$('#cancelBd').click(function(){
			$("#idBd").val("");
			$("#bd").val("");
			$("#bd").blur();
			$('#cancelBd').addClass("hidden");
		});
		
		$("#table-list-bd tbody").on('click', 'tr', function(e){
			var idBd = $(this).context.children[1].innerText;
			var kodeBd = $(this).context.children[2].innerText;
			$("#idBd").val(idBd);
			$("#bd").removeAttr("disabled");
			$("#bd").focus();
			$("#bd").val(kodeBd);
			$("#bd").attr("disabled","disabled");;
			$("#btnTutupListBd").click();
			$('#cancelBd').removeClass("hidden");
		});
		
		$("#cetakLaporanPerBd").click(function(e){
			e.preventDefault();

			Swal.fire({
		      	  title: 'Are you sure?',
		      	  icon: 'warning',
		      	  showCancelButton: true,
		      	  confirmButtonColor: '#3085d6',
		      	  cancelButtonColor: '#d33',
		      	  confirmButtonText: 'Cetak',
		      	  cancelButtonText: 'Batal'
		      	}).then((result) => {
		      	  if (result.isConfirmed) {
                    var link1 = "<%= ApplConstant.URL_LAPORAN_PER_BD_CETAK.concat("?pBd=")%>"+$("#bd").val();
                    window.open(link1,'_blank');
		      	  }
		    });
		});
		
	});
</script>
	
</body>
</html>