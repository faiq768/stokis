<div class="modal fade" id="listBarangKeluarMasukModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-col-blue">
            <div class="modal-header">
                <h4 class="modal-title" id="listBarangKeluarMasukModalLabel">Tambah Barang</h4>
            </div>
            <div class="modal-body">
            	<div class="row">
            		<div class="col-lg-12">
       					<table class="table table-bordered table-striped table-hover col-black" id="table-list-barang">
                           <thead>
                               <tr>
                                   <th>No</th>
                                   <th class="hidden">ID</th>
                                   <th>Kode Barang</th>
                                   <th>Nama Barang</th>
                                   <th>Stok</th>
                                   <th>Satuan</th>
                                   <th>Induk Satuan</th>
                                   <th>Nominal Induk Satuan</th>
                               </tr>
                           </thead>
                           <tbody>
                           </tbody>
                        </table>
            		</div>
            	</div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnTutupListBarang" class="btn btn-link waves-effect" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>