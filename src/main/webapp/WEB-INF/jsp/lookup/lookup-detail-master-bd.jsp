<div class="modal fade" id="addDetailMasterBdModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-col-blue">
            <div class="modal-header">
                <h4 class="modal-title" id="addDetailMasterBdModalLabel">Detail</h4>
            </div>
            <div class="modal-body">
            	<div class="row">
            		<div class="col-lg-12">
            			<div class="table-responsive">
	                        <table class="table table-bordered table-striped table-hover" id="table-detail-master-bd">
	                            <thead>
	                                <tr>
	                                    <th>No</th>
	                                    <th>Nama Barang</th>
	                                    <th>Jumlah Barang</th>
	                                    <!-- <th>Realisasi Jumlah Barang</th> -->
	                                    <th>Aksi</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                            </tbody>
	                         </table>
	                    </div>
            		</div>
            	</div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnTutupMasterBarang" class="btn btn-link waves-effect" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>