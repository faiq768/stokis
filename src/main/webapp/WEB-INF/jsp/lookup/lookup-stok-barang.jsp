<div class="modal fade" id="stokBarangModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content modal-col-blue">
            <div class="modal-header">
                <h4 class="modal-title" id="stokBarangModalLabel"></h4>
            </div>
            <div class="modal-body">
            	<div class="row">
            		<div class="col-lg-12">
            			<div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" id="ubahStokBarang" onkeypress="return isNumberKey(event);" style="background-color:#2196F3;" class="form-control col-white" maxLength="5" autocomplete="off">
                                <label class="form-label col-white">Stok Barang</label>
                            </div>
                        </div>
            		</div>
            	</div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnReturnStokBarang" class="btn btn-link waves-effect hidden">Simpan</button>
                <button type="button" id="btnSimpanAddStokBarang" class="btn btn-link waves-effect">Simpan</button>
                <button type="button" id="btnSimpanRemoveStokBarang" class="btn btn-link waves-effect">Simpan</button>
                <button type="button" id="btnTutupUbahMasterBarang" class="btn btn-link waves-effect" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>