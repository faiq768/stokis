<div class="modal fade" id="listBdKeluarMasukModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-col-blue">
            <div class="modal-header">
                <h4 class="modal-title" id="listBdKeluarMasukModalLabel">List Bagian</h4>
            </div>
            <div class="modal-body">
            	<div class="row">
            		<div class="col-lg-12">
       					<table class="table table-bordered table-striped table-hover col-black" id="table-list-bd">
                           <thead>
                               <tr>
                                   <th>No</th>
                                   <th class="hidden">ID</th>
                                   <th>Nama BD</th>
                                   <th>Deskripsi BD</th>
                               </tr>
                           </thead>
                           <tbody>
                           </tbody>
                        </table>
            		</div>
            	</div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnTutupListBd" class="btn btn-link waves-effect" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>