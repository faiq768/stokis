<%--
  Created by IntelliJ IDEA.
  User: faiq
  Date: 12/06/2022
  Time: 21:56:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="modal fade" id="listBagianModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-col-blue">
            <div class="modal-header">
                <h4 class="modal-title" id="listBagianModalLabel">List Bagian</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-bordered table-striped table-hover col-black" id="table-list-bagian">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th class="hidden">ID</th>
                                <th>Nama Bagian</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnTutupListBagian" class="btn btn-link waves-effect" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>