<div class="modal fade" id="addBarangDetailMasterBdModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-col-blue">
            <div class="modal-header">
                <h4 class="modal-title" id="addBarangDetailMasterBdModalLabel">Tambah Barang</h4>
            </div>
            <div class="modal-body">
            	<div class="row">
            		<div class="col-lg-4">
            			<div class="form-group form-float">
                            <div class="form-line">
                            	<input type="hidden" id="idBarang">
                                <input type="text" id="kodeBarang" style="background-color:#2196F3;" class="form-control col-white" maxLength="5" autocomplete="off" disabled>
                                <label class="form-label col-white">Kode Barang</label>
                            </div>
                        </div>
            		</div>
            		<div class="col-lg-1">
            			<button type="button" class="btn btn-sm btn-warning" id="searchBarangDetailMasterBd" data-toggle="modal" data-target="#listBarangMasterBdModal"><i class="fa fa-search" aria-hidden="true"></i></button>
            		</div>
            		<div class="col-lg-6">
            			<div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" id="namaBarang" style="background-color:#2196F3;" class="form-control col-white" maxLength="50" autocomplete="off"  disabled>
                                <label class="form-label col-white">Nama Barang</label>
                            </div>
                        </div>
            		</div>
            		<div class="col-lg-3">
            			<div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" id="jumlahBarang" onkeypress="return isNumberKey(event);" style="background-color:#2196F3;" class="form-control col-white" maxLength="5" autocomplete="off">
                                <label class="form-label col-white">Jumlah Barang</label>
                            </div>
                        </div>
            		</div>
            	</div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSimpanListBarangMasterBd" class="btn btn-link waves-effect">Simpan</button>
                <button type="button" id="btnTutupAddListBarangMasterBd" class="btn btn-link waves-effect" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>