<%@page import="com.faiq.eka.persistence.entity.MasterSatuan"%>
<%@page import="java.util.List"%>


<div class="modal fade" id="addBarangModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-col-blue">
            <div class="modal-header">
                <h4 class="modal-title" id="addBarangModalLabel">Tambah Barang</h4>
            </div>
            <div class="modal-body">
            	<div class="row clearfix">
            		<div class="col-lg-5">
            			<div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" id="kodeBarang" style="background-color:#2196F3;" class="form-control col-white" maxLength="10" autocomplete="off">
                                <label class="form-label col-white">Kode Barang</label>
                            </div>
                        </div>
            		</div>
            		<div class="col-lg-5">
            			<div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" id="namaBarang" style="background-color:#2196F3;" class="form-control col-white" maxLength="255" autocomplete="off">
                                <label class="form-label col-white">Nama Barang</label>
                            </div>
                        </div>
            		</div>
                </div>
                <div class="row clearfix">
            		<div class="col-lg-3">
            			<div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" id="stokBarang" onkeypress="return isNumberKey(event);" style="background-color:#2196F3;" class="form-control col-white" maxLength="11" autocomplete="off">
                                <label class="form-label col-white">Stok Barang</label>
                            </div>
                        </div>
            		</div>
            		<div class="col-lg-6">
            			<select class="form-control show-tick" id="satuan">
                            <option value="">-- Pilih Satuan --</option>
            					<%
									List<MasterSatuan> lSatuan = (List<MasterSatuan>)request.getAttribute("lSatuan");
									int index = 0;
									int dataN = 1;
									String alp = "";
									for(MasterSatuan st : lSatuan){
								%>
									<option value="<%= st.getId() %>"><%= st.getSatuan() %></option>
								<%
									}
								%>

                        </select>
            		</div>
                    <div class="col-lg-3">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" id="hargaSatuan" onkeypress="return isNumberKey(event);" style="background-color:#2196F3;" class="form-control col-white" maxLength="21" autocomplete="off">
                                <label class="form-label col-white">Harga Satuan</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-4">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" id="stokIndukSatuan" onkeypress="return isNumberKey(event);" style="background-color:#2196F3;" class="form-control col-white" maxLength="11">
                                <label class="form-label col-white">Stok Induk Satuan Barang</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <select class="form-control show-tick" id="indukSatuan">
                            <option value="">-- Pilih Induk Satuan --</option>
                            <%
//                                List<MasterSatuan> lSatuan = (List<MasterSatuan>)request.getAttribute("lSatuan");
//                                int index = 0;
//                                int dataN = 1;
//                                String alp = "";
                                for(MasterSatuan st : lSatuan){
                            %>
                            <option value="<%= st.getId() %>"><%= st.getSatuan() %></option>
                            <%
                                }
                            %>
                        </select>
                    </div>
            	</div>
                <div class="row clearfix">
                    <div class="col-lg-4">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" id="nominalIndukSatuan" onkeypress="return isNumberKey(event);" style="background-color:#2196F3;" class="form-control col-white" maxLength="11" autocomplete="off">
                                <label class="form-label col-white">Nominal Induk Satuan</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <input type="checkbox" id="moving" value="Y" class="filled-in chk-col-deep-orange" checked />
                        <label for="moving">Moving</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSimpanMasterBarang" class="btn btn-link waves-effect">Simpan</button>
                <button type="button" id="btnTutupMasterBarang" class="btn btn-link waves-effect" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>