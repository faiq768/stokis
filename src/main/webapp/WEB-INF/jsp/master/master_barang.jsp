<!DOCTYPE html>
<%@page import="com.faiq.eka.constant.ApplConstant"%>
<%@ page import="com.faiq.eka.persistence.entity.MasterSatuan" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String resourceLink = "/"; %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authentication property="principal.authorities" var="authorities" />
<c:set var = "isAdmin" value = "${false}"/>
<c:set var = "isDataEntry" value = "${false}"/>
<c:set var = "isViewer" value = "${false}"/>
<c:forEach items="${authorities}" var="authority" varStatus="vs">
    <c:choose>
        <c:when test="${authority.authority == 'Administrator'}">
            <c:set var = "isAdmin" value = "${true}"/>
        </c:when>
        <c:when test="${authority.authority == 'Data Entry'}">
            <c:set var = "isDataEntry" value = "${true}"/>
        </c:when>
        <c:otherwise>
            <c:set var = "isViewer" value = "${true}"/>
        </c:otherwise>
    </c:choose>
</c:forEach>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="muh nailur rohman faiq">
    <meta name="keyword" content="Dashboard">

    <title>EKA STOKIS APPS</title>
    <jsp:include page="../general/header_import.jsp"></jsp:include>
</head>
<body class="theme-blue-grey">

<jsp:include page="../general/header.jsp"></jsp:include>
<jsp:include page="../general/menu.jsp">
	<jsp:param name="menuact" value="masterBarang" />
</jsp:include>
<section class="content">
<jsp:include page="../lookup/lookup-add-barang.jsp"></jsp:include>
<jsp:include page="../lookup/lookup-stok-barang.jsp"></jsp:include>
    <div class="container-fluid">
        <ol class="breadcrumb breadcrumb-bg-blue">
            <li><a href="javascript:void(0);"><i class="fa fa-home fa-lg m-t-10" aria-hidden="true"></i> Home</a></li>
            <li class="active"><i class="fa fa-archive fa-lg m-t-10" aria-hidden="true"></i> Master Barang</li>
        </ol>

        <div class="row clearfix">
            <div class="col-lg-12">
            	<div class="card">
            		<div class="header bg-blue">
                         <h2>
                             Daftar Barang <c:if test="${isDataEntry}"> &nbsp;<button type="button" class="btn bg-pink" data-toggle="modal" data-target="#addBarangModal">Tambah</button> </c:if>
                         </h2>
                     </div>
                     <div class="body">
                     	<div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="table-master-barang">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Barang</th>
                                        <th>Nama Barang</th>
                                        <th>Stok</th>
                                        <th>Satuan</th>
                                        <th>Harga Satuan</th>
                                        <th>Stok Induk Satuan</th>
                                        <th>Induk Satuan</th>
                                        <th>Nominal Induk Satuan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                             </table>
                         </div>
                     </div>
            	</div>
            </div>
        </div>
    </div>
</section>
 	
 	<jsp:include page="../general/bottom_import.jsp"></jsp:include>
 	<script>
 		$(document).ready(function(){

 			var oTable = $("#table-master-barang").dataTable(config());

 			$("#btnSimpanMasterBarang").click(function(e){
 				e.preventDefault();
 				var moving = '';

 				if($("#kodeBarang").val().length < 1){
					$("#kodeBarang").focus();
					return false;
				}else if($("#namaBarang").val().length < 1){
					$("#namaBarang").focus();
					return false;
				}else if($("#stokBarang").val().length < 1){
					$("#stokBarang").focus();
					return false;
				}else if($("#hargaSatuan").val().length < 1){
                    $("#hargaSatuan").focus();
                    return false;
                }else if($("#stokIndukSatuan").val().length < 1){
                    $("#stokIndukSatuan").focus();
                    return false;
                }else if($("#indukSatuan").val().length < 1){
                    $("#indukSatuan").focus();
                    return false;
                }else if($("#nominalIndukSatuan").val().length < 1){
                    $("#nominalIndukSatuan").focus();
                    return false;
                }

                if ($("#moving").prop('checked') === true){
                    moving = 'Y';
                } else{
                    moving = 'N';
                }

				Swal.fire({
			      	  title: 'Are you sure?',
			      	  icon: 'warning',
			      	  showCancelButton: true,
			      	  confirmButtonColor: '#3085d6',
			      	  cancelButtonColor: '#d33',
			      	  confirmButtonText: 'Simpan',
			      	  cancelButtonText: 'Batal'
			      	}).then((result) => {
			      	  if (result.isConfirmed) {
			      		$(".page-loader-wrapper").css("display","");

				    	var datas = {
                                "kodeBarang"    : $("#kodeBarang").val(),
                                "namaBarang"    : $("#namaBarang").val(),
                                "stok"          : $("#stokBarang").val(),
                                "idSatuan"      : $("#satuan").val(),
                                "hargaSatuan"   : $("#hargaSatuan").val(),
                                "stokIndukSatuan" : $("#stokIndukSatuan").val(),
                                "idIndukSatuan"   : $("#indukSatuan").val(),
                                "nominalIndukSatuan" : $("#nominalIndukSatuan").val(),
                                "moving"            : moving
			                }

			      		$.ajax({
			              	type: 'POST',
			              	contentType: "application/json",
			              	url: "<%= ApplConstant.URL_MASTER_BARANG_SAVE%>",
			              	data: JSON.stringify(datas),
			                  dataType: 'json',
			                  cache: false,
			              	success : function(){
			              		oTable.fnClearTable();
			              		oTable.fnDestroy();
			              	  	oTable = $('#table-master-barang').dataTable(config());
			              	  $(".page-loader-wrapper").css("display","none");
			              		Swal.fire({
			            			  icon: 'success',
			            			  text: "Berhasil Disimpan"
			            		});
			              		 $("#kodeBarang").val("");
			              		 $("#namaBarang").val("");
			              		 $("#stokBarang").val("");
                                $("#hargaSatuan").val("");
                                $("#stokIndukSatuan").val("");
                                $("#indukSatuan").val("");
                                $("#nominalIndukSatuan").val("");
			              		 $("#btnTutupMasterBarang").click();
			              	},
			              	error : function(data){
			              		$(".page-loader-wrapper").css("display","none");
			              		if(data.status == 302){
			              			Swal.fire({
				              			  icon: 'warning',
				              			  title: 'Oops...',
				              			  text: "Kode Barang sudah dipakai"
				              			});
			              		}else{
			              			Swal.fire({
				              			  icon: 'error',
				              			  title: 'Oops...',
				              			  text: "Error "
				              			});
			              		}
			              			
			              	}
			              });
			      	  }
			    });
 			});
 			
 			$("#table-master-barang tbody").on('click', '#btnDeleteMasterBarang', function(e){
				e.preventDefault();
				
				Swal.fire({
			      	  title: 'Are you sure?',
			      	  icon: 'warning',
			      	  showCancelButton: true,
			      	  confirmButtonColor: '#3085d6',
			      	  cancelButtonColor: '#d33',
			      	  confirmButtonText: 'Simpan',
			      	  cancelButtonText: 'Batal'
			      	}).then((result) => {
			      	  if (result.isConfirmed) {
			      		$(".page-loader-wrapper").css("display","");
				    	
				    	var datas = {
				    			"id"	: $(this).attr("data")
			                }
			      		
			      		$.ajax({
			              	type: 'POST',
			              	contentType: "application/json",
			              	url: "<%= ApplConstant.URL_MASTER_BARANG_DELETE%>",
			              	data: JSON.stringify(datas),
			                  dataType: 'json',
			                  cache: false,
			              	success : function(){
			              		oTable.fnClearTable();
			              		oTable.fnDestroy();
			              	  	oTable = $('#table-master-barang').dataTable(config());
			              	  $(".page-loader-wrapper").css("display","none");
			              		Swal.fire({
			            			  icon: 'success',
			            			  text: "Berhasil Dihapus"
			            		});
			              		
			              	},
			              	error : function(e){
			              		$(".page-loader-wrapper").css("display","none");
			              		 console.log(e);
			              			if (e.status == 302){
                                        Swal.fire({
                                            icon: 'warning',
                                            title: 'Barang tidak dapat dihapus!',
                                            text: "Sudah terdapat transaksi pada catatan keluar masuk barang"
                                        });
                                    } else{
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Oops...',
                                            text: "Error "
                                        });
                                    }
			              			
			              	}
			              });
			      	  }
			    });
				
			});
 			
 			$("#table-master-barang tbody").on('click', '#btnEditMasterBarang', function(e){
				e.preventDefault();
				document.getElementById("kodeBarang").focus();
				var kodeBarang = $(this).attr("kodeBarang");
				var namaBarang = $(this).attr("namaBarang");
                var nominalIndukSatuan = $(this).attr("nominalIndukSatuan");
				var id		   = $(this).attr("data");
				var labelKodeBarang = "#label-kode-barang-"+id;
				var inputKodeBarang = "#input-kode-barang-"+id;
				var labelNamaBarang = "#label-nama-barang-"+id;
				var inputNamaBarang = "#input-nama-barang-"+id;
                var labelNominalIndukSatuan = "#label-nominal-induk-satuan-"+id;
                var inputNominalIndukSatuan = "#input-nominal-induk-satuan-"+id;
                var labelHargaSatuan = "#label-harga-satuan-"+id;
                var inputHargaSatuan = "#input-harga-satuan-"+id;
                var labelSatuan     = "#label-satuan-"+id;
                var inputSatuan     = "#input-satuan-"+id;
                var labelIndukSatuan     = "#label-induk-satuan-"+id;
                var inputIndukSatuan     = "#input-induk-satuan-"+id;
				var btnFirstBarang	= "#btn-first-"+id;
				var btnSecondBarang	= "#btn-second-"+id;
				$(labelKodeBarang).addClass("hidden");
				$(inputKodeBarang).removeClass("hidden");
				$(labelNamaBarang).addClass("hidden");
				$(inputNamaBarang).removeClass("hidden");
                $(labelNominalIndukSatuan).addClass("hidden");
                $(inputNominalIndukSatuan).removeClass("hidden");
                $(labelHargaSatuan).addClass("hidden");
                $(inputHargaSatuan).removeClass("hidden");
                $(labelSatuan).addClass("hidden");
                $(inputSatuan).removeClass("hidden");
                $(labelIndukSatuan).addClass("hidden");
                $(inputIndukSatuan).removeClass("hidden");
				$(btnFirstBarang).addClass("hidden");
				$(btnSecondBarang).removeClass("hidden");
				
 			});
 			
 			$("#table-master-barang tbody").on('click', '#btnCencelMasterBarang', function(e){
                var kodeBarang = $(this).attr("kodeBarang");
                var namaBarang = $(this).attr("namaBarang");
                var nominalIndukSatuan = $(this).attr("nominalIndukSatuan");
                var id		   = $(this).attr("data");
                var labelKodeBarang = "#label-kode-barang-"+id;
                var inputKodeBarang = "#input-kode-barang-"+id;
                var labelNamaBarang = "#label-nama-barang-"+id;
                var inputNamaBarang = "#input-nama-barang-"+id;
                var labelNominalIndukSatuan = "#label-nominal-induk-satuan-"+id;
                var inputNominalIndukSatuan = "#input-nominal-induk-satuan-"+id;
                var labelHargaSatuan = "#label-harga-satuan-"+id;
                var inputHargaSatuan = "#input-harga-satuan-"+id;
                var labelSatuan     = "#label-satuan-"+id;
                var inputSatuan     = "#input-satuan-"+id;
                var labelIndukSatuan     = "#label-induk-satuan-"+id;
                var inputIndukSatuan     = "#input-induk-satuan-"+id;
				var btnFirstBarang	= "#btn-first-"+id;
				var btnSecondBarang	= "#btn-second-"+id;
				$(labelKodeBarang).removeClass("hidden");
				$(inputKodeBarang).addClass("hidden");
				$(labelNamaBarang).removeClass("hidden");
				$(inputNamaBarang).addClass("hidden");
                $(labelNominalIndukSatuan).removeClass("hidden");
                $(inputNominalIndukSatuan).addClass("hidden");
                $(labelHargaSatuan).removeClass("hidden");
                $(inputHargaSatuan).addClass("hidden");
                $(labelSatuan).removeClass("hidden");
                $(inputSatuan).addClass("hidden");
                $(labelIndukSatuan).removeClass("hidden");
                $(inputIndukSatuan).addClass("hidden");
				$(btnFirstBarang).removeClass("hidden");
				$(btnSecondBarang).addClass("hidden");
				
 			});
 			
 			$("#table-master-barang tbody").on('click', '#btnEditSimpanMasterBarang', function(e){
				e.preventDefault();
				var id		   = $(this).attr("data");
				var inputKodeBarang = "#kode-barang-"+id;
				var inputNamaBarang = "#nama-barang-"+id;
                var inputNominalIndukSatuan = "#nominal-induk-satuan-"+id;
                var inputHargaSatuan = "#harga-satuan-"+id;
                var inputSatuan = "#satuan-"+id;
                var inputIndukSatuan = "#induk-satuan-"+id;
				
				Swal.fire({
			      	  title: 'Are you sure?',
			      	  icon: 'warning',
			      	  showCancelButton: true,
			      	  confirmButtonColor: '#3085d6',
			      	  cancelButtonColor: '#d33',
			      	  confirmButtonText: 'Simpan',
			      	  cancelButtonText: 'Batal'
			      	}).then((result) => {
			      	  if (result.isConfirmed) {
			      		$(".page-loader-wrapper").css("display","");
				    	
				    	var datas = {
				    			"id"	: $(this).attr("data"),
				    			"kodeBarang": $(inputKodeBarang).val(),
				    			"namaBarang": $(inputNamaBarang).val(),
                               "nominalIndukSatuan" : $(inputNominalIndukSatuan).val(),
                                "hargaSatuan": $(inputHargaSatuan).val(),
                                "idSatuan": $(inputSatuan).val(),
                                "idIndukSatuan": $(inputIndukSatuan).val()
			                }
			      		
			      		$.ajax({
			              	type: 'POST',
			              	contentType: "application/json",
			              	url: "<%= ApplConstant.URL_MASTER_BARANG_CARI_TRX%>",
			              	data: JSON.stringify(datas),
			                  dataType: 'json',
			                  cache: false,
			              	success : function(data){
                                $(".page-loader-wrapper").css("display","none");
			              	    if (data.length > 0){
                                    Swal.fire({
                                        title: 'Sudah terdapat transaksi pada barang ini, apakah anda yakin untuk melanjutkanya?',
                                        icon: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: 'Lanjutkan',
                                        cancelButtonText: 'Batal'
                                    }).then((result) => {
                                        if (result.isConfirmed) {
                                        $(".page-loader-wrapper").css("display","");
                                            simpanEditBarang(datas,oTable);
                                        }
                                    });
                                } else{
			              	        simpanEditBarang(datas,oTable);
                                }


			              	},
			              	error : function(data){
			              		$(".page-loader-wrapper").css("display","none");
                                if(data.status == 302){
                                    Swal.fire({
                                        icon: 'warning',
                                        title: 'Oops...',
                                        text: "Kode Barang sudah dipakai"
                                    });
                                }else{
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Oops...',
                                        text: "Error "
                                    });
                                }
			              	}
			              });
			      	  }
			    });
 			});
 			
 			$("#table-master-barang tbody").on('click', '#btnTambahStokMasterBarang', function(e){
 				e.preventDefault();
 				$("#stokBarangModalLabel").html("Tambah Stok Barang");
 				$("#btnSimpanAddStokBarang").removeClass("hidden");
 				$("#btnSimpanRemoveStokBarang").addClass("hidden");
 				$("#btnSimpanAddStokBarang").attr("dataID",$(this).attr("data"));
 			});
 			
 			$("#table-master-barang tbody").on('click', '#btnKurangiStokMasterBarang', function(e){
 				e.preventDefault();
 				$("#stokBarangModalLabel").html("Kurangi Stok Barang");
 				$("#btnSimpanRemoveStokBarang").removeClass("hidden");
 				$("#btnSimpanAddStokBarang").addClass("hidden");
 				$("#btnSimpanRemoveStokBarang").attr("dataID",$(this).attr("data"));
 			});
 			
 			$("#btnSimpanAddStokBarang").click(function(e){
 				e.preventDefault();
 				Swal.fire({
			      	  title: 'Are you sure?',
			      	  icon: 'warning',
			      	  showCancelButton: true,
			      	  confirmButtonColor: '#3085d6',
			      	  cancelButtonColor: '#d33',
			      	  confirmButtonText: 'Simpan',
			      	  cancelButtonText: 'Batal'
			      	}).then((result) => {
			      	  if (result.isConfirmed) {
			      		$(".page-loader-wrapper").css("display","");
				    	
				    	var datas = {
				    			"id"	: $(this).attr("dataID"),
				    			"stok": $("#ubahStokBarang").val()
			                }
			      		
			      		$.ajax({
			              	type: 'POST',
			              	contentType: "application/json",
			              	url: "<%= ApplConstant.URL_TAMBAH_STOK_BARANG%>",
			              	data: JSON.stringify(datas),
			                  dataType: 'json',
			                  cache: false,
			              	success : function(){
			              		oTable.fnClearTable();
			              		oTable.fnDestroy();
			              	  	oTable = $('#table-master-barang').dataTable(config());
			              	  $(".page-loader-wrapper").css("display","none");
			              		Swal.fire({
			            			  icon: 'success',
			            			  text: "Berhasil Menambah Stok"
			            		});
			              		$("#btnTutupUbahMasterBarang").click();
			              		
			              	},
			              	error : function(e){
			              		$(".page-loader-wrapper").css("display","none");
			              		 
			              			Swal.fire({
				              			  icon: 'error',
				              			  title: 'Oops...',
				              			  text: "Error "
				              			});
			              			
			              	}
			              });
			      	  }
			    });
 			});
 			
 			$("#btnSimpanRemoveStokBarang").click(function(e){
 				e.preventDefault();
 				Swal.fire({
			      	  title: 'Are you sure?',
			      	  icon: 'warning',
			      	  showCancelButton: true,
			      	  confirmButtonColor: '#3085d6',
			      	  cancelButtonColor: '#d33',
			      	  confirmButtonText: 'Simpan',
			      	  cancelButtonText: 'Batal'
			      	}).then((result) => {
			      	  if (result.isConfirmed) {
			      		$(".page-loader-wrapper").css("display","");
				    	
				    	var datas = {
				    			"id"	: $(this).attr("dataID"),
				    			"stok": $("#ubahStokBarang").val()
			                }
			      		
			      		$.ajax({
			              	type: 'POST',
			              	contentType: "application/json",
			              	url: "<%= ApplConstant.URL_KURANGI_STOK_BARANG%>",
			              	data: JSON.stringify(datas),
			                  dataType: 'json',
			                  cache: false,
			              	success : function(){
			              		oTable.fnClearTable();
			              		oTable.fnDestroy();
			              	  	oTable = $('#table-master-barang').dataTable(config());
			              	  $(".page-loader-wrapper").css("display","none");
			              		Swal.fire({
			            			  icon: 'success',
			            			  text: "Berhasil Diedit"
			            		});
			              		$("#btnTutupUbahMasterBarang").click();
			              	},
			              	error : function(e){
			              		$(".page-loader-wrapper").css("display","none");
			              		 
			              			Swal.fire({
				              			  icon: 'error',
				              			  title: 'Oops...',
				              			  text: "Error "
				              			});
			              			
			              	}
			              });
			      	  }
			    });
 			});

 			// $("#nominalIndukSatuan").keyup(function () {
            //    var nominalIndukBarang = $(this).val();
            //    var stokBarang = $("#stokBarang").val();
            //    var stokIndukSatuanBarang = (parseFloat(stokBarang) / parseFloat(nominalIndukBarang));
            //    stokIndukSatuanBarang = isNaN(stokIndukSatuanBarang)? 0 : stokIndukSatuanBarang;
            //     $("#stokIndukSatuan").focus();
            //    $("#stokIndukSatuan").val(stokIndukSatuanBarang);
            //    $(this).focus();
            // });

 			$("#stokBarang").keyup(function () {
                var stokBarang = $(this).val();
                var nominalIndukSatuan = $("#nominalIndukSatuan").val();
                var rumus = (parseFloat(stokBarang) / parseFloat(nominalIndukSatuan));
                rumus = isNaN(rumus) ? 0 : rumus;
                $("#stokIndukSatuan").focus();
                $("#stokIndukSatuan").val(rumus);
                $(this).focus();
            });

 			$("#stokIndukSatuan").keyup(function () {
               var stokIndukSatuan = $(this).val();
               var nominalIndukSatuan = $("#nominalIndukSatuan").val();
               var rumus = (parseFloat(stokIndukSatuan)*parseFloat(nominalIndukSatuan));
               rumus = isNaN(rumus) ? 0 : rumus;
                $("#stokBarang").focus();
                $("#stokBarang").val(rumus);
                $(this).focus();
            });

 		});
 		
 		function isNumberKey(evt){
 			var charCode = (evt.which) ? evt.which : evt.keyCode
 		 		    if(
 		 		    	!(charCode > 31 && (charCode < 48 || charCode > 57))
 		 		    	|| (charCode == 46)
 		 		    ){
 		 		    	return true;	
 		 		    }else{
 		 		    	
 		 		    	return false;
 		 		    }
 		    
 		}
        function formatRupiah(angka, prefix){
            var number_string = angka.replace(/[^.\d]/g, '').toString(),
                split   		= number_string.split('.'),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if(ribuan){
                separator = sisa ? ',' : '';
                rupiah += separator + ribuan.join(',');
            }

            rupiah = split[1] != undefined ? rupiah + '.' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }

        function simpanEditBarang(datas,oTable ){
            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: "<%= ApplConstant.URL_MASTER_BARANG_EDIT%>",
                data: JSON.stringify(datas),
                dataType: 'json',
                cache: false,
                success : function(){
                    oTable.fnClearTable();
                    oTable.fnDestroy();
                    oTable = $('#table-master-barang').dataTable(config());
                    $(".page-loader-wrapper").css("display","none");
                    Swal.fire({
                        icon: 'success',
                        text: "Berhasil Diedit"
                    });

                },
                error : function(data){
                    $(".page-loader-wrapper").css("display","none");
                    if(data.status == 302){
                        Swal.fire({
                            icon: 'warning',
                            title: 'Oops...',
                            text: "Kode Barang sudah dipakai"
                        });
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: "Error "
                        });
                    }
                }
            });
        }

        function config() {
            var config = {
                "bStateSave": true,
                "responsive": true,
                "aLengthMenu": [[5, 10, 25], [5, 10, 25]],
                "iDisplayLength":5,
                "bLengthChange": true,
                /* "bFilter":false, */
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "<%= ApplConstant.URL_MASTER_BARANG_DATATABLE %>",
                "sAjaxDataProp": "data.content",
                "sServerMethod": "POST",
                "fnCreatedRow": function (row, data, index) {
                    $( row ).find('td:eq(0)').html(index + 1);
                },
                "aoColumns": [
                    { "mData": null, "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"5%" },
                    { "mData": "kodeBarang", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"20%","mRender":function(data,type,row){
                            return "<div id='label-kode-barang-"+row.id+"' class=''>"+
                                "<span>"+data+"</span>"+
                                "</div>"+
                                "<div id='input-kode-barang-"+row.id+"' class='hidden'>"+
                                "<input type='text' class='form-control text-center' id='kode-barang-"+row.id+"' value='"+data+"' maxlength='10'>"+
                                "</div>";
                        }
                    },
                    { "mData": "namaBarang", "sClass":"dt-head-center dt-body-left vertical-middle","sWidth":"40%","mRender":function(data,type,row){
                            return "<div id='label-nama-barang-"+row.id+"' class=''>"+
                                "<span>"+data+"</span>"+
                                "</div>"+
                                "<div id='input-nama-barang-"+row.id+"' class='hidden'>"+
                                "<input type='text' class='form-control text-center' id='nama-barang-"+row.id+"' value='"+data+"'>"+
                                "</div>";
                        }
                    },
                    { "mData": "stok", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"10%","mRender":function(data,type,row){
                            var btn = data;
                            <c:if test="${isDataEntry}">
                            btn = "<a href='#!' id='btnTambahStokMasterBarang' data='"+row.id+"' title='Tambah Stok' class='col-blue' data-toggle='modal' data-target='#stokBarangModal'><i class='fa fa-plus-circle' aria-hidden='true'></i></a> "+
                                data+
                                " <a href='#!' id='btnKurangiStokMasterBarang' data='"+row.id+"' title='Kurangi Stok' class='col-red' data-toggle='modal' data-target='#stokBarangModal'><i class='fa fa-minus-circle' aria-hidden='true'></i></a>"
                            ;
                            </c:if>
                            return btn;
                        }},
                    { "mData": "satuan", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"10%", "mRender":function (data,type,row) {
                            return "<div id='label-satuan-"+row.id+"' class=''>"+
                                "<span>"+data+"</span>"+
                                "</div>"+
                                "<div id='input-satuan-"+row.id+"' class='hidden'>"+
                                "<select class='form-control' id='satuan-"+row.id+"'>"+
                                <%
                                    List<MasterSatuan> lSatuan = (List<MasterSatuan>)request.getAttribute("lSatuan");
                                    int index = 0;
                                    int dataN = 1;
                                    String alp = "";
                                    for(MasterSatuan st : lSatuan){
                                %>

                                "<option value='<%= st.getId() %>' "+(data=='<%=st.getSatuan()%>'?'selected':'')+"><%= st.getSatuan() %></option>"+
                                <%
                                    }
                                %>
                                "</select>" +
                                "</div>";
                        }},
                    { "mData": "hargaSatuan", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"10%", "mRender":function (data,type,row) {
                            return "<div id='label-harga-satuan-"+row.id+"' class=''>"+
                                "<span>"+formatRupiah(data)+"</span>"+
                                "</div>"+
                                "<div id='input-harga-satuan-"+row.id+"' class='hidden'>"+
                                "<input type='text' class='form-control text-center' onkeypress='return isNumberKey(event);' id='harga-satuan-"+row.id+"' value='"+data+"'>"+
                                "</div>";
                        }},
                    { "mData": "stokIndukSatuan", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"10%"},
                    { "mData": "indukSatuan", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"10%", "mRender":function (data,type,row) {
                            return "<div id='label-induk-satuan-"+row.id+"' class=''>"+
                                "<span>"+data+"</span>"+
                                "</div>"+
                                "<div id='input-induk-satuan-"+row.id+"' class='hidden'>"+
                                "<select class='form-control' id='induk-satuan-"+row.id+"'>"+
                                <%
                                    for(MasterSatuan st : lSatuan){
                                %>

                                "<option value='<%= st.getId() %>' "+(data=='<%=st.getSatuan()%>'?'selected':'')+"><%= st.getSatuan() %></option>"+
                                <%
                                    }
                                %>
                                "</select>" +
                                "</div>";
                        }},
                    { "mData": "nominalIndukSatuan", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"10%","mRender":function(data,type,row){
                            return "<div id='label-nominal-induk-satuan-"+row.id+"' class=''>"+
                                "<span>"+data+"</span>"+
                                "</div>"+
                                "<div id='input-nominal-induk-satuan-"+row.id+"' class='hidden'>"+
                                "<input type='text' class='form-control text-center' onkeypress='return isNumberKey(event);' id='nominal-induk-satuan-"+row.id+"' value='"+data+"'>"+
                                "</div>";
                        }
                    },
                    { "mData": "id", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"15%","mRender":function(data,type,row){
                            var nominalIndukSatuan = row.nominalIndukSatuan.replaceAll(/[,.]/g,"-");
                            var btn = "";
                            <c:if test="${isDataEntry}">
                            btn = "<div id='btn-first-"+data+"'>"+
                                "<a href='#!' id='btnEditMasterBarang' data='"+data+"' title='Edit' kodeBarang='"+row.kodeBarang+"' namaBarang='"+row.namaBarang+"-"+row.id+"' nominalIndukSatuan='"+nominalIndukSatuan+"-"+row.id+"' class='col-blue' ><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>"+
                                " || <a href='#!' id='btnDeleteMasterBarang' data='"+data+"' title='Hapus' class='col-red'><i class='fa fa-trash-o' aria-hidden='true'></i></a>"+
                                "</div>"+
                                "<div id='btn-second-"+data+"' class='hidden'>"+
                                "<a href='#!' id='btnEditSimpanMasterBarang' data='"+data+"' title='Simpan' kodeBarang='"+row.kodeBarang+"' namaBarang='"+row.namaBarang+"-"+row.id+"' nominalIndukSatuan='"+nominalIndukSatuan+"-"+row.id+"' class='col-green' ><i class='fa fa-check-circle-o' aria-hidden='true'></i></a>"+
                                " || <a href='#!' id='btnCencelMasterBarang' data='"+data+"' title='Batal' kodeBarang='"+row.kodeBarang+"' namaBarang='"+row.namaBarang+"-"+row.id+"' nominalIndukSatuan='"+nominalIndukSatuan+"-"+row.id+"' class='col-red'><i class='fa fa-times' aria-hidden='true'></i></a>"+
                                "</div>"
                            ;
                            </c:if>
                            return btn;
                        } },
                ],
            };

            return config;
        }

 	</script>
	
</body>
</html>