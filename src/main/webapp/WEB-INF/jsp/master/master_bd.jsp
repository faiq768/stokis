<!DOCTYPE html>
<%@page import="com.faiq.eka.constant.ApplConstant"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<% String resourceLink = "/"; %>

<sec:authentication property="principal.authorities" var="authorities" />
<c:set var = "isAdmin" value = "${false}"/>
<c:set var = "isDataEntry" value = "${false}"/>
<c:set var = "isViewer" value = "${false}"/>
<c:forEach items="${authorities}" var="authority" varStatus="vs">
    <c:choose>
        <c:when test="${authority.authority == 'Administrator'}">
            <c:set var = "isAdmin" value = "${true}"/>
        </c:when>
        <c:when test="${authority.authority == 'Data Entry'}">
            <c:set var = "isDataEntry" value = "${true}"/>
        </c:when>
        <c:otherwise>
            <c:set var = "isViewer" value = "${true}"/>
        </c:otherwise>
    </c:choose>
</c:forEach>

<html lang="en">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="muh nailur rohman faiq">
    <meta name="keyword" content="Dashboard">

    <title>EKA STOKIS APPS</title>
    <jsp:include page="../general/header_import.jsp"></jsp:include>
</head>
<body class="theme-blue-grey">

<jsp:include page="../general/header.jsp"></jsp:include>
<jsp:include page="../general/menu.jsp">
	<jsp:param name="menuact" value="masterBd" />
</jsp:include>
<jsp:include page="../lookup/lookup-detail-master-bd.jsp"></jsp:include>
<jsp:include page="../lookup/lookup-add-barang-master-bd.jsp"></jsp:include>
<jsp:include page="../lookup/lookup-list-barang-master-bd.jsp"></jsp:include>
<jsp:include page="../lookup/lookup-history-barang-detailbd.jsp"></jsp:include>
<section class="content">
    <div class="container-fluid">
        <ol class="breadcrumb breadcrumb-bg-pink">
            <li><a href="javascript:void(0);"><i class="fa fa-home fa-lg m-t-10" aria-hidden="true"></i> Home</a></li>
            <li class="active"><i class="fa fa-object-group fa-lg m-t-10" aria-hidden="true"></i> Master BD</li>
        </ol>
		<spring:url value="<%=ApplConstant.URL_MASTER_BD_SAVE%>" var="masterBdUrl"/>
	 	<form:form class="form-horizontal tasi-form" method="post" modelAttribute="masterBdForm" id="masterBdForm" action="${masterBdUrl}">
        <div class="row clearfix">
            <div class="
                <c:if test="${isDataEntry}">col-lg-8 col-md-8</c:if>
                <c:if test="${!isDataEntry}">col-lg-12 col-md-12</c:if>

            ">
            	<div class="card">
            		<div class="header bg-pink">
                         <h2>
                             Daftar BD
                         </h2>
                     </div>
                     <div class="body">
                     	<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover" id="table-master-bd">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama BD</th>
                                            <th>Deskripsi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                 </table>
                         </div>
                     </div>
            	</div>
            </div>
            <c:if test="${isDataEntry}">
            <div class="col-lg-4 col-md-4">
            	<div class="card">
            		<div class="header bg-pink">
                         <h2>
                             Tambah BD
                         </h2>
                     </div>
                     <div class="body">
                     	<div class="row clearfix">
                     		<div class="col-lg-12">
                                 <div class="form-group form-float">
                                     <div class="form-line">
                                         <input type="text" id="namaBd" class="form-control" style="text-transform:UPPERCASE;" maxLength="5">
                                         <label class="form-label">Nama BD</label>
                                     </div>
                                 </div>
                             </div>
                             <div class="col-lg-12">
                                 <div class="form-group form-float">
                                     <div class="form-line">
                                         <textarea rows="2" id="deskripsiBd" class="form-control"></textarea>
                                         <label class="form-label">Deskripsi BD</label>
                                     </div>
                                 </div>
                             </div>
                             <div class="col-lg-12">
                             	<button class="btn bg-pink waves-effect" id="btnSimpanBd" type="button">Simpan</button>
                             	<button class="btn bg-pink waves-effect hidden" id="btnSimpanEditBd" type="button">Simpan</button>
                             	<button class="btn bg-red waves-effect hidden" id="btnBatalSimpanBd" type="button">Batal</button>
                             </div>
                     	</div>
                     </div>
            	</div>
            </div>
            </c:if>
        </div>
        </form:form>
        <div class="row clearfix">
            
            
        </div>
    </div>
</section>
 	
 	<jsp:include page="../general/bottom_import.jsp"></jsp:include>
	<script>
		$(document).ready(function(){
			
			var config = {
                    "bStateSave": true,
					"responsive": true,
					"aLengthMenu": [[5, 10, 25], [5, 10, 25]],
					"iDisplayLength":5,
			       	"bLengthChange": true,
			       	/* "bFilter":false, */
			       	"bProcessing": true,
			       	"bServerSide": true,
			       	"sAjaxSource": "<%= ApplConstant.URL_MASTER_BD_DATATABLE %>",
			       	"sAjaxDataProp": "data.content",
			       	"sServerMethod": "POST",
				   "fnCreatedRow": function (row, data, index) {
			            $( row ).find('td:eq(0)').html(index + 1);
			        },
			       	"aoColumns": [
			       		{ "mData": null, "sClass":"dt-head-center dt-body-center vertical-middle" },
			            { "mData": "namaBd", "sClass":"dt-head-center dt-body-center vertical-middle"},
			            { "mData": "deskripsiBd", "sClass":"dt-head-center dt-body-left vertical-middle"},
			            { "mData": "id", "sClass":"dt-head-center dt-body-center vertical-middle","mRender":function(data,type,row){
                            var btn ="<a href='#!' id='btnDetailMasterBd' data='"+data+"' class='col-blue' data-toggle='modal' data-target='#addDetailMasterBdModal'><i class='fa fa-eye' aria-hidden='true'></i></a>";
                                <c:if test="${isDataEntry}">
                                    btn += " || <a href='#!' id='btnEditMasterBd' data='"+data+"' class='col-blue'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>"+
                                            " || <a href='#!' id='btnDeleteMasterBd' data='"+data+"' class='col-red'><i class='fa fa-trash-o' aria-hidden='true'></i></a>";
                                </c:if>
                           return btn;
			 		       } },
			        ],
				};

			var oTable 	= $("#table-master-bd").dataTable(config);
			var oTable2 = $("#table-detail-master-bd").dataTable();
			var oTable3 = $("#table-list-barang-master-bd").dataTable();
            var oTable4 = $("#table-history-detail-barang-bd").dataTable();
			
			
			$("#btnSimpanBd").click(function(e){
				e.preventDefault();
				if($("#namaBd").val().length < 1){
					$("#namaBd").focus();
					return false;
				}
				
				Swal.fire({
			      	  title: 'Are you sure?',
			      	  icon: 'warning',
			      	  showCancelButton: true,
			      	  confirmButtonColor: '#3085d6',
			      	  cancelButtonColor: '#d33',
			      	  confirmButtonText: 'Simpan',
			      	  cancelButtonText: 'Batal'
			      	}).then((result) => {
			      	  if (result.isConfirmed) {
			      		$(".page-loader-wrapper").css("display","");
				    	
				    	var datas = {
			      				"namaBd" : $("#namaBd").val(),
			      				"deskripsiBd" : $("#deskripsiBd").val(),
			                }
			      		
			      		$.ajax({
			              	type: 'POST',
			              	contentType: "application/json",
			              	url: "<%= ApplConstant.URL_MASTER_BD_SAVE%>",
			              	data: JSON.stringify(datas),
			                  dataType: 'json',
			                  cache: false,
			              	success : function(){
			              		oTable.fnClearTable();
			              		oTable.fnDestroy();
			              	  	oTable = $('#table-master-bd').dataTable(config);
			              	  $(".page-loader-wrapper").css("display","none");
			              		Swal.fire({
			            			  icon: 'success',
			            			  text: "Berhasil Disimpan"
			            		});
			              		$("#namaBd").val("");
			              		 $("#deskripsiBd").val("")
			              	},
			              	error : function(data){
			              		$(".page-loader-wrapper").css("display","none");
			              		if(data.responseText == "1"){
			              			Swal.fire({
				              			  icon: 'warning',
				              			  title: 'Oops...',
				              			  text: "Bd yang anda masukkan sudah ada"
				              			});
			              		}else{ 
			              			Swal.fire({
				              			  icon: 'error',
				              			  title: 'Oops...',
				              			  text: "Error "
				              			});
			              		} 
			              			
			              	}
			              });
			      	  }
			    });
			});
			
			$("#table-master-bd tbody").on('click', '#btnEditMasterBd', function(e){
				e.preventDefault();
				$("#namaBd").focus();
				$("#namaBd").val($(this).parents('tr')[0].cells[1].innerHTML);
				$("#deskripsiBd").focus();
				$("#deskripsiBd").val($(this).parents('tr')[0].cells[2].innerHTML);
				$("#btnSimpanEditBd").attr("dataId",$(this).attr("data"));
				$("#btnSimpanEditBd").removeClass("hidden");
				$("#btnBatalSimpanBd").removeClass("hidden");
				$("#btnSimpanBd").addClass("hidden");
			});		
			
			$("#btnBatalSimpanBd").click(function(){
				$("#btnSimpanEditBd").addClass("hidden");
				$("#btnBatalSimpanBd").addClass("hidden");
				$("#btnSimpanBd").removeClass("hidden");
				$("#namaBd").val("");
				$("#deskripsiBd").val("");
			});
			
			$("#btnSimpanEditBd").click(function(e){
				Swal.fire({
			      	  title: 'Are you sure?',
			      	  icon: 'warning',
			      	  showCancelButton: true,
			      	  confirmButtonColor: '#3085d6',
			      	  cancelButtonColor: '#d33',
			      	  confirmButtonText: 'Simpan',
			      	  cancelButtonText: 'Batal'
			      	}).then((result) => {
			      	  if (result.isConfirmed) {
			      		$(".page-loader-wrapper").css("display","");
				    	
				    	var datas = {
				    			"id"	: $(this).attr("dataId"),
			      				"namaBd" : $("#namaBd").val(),
			      				"deskripsiBd" : $("#deskripsiBd").val(),
			                }
			      		
			      		$.ajax({
			              	type: 'POST',
			              	contentType: "application/json",
			              	url: "<%= ApplConstant.URL_MASTER_BD_EDIT%>",
			              	data: JSON.stringify(datas),
			                  dataType: 'json',
			                  cache: false,
			              	success : function(){
			              		oTable.fnClearTable();
			              		oTable.fnDestroy();
			              	  	oTable = $('#table-master-bd').dataTable(config);
			              	  $(".page-loader-wrapper").css("display","none");
			              		Swal.fire({
			            			  icon: 'success',
			            			  text: "Berhasil Diedit"
			            		});
			              		$("#btnSimpanEditBd").addClass("hidden");
			    				$("#btnBatalSimpanBd").addClass("hidden");
			    				$("#btnSimpanBd").removeClass("hidden");
			              		$("#namaBd").val("");
			              		 $("#deskripsiBd").val("")
			              	},
			              	error : function(data){
			              		$(".page-loader-wrapper").css("display","none");
			              		if(data.responseText == "1"){
			              			Swal.fire({
				              			  icon: 'warning',
				              			  title: 'Oops...',
				              			  text: "Bd yang anda masukkan sudah ada"
				              			});
			              		}else{ 
			              			Swal.fire({
				              			  icon: 'error',
				              			  title: 'Oops...',
				              			  text: "Error "
				              			});
			              		} 
			              			
			              	}
			              });
			      	  }
			    });
			});
			
			$("#table-master-bd tbody").on('click', '#btnDeleteMasterBd', function(e){
				e.preventDefault();
				
				Swal.fire({
			      	  title: 'Are you sure?',
			      	  icon: 'warning',
			      	  showCancelButton: true,
			      	  confirmButtonColor: '#3085d6',
			      	  cancelButtonColor: '#d33',
			      	  confirmButtonText: 'Simpan',
			      	  cancelButtonText: 'Batal'
			      	}).then((result) => {
			      	  if (result.isConfirmed) {
			      		$(".page-loader-wrapper").css("display","");
				    	
				    	var datas = {
				    			"id"	: $(this).attr("data")
			                }
			      		
			      		$.ajax({
			              	type: 'POST',
			              	contentType: "application/json",
			              	url: "<%= ApplConstant.URL_MASTER_BD_DELETE%>",
			              	data: JSON.stringify(datas),
			                  dataType: 'json',
			                  cache: false,
			              	success : function(){
			              		oTable.fnClearTable();
			              		oTable.fnDestroy();
			              	  	oTable = $('#table-master-bd').dataTable(config);
			              	  $(".page-loader-wrapper").css("display","none");
			              		Swal.fire({
			            			  icon: 'success',
			            			  text: "Berhasil Dihapus"
			            		});
			              		
			              	},
			              	error : function(e){
			              		$(".page-loader-wrapper").css("display","none");
			              		 
			              			Swal.fire({
				              			  icon: 'error',
				              			  title: 'Oops...',
				              			  text: "Error "
				              			});
			              			
			              	}
			              });
			      	  }
			    });
				
			});
			
			$("#table-master-bd tbody").on('click', '#btnDetailMasterBd', function(e){
				e.preventDefault();
				var bd = $(this).parents('tr')[0].cells[1].innerHTML;
				var idBd = $(this).attr("data");
				$("#addDetailMasterBdModalLabel").html("Detail "+bd);
                // $("#addDetailMasterBdModalLabel").html("Detail "+bd+" &nbsp;<button type='button' idBd='"+idBd+"' id='tombolTambahBarangDetailMasterBd' class='btn bg-pink' data-toggle='modal' data-target='#addBarangDetailMasterBdModal'>Tambah</button>");
				var config2 = { 
						"responsive": true,
						"aLengthMenu": [[5, 10, 25], [5, 10, 25]],
						"iDisplayLength":5,
				       	"bLengthChange": true,
				       	/* "bFilter":false, */
				       	"bProcessing": true,
				       	"bServerSide": true,
				       	"sAjaxSource": "<%= ApplConstant.URL_DETAIL_MASTER_BD_DATATABLE+"?idBd=" %>"+idBd,
				       	"sAjaxDataProp": "data.content",
				       	"sServerMethod": "POST",
					   "fnCreatedRow": function (row, data, index) {
				            $( row ).find('td:eq(0)').html(index + 1);
				        },
				       	"aoColumns": [
				       		{ "mData": null, "sClass":"dt-head-center dt-body-center vertical-middle" },
				            { "mData": "namaBarang", "sClass":"dt-head-center dt-body-center vertical-middle"},
				            { "mData": "jumlah", "sClass":"dt-head-center dt-body-center vertical-middle"},
				            { "mData": "id", "sClass":"dt-head-center dt-body-center vertical-middle","mRender":function(data,type,row){

				 		    	   return "<a href='#!' id='btnHistoryDetailMasterBd' idBd='"+idBd+"' idBarang='"+row.idBarang+"' data='"+data+"' class='col-pink' data-toggle='modal' data-target='#historyBarangDetailBdModal'><i class='fa fa-eye' aria-hidden='true'></i></a>"
				 		       } },
				        ],
					};
				
				oTable2.fnClearTable();
          		oTable2.fnDestroy();
          	  	oTable2 = $('#table-detail-master-bd').dataTable(config2);
          	  	$("#table-detail-master-bd").css({"width":"100%","color":"black"});
				
				$("#tombolTambahBarangDetailMasterBd").click(function(){
					var namaBd = $(this).attr("idBd");
					$("#btnSimpanListBarangMasterBd").attr("idBd",idBd);
					$("#searchBarangDetailMasterBd").attr("idBd",idBd);
				});
			});	
			
			$("#searchBarangDetailMasterBd").click(function(){
				var idBd = $(this).attr("idBd");
				var config3 = { 
						"responsive": true,
						"aLengthMenu": [[5, 10, 25], [5, 10, 25]],
						"iDisplayLength":5,
				       	"bLengthChange": true,
				       	/* "bFilter":false, */
				       	"bProcessing": true,
				       	"bServerSide": true,
				       	"sAjaxSource": "<%= ApplConstant.URL_MASTER_BD_LIST_BARANG_DATATABLE+"?idBd=" %>"+idBd,
				       	"sAjaxDataProp": "data.content",
				       	"sServerMethod": "POST",
					   "fnCreatedRow": function (row, data, index) {
				            $( row ).find('td:eq(0)').html(index + 1);
				        },
				       	"aoColumns": [
				       		{ "mData": null, "sClass":"dt-head-center dt-body-center vertical-middle" },
				            { "mData": "id", "sClass":"dt-head-center dt-body-center vertical-middle hidden"},
				            { "mData": "kodeBarang", "sClass":"dt-head-center dt-body-center vertical-middle"},
				            { "mData": "namaBarang", "sClass":"dt-head-center dt-body-left vertical-middle"},
				        ],
					};
				
				oTable3.fnClearTable();
          		oTable3.fnDestroy();
          	  	oTable3 = $('#table-list-barang-master-bd').dataTable(config3);
          	  	$('#table-list-barang-master-bd').css("width","100%");
			});
			
			$("#table-list-barang-master-bd tbody").on('click', 'tr', function(e){
				var id = $(this).context.children[1].innerText;
				var kodeBarang = $(this).context.children[2].innerText;
				var namaBarang = $(this).context.children[3].innerText;
				$("#idBarang").val(id);
				$("#kodeBarang").removeAttr("disabled");
				$("#kodeBarang").focus();
				$("#kodeBarang").val(kodeBarang);
				$("#kodeBarang").attr("disabled","disabled");
				$("#namaBarang").removeAttr("disabled");
				$("#namaBarang").focus();
				$("#namaBarang").val(namaBarang);
				$("#namaBarang").attr("disabled","disabled");
				$("#btnTutupListBarangMasterBd").click();
			});
			
			$("#btnSimpanListBarangMasterBd").click(function(e){
				e.preventDefault();
				
				Swal.fire({
			      	  title: 'Are you sure?',
			      	  icon: 'warning',
			      	  showCancelButton: true,
			      	  confirmButtonColor: '#3085d6',
			      	  cancelButtonColor: '#d33',
			      	  confirmButtonText: 'Simpan',
			      	  cancelButtonText: 'Batal'
			      	}).then((result) => {
			      	  if (result.isConfirmed) {
			      		$(".page-loader-wrapper").css("display","");
				    	var idBd = $(this).attr("idBd");
				    	var datas = {
				    			"idBd" : idBd,
				    			"idBarang"	: $("#idBarang").val(),
				    			"srJumlahBarang" : $("#jumlahBarang").val()
			                }
				    	
				    	var config2 = { 
								"responsive": true,
								"aLengthMenu": [[5, 10, 25], [5, 10, 25]],
								"iDisplayLength":5,
						       	"bLengthChange": true,
						       	/* "bFilter":false, */
						       	"bProcessing": true,
						       	"bServerSide": true,
						       	"sAjaxSource": "<%= ApplConstant.URL_DETAIL_MASTER_BD_DATATABLE+"?idBd=" %>"+idBd,
						       	"sAjaxDataProp": "data.content",
						       	"sServerMethod": "POST",
							   "fnCreatedRow": function (row, data, index) {
						            $( row ).find('td:eq(0)').html(index + 1);
						        },
						       	"aoColumns": [
						       		{ "mData": null, "sClass":"dt-head-center dt-body-center vertical-middle" },
						            { "mData": "namaBarang", "sClass":"dt-head-center dt-body-center vertical-middle"},
						            { "mData": "jumlah", "sClass":"dt-head-center dt-body-left vertical-middle"},
						            { "mData": "id", "sClass":"dt-head-center dt-body-center vertical-middle","mRender":function(data,type,row){
						            	
						 		    	   return "<a href='#!' id='btnDeleteDetailMasterBd' idBd='"+idBd+"' data='"+data+"' class='col-red'><i class='fa fa-trash-o' aria-hidden='true'></i></a>"
						 		       } },
						        ],
							};
			      		
			      		$.ajax({
			              	type: 'POST',
			              	contentType: "application/json",
			              	url: "<%= ApplConstant.URL_MASTER_BD_SAVE_DETAIL%>",
			              	data: JSON.stringify(datas),
			                  dataType: 'json',
			                  cache: false,
			              	success : function(){
			              		oTable2.fnClearTable();
			              		oTable2.fnDestroy();
			              	  	oTable2 = $('#table-detail-master-bd').dataTable(config2);
			              	  $("#table-detail-master-bd").css({"width":"100%","color":"black"});
			              	  $("#kodeBarang").val("");
                             $("#namaBarang").val("");
                             $("#jumlahBarang").val("");
			              	  $("#btnTutupAddListBarangMasterBd").click();
			              	  $(".page-loader-wrapper").css("display","none");
			              		Swal.fire({
			            			  icon: 'success',
			            			  text: "Berhasil Ditambahkan"
			            		});
			              		
			              	},
			              	error : function(e){
			              		$(".page-loader-wrapper").css("display","none");
			              		 
			              			Swal.fire({
				              			  icon: 'error',
				              			  title: 'Oops...',
				              			  text: "Error "
				              			});
			              			
			              	}
			              });
			      	  }
			    });
			});
			
			$("#table-detail-master-bd tbody").on('click', '#btnHistoryDetailMasterBd', function(e){
				e.preventDefault();
                var idBd = $(this).attr("idBd");
                var idBarang = $(this).attr("idBarang");
                var con = {
                        "responsive": true,
                        "aLengthMenu": [[5, 10, 25], [5, 10, 25]],
                        "iDisplayLength":5,
                        "bLengthChange": true,
                        /* "bFilter":false, */
                        "bProcessing": true,
                        "bServerSide": true,
                        "sAjaxSource": "<%= ApplConstant.URL_HISTORY_BARANG_DETAIL_MASTER_BD_DATATABLE+"?idBd=" %>"+idBd+"&idBarang="+idBarang,
                        "sAjaxDataProp": "data.content",
                        "sServerMethod": "POST",
                       "fnCreatedRow": function (row, data, index) {
                            $( row ).find('td:eq(0)').html(index + 1);
                        },
                        "aoColumns": [
                            { "mData": null, "sClass":"dt-head-center dt-body-center vertical-middle" },
                            { "mData": "namaBarang", "sClass":"dt-head-center dt-body-left vertical-middle"},
                            { "mData": "jumlah", "sClass":"dt-head-center dt-body-center vertical-middle"},
                            { "mData": "penerima", "sClass":"dt-head-center dt-body-left vertical-middle"},
                            { "mData": "tanggal", "sClass":"dt-head-center dt-body-center vertical-middle"},
                        ],
                    };

                oTable4.fnClearTable();
                oTable4.fnDestroy();
                // $('#table-history-detail-barang-bd').empty();
                oTable4 = $("#table-history-detail-barang-bd").dataTable(con);
                $("#table-history-detail-barang-bd").css({"width":"100%","color":"black"});
			});
			
		});
		
		function isNumberKey(evt){
			var charCode = (evt.which) ? evt.which : evt.keyCode
 		 		    if(
 		 		    	!(charCode > 31 && (charCode < 48 || charCode > 57))
 		 		    	|| (charCode == 46)
 		 		    ){
 		 		    	return true;	
 		 		    }else{
 		 		    	
 		 		    	return false;
 		 		    }
 		}
	</script>
</body>
</html>