<!DOCTYPE html>
<%@page import="com.faiq.eka.constant.ApplConstant"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String resourceLink = "/"; %>

<html lang="en">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="muh nailur rohman faiq">
    <meta name="keyword" content="Dashboard">

    <title>EKA STOKIS APPS</title>
    <jsp:include page="../general/header_import.jsp"></jsp:include>
</head>
<body class="theme-blue-grey">

<jsp:include page="../general/header.jsp"></jsp:include>
<jsp:include page="../general/menu.jsp">
	<jsp:param name="menuact" value="daftarKeluarMasukBarang" />
</jsp:include>
<jsp:include page="../lookup/lookup-list-bd.jsp"></jsp:include>
<jsp:include page="../lookup/lookup-list-bagian.jsp"></jsp:include>
<jsp:include page="../lookup/lookup-stok-barang.jsp"></jsp:include>
<section class="content">
    <div class="container-fluid">
        <ol class="breadcrumb breadcrumb-bg-deep-purple">
            <li><a href="javascript:void(0);"><i class="fa fa-home fa-lg m-t-10" aria-hidden="true"></i> Home</a></li>
            <li class="active"><i class="fa fa-calendar-o fa-lg m-t-10" aria-hidden="true"></i> Daftar Keluar Masuk Barang</li>
        </ol>

        <div class="row clearfix">
        	<div class="col-lg-12">
            	<div class="card">
		            <div class="header bg-deep-purple">
		                <h2>
		                    Daftar Keluar Masuk Barang 
		                </h2>
		            </div>
		            <div class="body">
		            	<div class="row">
		            		<div class="col-lg-12">
				            	<div class="card">
				            		<!-- <div class="header bg-blue">
				                         <h2>
				                             Daftar Barang &nbsp;<button type="button" class="btn bg-pink" data-toggle="modal" data-target="#addBarangModal">Tambah</button>
				                         </h2>
				                     </div> -->
				                     <div class="body">
				                     	<div class="table-responsive">
				                            <table class="table table-bordered table-striped table-hover" id="table-keluar-masuk-barang" style="width: 200%;">
				                                <thead>
				                                    <tr>
				                                        <th>No</th>
				                                        <th>Nama BD</th>
				                                        <th>Kode Barang</th>
				                                        <th>Nama Barang</th>
				                                        <th>Jumlah</th>
				                                        <th>Satuan</th>
				                                        <th>Penerima</th>
														 <th>Bagian</th>
				                                        <th>Tanggal Diterima</th>
				                                        <th>Catatan</th>
														 <th>Status</th>
				                                        <th>Aksi</th>
				                                    </tr>
				                                </thead>
				                                <tbody>
				                                </tbody>
				                             </table>
				                         </div>
				                     </div>
				            	</div>
				            </div>
		            	</div>
		            	
		            </div>
		       </div>
		  </div>
        </div>
        <div class="row clearfix">
            
            
        </div>
    </div>
</section>
 	
 	<jsp:include page="../general/bottom_import.jsp"></jsp:include>
 	
<script type="text/javascript">
	$(document).ready(function(){
		
		var config = {
            	"bStateSave": true,
				"responsive": true,
				"aLengthMenu": [[5, 10, 25], [5, 10, 25]],
				"iDisplayLength":5,
		       	"bLengthChange": true,
		       	/* "bFilter":false, */
		       	"bProcessing": true,
		       	"bServerSide": true,
		       	"sAjaxSource": "<%= ApplConstant.URL_DAFTAR_KELUAR_MASUK_DATATABLE %>",
		       	"sAjaxDataProp": "data.content",
		       	"sServerMethod": "POST",
			   "fnCreatedRow": function (row, data, index) {
		            $( row ).find('td:eq(0)').html(index + 1);
		        },
		       	"aoColumns": [
		       		{ "mData": null, "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"2%" },
		            { "mData": "namaBd", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"8%", "mRender":function(data,type,row){
                            return "<div id='label-nama-bd-"+data+"-"+row.id+"' class=''>"+
                                "<span>"+((data==null || data == '')? '':data)+"</span>"+
                                "</div>"+
                                "<div id='input-nama-bd-"+data+"-"+row.id+"' class='hidden'>"+
                                "<input type='text' class='form-control text-center' id='nama-bd-"+data+"-"+row.id+"' value='"+((data==null || data == '')? '':data)+"' readonly='readonly'>"+
								 "<input type='hidden' id='hidden-nama-bd-"+data+"-"+row.id+"' value='"+row.idBd+"'>"+
								 "<button type='button' class='btn-sm btn-warning' data='"+data+"-"+row.id+"' id='searchBd' data-toggle='modal' data-target='#listBdKeluarMasukModal'><i class='fa fa-search' aria-hidden='true'></i></button>"+
                                "</div>";
						}},
		            { "mData": "kodeBarang", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"10%"},
		            { "mData": "namaBarang", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"20%"},
		            { "mData": "jumlah", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"5%"},
		            { "mData": "satuan", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"5%"},
		            { "mData": "penerima", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"5%", "mRender":function(data,type,row){
                            return "<div id='label-penerima-"+row.id+"' class=''>"+
                                "<span>"+((data==null || data == '')? '':data)+"</span>"+
                                "</div>"+
                                "<div id='input-penerima-"+row.id+"' class='hidden'>"+
                                "<textarea class='form-control text-center' rows='1' id='penerima-"+row.id+"'>"+((data==null || data == '')? '':data)+"</textarea>"+
                                "</div>";
                        }},
                    { "mData": "bagian", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"10%", "mRender":function(data,type,row){
                            return "<div id='label-bagian-"+row.id+"' class=''>"+
                                "<span>"+((data==null || data == '')? '':data)+"</span>"+
                                "</div>"+
                                "<div id='input-bagian-"+row.id+"' class='hidden'>"+
                                "<input type='text' class='form-control text-center' id='bagian-"+row.id+"' value='"+((data==null || data == '')? '':data)+"' readonly='readonly'>"+
                                "<input type='hidden' id='hidden-bagian-"+row.id+"' value='"+row.idBagian+"'>"+
                                "<button type='button' class='btn-sm btn-warning' data='"+row.id+"' id='searchBagian' data-toggle='modal' data-target='#listBagianModal'><i class='fa fa-search' aria-hidden='true'></i></button>"+
                                "</div>";
                        }},
		            { "mData": "tanggal", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"10%", "mRender":function(data,type,row){
                        	var tanggal = data.split("-");
		                return "<div id='label-tanggal-"+row.id+"' class=''>"+
                                "<span>"+((data==null || data == '')? '':tanggal[2]+"-"+tanggal[1]+"-"+tanggal[0])+"</span>"+
                                "</div>"+
                                "<div id='input-tanggal-"+row.id+"' class='hidden'>"+
                                "<input type='date' class='form-control text-center' rows='3' id='tanggal-"+row.id+"' value='"+((data==null || data == '')? '':data)+"'>"+
                                "</div>";
                        }},
		            { "mData": "deskripsi", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"10%", "mRender":function(data,type,row){
                            return "<div id='label-deskripsi-"+row.id+"' class=''>"+
                                "<span>"+((data==null || data == '')? '':data)+"</span>"+
                                "</div>"+
                                "<div id='input-deskripsi-"+row.id+"' class='hidden'>"+
                                "<textarea class='form-control text-center' rows='3' id='deskripsi-"+row.id+"'>"+((data==null || data == '')? '':data)+"</textarea>"+
                                "</div>";
                        }},
                    { "mData": "status", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"5%","mRender":function (data,type,row) {
							var status;
							if (data == "KELUAR"){
							    status = "<span class='btn-sm btn-info'>"+data+"</span>";
							}else if (data == "KEMBALI"){
                                status = "<span class='btn-sm btn-danger'>"+data+"</span>";
							}
                        	return status;
                        }},
		            { "mData": "id", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"5%","mRender":function(data,type,row){
		                var btn = "<i class='fa fa-close hidden' aria-hidden='true'></i>";
		                if (row.status != "KEMBALI"){
                            var first = "<div id='btn-first-"+data+"'>";
                            var second = "";
                            var third 	= "<a href='#!' id='btnReturnKeluarMasukBarang' data='"+data+"' title='Kembalikan' class='col-blue' data-toggle='modal' data-target='#stokBarangModal'><i class='fa fa-undo' aria-hidden='true'></i></a>"+
                                "</div>"+
                                "<div id='btn-second-"+data+"' class='hidden'>"+
                                "<a href='#!' id='btnEditSimpanNamaBd' data='"+data+"' title='Simpan' namaBd='"+row.namaBd+"-"+data+"' class='col-green' ><i class='fa fa-check-circle-o' aria-hidden='true'></i></a>"+
                                " || <a href='#!' id='btnCancelNamaBd' data='"+data+"' title='Batal' namaBd='"+row.namaBd+"-"+data+"' class='col-red'><i class='fa fa-times' aria-hidden='true'></i></a>"+
                                "</div>";
                            // if(row.namaBd == null || row.namaBd == ""){
                                second = "<a href='#!' id='btnEditNamaBd' data='"+data+"' title='Edit' namaBd='"+row.namaBd+"-"+data+"' class='col-blue' ><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a> || ";
                            // }
                            btn = first+second+third
						}
		 		    	   return btn;
		 		       } },
		        ],
			};

        var configBd = {
            "responsive": true,
            "aLengthMenu": [[5, 10, 25], [5, 10, 25]],
            "iDisplayLength":5,
            "bLengthChange": true,
            /* "bFilter":false, */
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<%= ApplConstant.URL_LIST_BD_DATATABLE %>",
            "sAjaxDataProp": "data.content",
            "sServerMethod": "POST",
            "fnCreatedRow": function (row, data, index) {
                $( row ).find('td:eq(0)').html(index + 1);
            },
            "aoColumns": [
                { "mData": null, "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"5%" },
                { "mData": "id", "sClass":"dt-head-center dt-body-center vertical-middle hidden","sWidth":"20%"},
                { "mData": "namaBd", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"20%"},
                { "mData": "deskripsiBd", "sClass":"dt-head-center dt-body-left vertical-middle","sWidth":"50%"}
            ],
        };

        var configBagian = {
            "responsive": true,
            "aLengthMenu": [[5, 10, 25], [5, 10, 25]],
            "iDisplayLength":5,
            "bLengthChange": true,
            /* "bFilter":false, */
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<%= ApplConstant.URL_DAFTAR_BAGIAN %>",
            "sAjaxDataProp": "data.content",
            "sServerMethod": "POST",
            "fnCreatedRow": function (row, data, index) {
                $( row ).find('td:eq(0)').html(index + 1);
            },
            "aoColumns": [
                { "mData": null, "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"5%" },
                { "mData": "id", "sClass":"dt-head-center dt-body-center vertical-middle hidden","sWidth":"20%"},
                { "mData": "deskripsi", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"20%"},
            ],
        };
			
		var oTable = $("#table-keluar-masuk-barang").dataTable(config);
        var oTableBd = $("#table-list-bd").dataTable();
        var oTableBagian = $("#table-list-bagian").dataTable();

        $("#table-keluar-masuk-barang tbody").on('click', '#btnEditNamaBd', function(e) {
            e.preventDefault();
            var namaBd 		= $(this).attr("namaBd");
            var id		   	= $(this).attr("data");
            var labelNamaBd = "#label-nama-bd-"+namaBd;
            var inputNamaBd = "#input-nama-bd-"+namaBd;
            var labelDeskripsi = "#label-deskripsi-"+id;
            var inputDeskripsi = "#input-deskripsi-"+id;
            var labelTanggal = "#label-tanggal-"+id;
            var inputTanggal = "#input-tanggal-"+id;
            var labelPenerima = "#label-penerima-"+id;
            var inputPenerima = "#input-penerima-"+id;
            var labelBagian = "#label-bagian-"+id;
            var inputBagian = "#input-bagian-"+id;
            var btnFirstBarang	= "#btn-first-"+id;
            var btnSecondBarang	= "#btn-second-"+id;
            $(labelNamaBd).addClass("hidden");
            $(inputNamaBd).removeClass("hidden");
            $(labelDeskripsi).addClass("hidden");
            $(inputDeskripsi).removeClass("hidden");
            $(labelTanggal).addClass("hidden");
            $(inputTanggal).removeClass("hidden");
            $(labelPenerima).addClass("hidden");
            $(inputPenerima).removeClass("hidden");
            $(labelBagian).addClass("hidden");
            $(inputBagian).removeClass("hidden");
            $(btnFirstBarang).addClass("hidden");
            $(btnSecondBarang).removeClass("hidden");
        });

        $("#table-keluar-masuk-barang tbody").on('click', '#btnCancelNamaBd', function(e) {
            e.preventDefault();
            var namaBd 		= $(this).attr("namaBd");
            var id		   	= $(this).attr("data");
            var labelNamaBd = "#label-nama-bd-"+namaBd;
            var inputNamaBd = "#input-nama-bd-"+namaBd;
            var labelDeskripsi = "#label-deskripsi-"+id;
            var inputDeskripsi = "#input-deskripsi-"+id;
            var labelTanggal = "#label-tanggal-"+id;
            var inputTanggal = "#input-tanggal-"+id;
            var labelPenerima = "#label-penerima-"+id;
            var inputPenerima = "#input-penerima-"+id;
            var labelBagian = "#label-bagian-"+id;
            var inputBagian = "#input-bagian-"+id;
            var btnFirstBarang	= "#btn-first-"+id;
            var btnSecondBarang	= "#btn-second-"+id;
            $(labelNamaBd).removeClass("hidden");
            $(inputNamaBd).addClass("hidden");
            $(labelDeskripsi).removeClass("hidden");
            $(inputDeskripsi).addClass("hidden");
            $(labelTanggal).removeClass("hidden");
            $(inputTanggal).addClass("hidden");
            $(labelPenerima).removeClass("hidden");
            $(inputPenerima).addClass("hidden");
            $(labelBagian).removeClass("hidden");
            $(inputBagian).addClass("hidden");
            $(btnFirstBarang).removeClass("hidden");
            $(btnSecondBarang).addClass("hidden");

        });

        $("#table-keluar-masuk-barang tbody").on('click', '#searchBd', function(e) {
            var namaBd 		= $(this).attr("data");
            var inputNamaBd = "#nama-bd-"+namaBd;
            var hiddenNamaBd = "#hidden-nama-bd-"+namaBd;

            oTableBd.fnClearTable();
            oTableBd.fnDestroy();
            oTableBd = $('#table-list-bd').dataTable(configBd);
            $("#table-list-bd").css("width","100%");

            $("#table-list-bd tbody").on('click', 'tr', function(e){
                var idBd = $(this).context.children[1].innerText;
                var kodeBd = $(this).context.children[2].innerText;
                $(hiddenNamaBd).val(idBd);
                $(inputNamaBd).val(kodeBd);
                $("#btnTutupListBd").click();
            });
        });

        $("#table-keluar-masuk-barang tbody").on('click', '#searchBagian', function(e) {
            var id 		= $(this).attr("data");
            var inputNamaBagian = "#bagian-"+id;
            var hiddenNamaBagian = "#hidden-bagian-"+id;

            oTableBagian.fnClearTable();
            oTableBagian.fnDestroy();
            oTableBagian = $('#table-list-bagian').dataTable(configBagian);
            $("#table-list-bagian").css("width","100%");

            $("#table-list-bagian tbody").on('click', 'tr', function(e){
                var idBagian = $(this).context.children[1].innerText;
                var bagian = $(this).context.children[2].innerText;
                $(hiddenNamaBagian).val(idBagian);
                $(inputNamaBagian).val(bagian);
                $("#btnTutupListBagian").click();
            });
        });

        $("#table-keluar-masuk-barang tbody").on('click', '#btnEditSimpanNamaBd', function(e){
			e.preventDefault();
			var id 			= $(this).attr("data");
            var namaBd 		= $(this).attr("namaBd");
            var hiddenNamaBd = "#hidden-nama-bd-"+namaBd;
            var deskripsi = $("#deskripsi-"+id).val();
            var tanggal = $("#tanggal-"+id).val();
            var penerima = $("#penerima-"+id).val();
            var bagian = $("#hidden-bagian-"+id).val();

			Swal.fire({
				title: 'Are you sure?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Simpan',
				cancelButtonText: 'Batal'
			}).then((result) => {
				if (result.isConfirmed) {
				$(".page-loader-wrapper").css("display","");

				var datas = {
					"id"	: id,
					"idBd": $(hiddenNamaBd).val(),
					"catatan":deskripsi,
					"tanggalOrder":tanggal,
					"penerima":penerima,
					"idBagian":bagian
				}

				$.ajax({
					type: 'POST',
					contentType: "application/json",
					url: "<%= ApplConstant.URL_DAFTAR_KELUAR_MASUK_EDIT%>",
					data: JSON.stringify(datas),
					dataType: 'json',
					cache: false,
					success : function(){
						oTable.fnClearTable();
						oTable.fnDestroy();
						oTable = $('#table-keluar-masuk-barang').dataTable(config);
						$(".page-loader-wrapper").css("display","none");
						Swal.fire({
							icon: 'success',
							text: "Berhasil Diedit"
						});

					},
					error : function(e){
						$(".page-loader-wrapper").css("display","none");

						Swal.fire({
							icon: 'error',
							title: 'Oops...',
							text: "Error "+e
						});

					}
				});
			}
			});
		});

        $("#table-keluar-masuk-barang tbody").on('click', '#btnReturnKeluarMasukBarang', function(e){
            e.preventDefault();
            var id 			= $(this).attr("data");
			$("#stokBarangModalLabel").html("Kembalikan Stok");
            $("#btnReturnStokBarang").removeClass("hidden");
            $("#btnSimpanAddStokBarang").addClass("hidden");
            $("#btnSimpanRemoveStokBarang").addClass("hidden");
            $("#btnReturnStokBarang").attr("data",id);
        });

        $("#btnReturnStokBarang").click(function (e) {
            var id 			= $(this).attr("data");
            var stokKembali = $("#ubahStokBarang").val();
            Swal.fire({
            title: 'Are you sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Simpan',
            cancelButtonText: 'Batal'
            }).then((result) => {
				if (result.isConfirmed) {
					$(".page-loader-wrapper").css("display","");

					var datas = {
						"id"	: id,
						"jumlahBarang" : stokKembali
					}

						$.ajax({
							type: 'POST',
							contentType: "application/json",
							url: "<%= ApplConstant.URL_DAFTAR_KELUAR_MASUK_KEMBALI%>",
							data: JSON.stringify(datas),
							dataType: 'json',
							cache: false,
							success : function(){
								oTable.fnClearTable();
								oTable.fnDestroy();
								oTable = $('#table-keluar-masuk-barang').dataTable(config);
								$(".page-loader-wrapper").css("display","none");
								$("#btnTutupUbahMasterBarang").click();
								Swal.fire({
								icon: 'success',
								text: "Berhasil Dikembalikan"
								});
							},
							error : function(e){
							$(".page-loader-wrapper").css("display","none");
							if(e.status == 416){
                                Swal.fire({
                                	icon: 'warning',
                                	title: 'Oops...',
                                	text: "Jumlah pengembalian barang melebihi jumlah barang yang diorder"
                                });
							}else{
                                Swal.fire({
                                	icon: 'error',
                                	title: 'Oops...',
                                	text: "Error "+e
                                });
							}

							}
						});
				}
            });
        });
			
	});
	
	function isNumberKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return !(charCode > 31 && (charCode < 48 || charCode > 57));
		}

</script>
	
</body>
</html>