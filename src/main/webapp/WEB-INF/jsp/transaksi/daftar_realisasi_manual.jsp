<!DOCTYPE html>
<%@page import="com.faiq.eka.constant.ApplConstant"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String resourceLink = "/"; %>

<html lang="en">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="muh nailur rohman faiq">
    <meta name="keyword" content="Dashboard">

    <title>EKA STOKIS APPS</title>
    <jsp:include page="../general/header_import.jsp"></jsp:include>
</head>
<body class="theme-blue-grey">

<jsp:include page="../general/header.jsp"></jsp:include>
<jsp:include page="../general/menu.jsp">
	<jsp:param name="menuact" value="daftarRealisasiManual" />
</jsp:include>
<section class="content">
    <div class="container-fluid">
        <ol class="breadcrumb breadcrumb-bg-deep-purple">
            <li><a href="javascript:void(0);"><i class="fa fa-home fa-lg m-t-10" aria-hidden="true"></i> Home</a></li>
            <li class="active"><i class="fa fa-calendar-o fa-lg m-t-10" aria-hidden="true"></i> Daftar Realisasi Manual</li>
        </ol>

        <div class="row clearfix">
        	<div class="col-lg-12">
            	<div class="card">
		            <div class="header bg-deep-purple">
		                <h2>
		                    Daftar Realisasi Manual
		                </h2>
		            </div>
		            <div class="body">
		            	<div class="row">
		            		<div class="col-lg-12">
				            	<div class="card">
				            		<!-- <div class="header bg-blue">
				                         <h2>
				                             Daftar Barang &nbsp;<button type="button" class="btn bg-pink" data-toggle="modal" data-target="#addBarangModal">Tambah</button>
				                         </h2>
				                     </div> -->
				                     <div class="body">
				                     	<div class="table-responsive">
				                            <table class="table table-bordered table-striped table-hover" id="table-realisasi-manual" style="width: 200%;">
				                                <thead>
				                                    <tr>
				                                        <th>No</th>
				                                        <th>Nama BD</th>
				                                        <th>Kode Barang</th>
				                                        <th>Nama Barang</th>
				                                        <th>Jumlah</th>
				                                        <th>Satuan</th>
				                                        <th>Penerima</th>
				                                        <th>Tanggal Diterima</th>
				                                        <th>Aksi</th>
				                                    </tr>
				                                </thead>
				                                <tbody>
				                                </tbody>
				                             </table>
				                         </div>
				                     </div>
				            	</div>
				            </div>
		            	</div>
		            	
		            </div>
		       </div>
		  </div>
        </div>
        <div class="row clearfix">
            
            
        </div>
    </div>
</section>
 	
 	<jsp:include page="../general/bottom_import.jsp"></jsp:include>
 	
<script type="text/javascript">
	$(document).ready(function(){
		
		
		var config = {
            	"bStateSave": true,
				"responsive": true,
				"aLengthMenu": [[5, 10, 25], [5, 10, 25]],
				"iDisplayLength":5,
		       	"bLengthChange": true,
		       	/* "bFilter":false, */
		       	"bProcessing": true,
		       	"bServerSide": true,
		       	"sAjaxSource": "<%= ApplConstant.URL_DAFTAR_REALISASI_MANUAL_DATATABLE %>",
		       	"sAjaxDataProp": "data.content",
		       	"sServerMethod": "POST",
			   "fnCreatedRow": function (row, data, index) {
		            $( row ).find('td:eq(0)').html(index + 1);
		        },
		       	"aoColumns": [
		       		{ "mData": null, "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"2%" },
		            { "mData": "namaBd", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"8%"},
		            { "mData": "kodeBarang", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"10%"},
		            { "mData": "namaBarang", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"20%"},
		            { "mData": "jumlah", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"5%"},
		            { "mData": "satuan", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"5%"},
		            { "mData": "penerima", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"5%"},
		            { "mData": "tanggal", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"10%"},
		            { "mData": "id", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"5%","mRender":function(data,type,row){
		            	
		 		    	   return "<a href='#!' id='btnReturnRealisasiManual' data='"+data+"' title='Kembalikan' class='col-blue' ><i class='fa fa-undo' aria-hidden='true'></i></a>"
		 		    	   		 ;
		 		       } },
		        ],
			};
			
			oTable = $("#table-realisasi-manual").dataTable(config);
			
	});
	
	function isNumberKey(evt){
		    var charCode = (evt.which) ? evt.which : evt.keyCode
		    return !(charCode > 31 && (charCode < 48 || charCode > 57));
		}
</script>
	
</body>
</html>