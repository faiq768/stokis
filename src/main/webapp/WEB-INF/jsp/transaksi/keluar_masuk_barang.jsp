<!DOCTYPE html>
<%@page import="com.faiq.eka.constant.ApplConstant"%>
<%@ page import="com.faiq.eka.persistence.entity.MasterBagian" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String resourceLink = "/"; %>

<html lang="en">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="muh nailur rohman faiq">
    <meta name="keyword" content="Dashboard">

    <title>EKA STOKIS APPS</title>
    <jsp:include page="../general/header_import.jsp"></jsp:include>
</head>
<body class="theme-blue-grey">

<jsp:include page="../general/header.jsp"></jsp:include>
<jsp:include page="../general/menu.jsp">
	<jsp:param name="menuact" value="keluarMasukBarang" />
</jsp:include>
<jsp:include page="../lookup/lookup-list-barang.jsp"></jsp:include>
<jsp:include page="../lookup/lookup-list-bd.jsp"></jsp:include>
<section class="content">
    <div class="container-fluid">
        <ol class="breadcrumb breadcrumb-bg-deep-purple">
            <li><a href="javascript:void(0);"><i class="fa fa-home fa-lg m-t-10" aria-hidden="true"></i> Home</a></li>
            <li class="active"><i class="fa fa-calendar-o fa-lg m-t-10" aria-hidden="true"></i> Keluar Masuk Barang</li>
        </ol>

        <div class="row clearfix">
        	<div class="col-lg-12">
            	<div class="card">
		            <div class="header bg-deep-purple">
		                <h2>
		                    Keluar Masuk Barang 
		                </h2>
		            </div>
		            <div class="body">
		            	<div class="row">
		            		<div class="col-lg-2">
		            			<div class="form-group form-float">
		                            <div class="form-line">
		                            <input type="hidden" id="idBd" >
		                                <input type="text" id="bd" class="form-control" maxLength="5" autocomplete="off" disabled>
		                                <label class="form-label">BD</label>
		                            </div>
		                        </div>
		            		</div>
		            		<div class="col-lg-2">
		            			<button type="button" class="btn btn-sm btn-danger hidden" id="cancelBd" ><i class="fa fa-close" aria-hidden="true"></i></button>
		            			<button type="button" class="btn btn-sm btn-warning" id="searchBd" data-toggle="modal" data-target="#listBdKeluarMasukModal"><i class="fa fa-search" aria-hidden="true"></i></button>
		            		</div>
		            		<div class="col-lg-3">
		            			<div class="form-group form-float">
		                            <div class="form-line">
		                            <input type="hidden" id="idBarang" >
		                                <input type="text" id="kodeBarang" class="form-control" maxLength="5" autocomplete="off" disabled>
		                                <label class="form-label">Kode Barang</label>
		                            </div>
		                        </div>
		            		</div>
		            		<div class="col-lg-1">
		            			<button type="button" class="btn btn-sm btn-warning" id="searchBarang" data-toggle="modal" data-target="#listBarangKeluarMasukModal"><i class="fa fa-search" aria-hidden="true"></i></button>
		            		</div>
		            	</div>
		            	<div class="row">
		            		<div class="col-lg-6">
		            			<div class="form-group form-float">
		                            <div class="form-line">
		                                <input type="text" id="namaBarang" class="form-control" maxLength="5" autocomplete="off" disabled>
		                                <label class="form-label">Nama Barang</label>
		                            </div>
		                        </div>
		            		</div>
		            		<div class="col-lg-3">
		            			<div class="form-group form-float">
		                            <div class="form-line">
		                                <input type="text" id="penerima" class="form-control" maxLength="200" autocomplete="off">
		                                <label class="form-label">Penerima</label>
		                            </div>
		                        </div>
		            		</div>
							<div class="col-lg-3">
								<select class="form-control show-tick" id="bagian">
									<option value="">-- Pilih Bagian --</option>
									<%
										List<MasterBagian> lBagian = (List<MasterBagian>)request.getAttribute("lBagian");
										int index = 0;
										int dataN = 1;
										String alp = "";
										for(MasterBagian bg : lBagian){
									%>
									<option value="<%= bg.getId() %>"><%= bg.getDeskripsi() %></option>
									<%
										}
									%>

								</select>
							</div>
		            	</div>
		            	<div class="row">
		            		<div class="col-lg-2">
		            			<div class="form-group form-float">
		                            <div class="form-line">
										<input type="hidden" id="stokBarangKeseluruhan">
										<input type="hidden" id="nominalIndukSatuan">
		                                <input type="text" id="stokBarang" class="form-control" onkeypress="return isNumberKey(event);" maxLength="5" autocomplete="off">
		                                <label class="form-label">Jumlah Barang</label>
		                            </div>
		                        </div>
		            		</div>
							<div class="col-lg-1">
								<span id="satuanPertama">KG</span>
							</div>
							<div class="col-lg-1">
								<span class="btn-sm bg-deep-purple"><i style="font-size: 20px" class="fa fa-arrows-h" aria-hidden="true"></i></span>
							</div>
							<div class="col-lg-2">
								<div class="form-group form-float">
									<div class="form-line">
										<input type="text" id="stokIndukBarang" class="form-control" onkeypress="return isNumberKey(event);" maxLength="5" autocomplete="off">
										<label class="form-label">Jumlah Barang</label>
									</div>
								</div>
							</div>
							<div class="col-lg-1">
								<span id="satuanKedua">KG</span>
							</div>
		            	</div>
						<div class="row">
							<div class="col-lg-7">
								<div class="form-group form-float">
									<div class="form-line">
										<textarea rows="2" class="form-control" id="catatan" cols=""></textarea>
										<label class="form-label">Catatan</label>
									</div>
								</div>
							</div>
						</div>
		            	<div class="row">
		            		<div class="col-lg-12 align-center">
		            			<hr>
		            			<button type="button" id="simpanKeluarMasukBarang" class="btn btn-sm bg-deep-purple">Simpan</button>
		            		</div>
		            	</div>
		            </div>
		       </div>
		  </div>
        </div>
        <div class="row clearfix">
            
            
        </div>
    </div>
</section>
 	
 	<jsp:include page="../general/bottom_import.jsp"></jsp:include>
 	
<script type="text/javascript">
	$(document).ready(function(){
		
		
		var configBd = { 
				"responsive": true,
				"aLengthMenu": [[5, 10, 25], [5, 10, 25]],
				"iDisplayLength":5,
		       	"bLengthChange": true,
		       	/* "bFilter":false, */
		       	"bProcessing": true,
		       	"bServerSide": true,
		       	"sAjaxSource": "<%= ApplConstant.URL_LIST_BD_DATATABLE %>",
		       	"sAjaxDataProp": "data.content",
		       	"sServerMethod": "POST",
			   "fnCreatedRow": function (row, data, index) {
		            $( row ).find('td:eq(0)').html(index + 1);
		        },
		       	"aoColumns": [
		       		{ "mData": null, "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"5%" },
		       		{ "mData": "id", "sClass":"dt-head-center dt-body-center vertical-middle hidden","sWidth":"20%"},
		            { "mData": "namaBd", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"20%"},
		            { "mData": "deskripsiBd", "sClass":"dt-head-center dt-body-left vertical-middle","sWidth":"50%"}
		        ],
			};
			
			oTable = $("#table-list-barang").dataTable();
			oTableBd = $("#table-list-bd").dataTable();
			
			$("#searchBarang").click(function(){
				
				var namaBd = "";
				var config = { 
						"responsive": true,
						"aLengthMenu": [[5, 10, 25], [5, 10, 25]],
						"iDisplayLength":5,
				       	"bLengthChange": true,
				       	/* "bFilter":false, */
				       	"bProcessing": true,
				       	"bServerSide": true,
				       	"sAjaxSource": "<%= ApplConstant.URL_LIST_BARANG_DATATABLE+"?namaBd=" %>"+namaBd,
				       	"sAjaxDataProp": "data.content",
				       	"sServerMethod": "POST",
					   "fnCreatedRow": function (row, data, index) {
				            $( row ).find('td:eq(0)').html(index + 1);
				        },
				       	"aoColumns": [
				       		{ "mData": null, "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"5%" },
				       		{ "mData": "id", "sClass":"dt-head-center dt-body-center vertical-middle hidden","sWidth":"20%"},
				            { "mData": "kodeBarang", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"20%"},
				            { "mData": "namaBarang", "sClass":"dt-head-center dt-body-left vertical-middle","sWidth":"50%"},
				            { "mData": "stok", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"10%"},
                            { "mData": "satuan", "sClass":"dt-head-center dt-body-center vertical-middle hidden","sWidth":"10%"},
                            { "mData": "indukSatuan", "sClass":"dt-head-center dt-body-center vertical-middle hidden","sWidth":"10%"},
                            { "mData": "nominalIndukSatuan", "sClass":"dt-head-center dt-body-center vertical-middle hidden","sWidth":"10%"},
				        ],
					};
				
				oTable.fnClearTable();
          		oTable.fnDestroy();
          	  	oTable = $('#table-list-barang').dataTable(config);
          	  	$("#table-list-barang").css("width","100%");
			});
			
			$("#table-list-barang tbody").on('click', 'tr', function(e){
				var idBarang = $(this).context.children[1].innerText;
				var kodeBarang = $(this).context.children[2].innerText;
				var NamaBarang = $(this).context.children[3].innerText;
                var stok = $(this).context.children[4].innerText;
				var satuan	=	$(this).context.children[5].innerText;
                var indukSatuan	=	$(this).context.children[6].innerText;
                var nominalIndukSatuan	=	$(this).context.children[7].innerText;
				$("#idBarang").val(idBarang);
				$("#kodeBarang").removeAttr("disabled");
				$("#kodeBarang").focus();
				$("#kodeBarang").val(kodeBarang);
				$("#kodeBarang").attr("disabled","disabled");
				$("#namaBarang").removeAttr("disabled");
				$("#namaBarang").focus();
				$("#namaBarang").val(NamaBarang);
				$("#namaBarang").attr("disabled","disabled");
				$("#btnTutupListBarang").click();
				$("#satuanPertama").html(satuan);
                $("#satuanKedua").html(indukSatuan);
                $("#nominalIndukSatuan").val(nominalIndukSatuan);
                $("#stokBarangKeseluruhan").val(stok);
				
			});
						
			$("#simpanKeluarMasukBarang").click(function(e){
				e.preventDefault();

                var stokKeseluruhanBarang = $("#stokBarangKeseluruhan").val();
                var stokBarang = $("#stokBarang").val();
				if (parseFloat(stokBarang) > parseFloat(stokKeseluruhanBarang)){$("#stokBarang").focus();return false;}
				if ($("#kodeBarang").val().length < 0) {Swal.fire({icon: 'error',title: 'Oops...',text: "Pilih Barang Terlebih dahulu"});}
                if ($("#stokBarang").val().length < 0) { Swal.fire({icon: 'error',title: 'Oops...',text: "Masukkan Jumlah Barang"});}
                if ($("#penerima").val().length < 0) { Swal.fire({icon: 'error',title: 'Oops...',text: "Masukkan Nama Penerima"});}

				Swal.fire({
			      	  title: 'Are you sure?',
			      	  icon: 'warning',
			      	  showCancelButton: true,
			      	  confirmButtonColor: '#3085d6',
			      	  cancelButtonColor: '#d33',
			      	  confirmButtonText: 'Simpan',
			      	  cancelButtonText: 'Batal'
			      	}).then((result) => {
			      	  if (result.isConfirmed) {
			      		$(".page-loader-wrapper").css("display","");
				    	
				    	var datas = {
				    			"idBarang": $("#idBarang").val(),
				    			"idBd": $("#idBd").val(),
			      				"jumlahBarang" : $("#stokBarang").val(),
			      				"penerima":$("#penerima").val(),
			      				"catatan":$("#catatan").val(),
								"idBagian":$("#bagian").val()
			                }
			      		
			      		$.ajax({
			              	type: 'POST',
			              	contentType: "application/json",
			              	url: "<%= ApplConstant.URL_KELUAR_MASUK_SAVE%>",
			              	data: JSON.stringify(datas),
			                  dataType: 'json',
			                  cache: false,
			              	success : function(){
			              		
			              	  $(".page-loader-wrapper").css("display","none");
			              		Swal.fire({
			            			  icon: 'success',
			            			  text: "Berhasil Order"
			            		});
			              		
			              		$("#idBarang").val("");
			              		$("#idBd").val("");
			              		$("#kodeBarang").val("");
			              		$("#kodeBd").val("");
			              		$("#namaBarang").val("");
			              		$("#penerima").val("");
			              		$("#stokBarang").val("");
			              		$("#catatan").val("");
			              		$("#cancelBd").click();
			              		
			              	},
			              	error : function(e){
			              		$(".page-loader-wrapper").css("display","none");
			              		 
			              			Swal.fire({
				              			  icon: 'error',
				              			  title: 'Oops...',
				              			  text: "Error "
				              			});
			              			
			              	}
			              });
			      	  }
			    });
			});
			
			$("#searchBd").click(function(){
				oTableBd.fnClearTable();
				oTableBd.fnDestroy();
				oTableBd = $('#table-list-bd').dataTable(configBd);
          	  	$("#table-list-bd").css("width","100%");
			});

			$('#cancelBd').click(function(){
				$("#idBd").val("");
				$("#bd").val("");
				$("#bd").blur();
				$('#cancelBd').addClass("hidden");
			});
			
			$("#table-list-bd tbody").on('click', 'tr', function(e){
				var idBd = $(this).context.children[1].innerText;
				var kodeBd = $(this).context.children[2].innerText;
				$("#idBd").val(idBd);
				$("#bd").removeAttr("disabled");
				$("#bd").focus();
				$("#bd").val(kodeBd);
				$("#bd").attr("disabled","disabled");
				$("#btnTutupListBd").click();
				$('#cancelBd').removeClass("hidden");
			});

			$("#stokBarang").keyup(function () {
			    var stokKeseluruhanBarang = $("#stokBarangKeseluruhan").val();
				var stokBarang = $(this).val();
				var nominalIndukSatuan = $("#nominalIndukSatuan").val();
				var rumus = parseFloat(stokBarang)/parseFloat(nominalIndukSatuan);
				rumus = isNaN(rumus) ? 0 : rumus;
				if (rumus > parseFloat(stokKeseluruhanBarang)){
                    Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: "Jumlah barang yang anda masukkan melebihi stok yang ada"
                    });
                    $(this).focus();
                    $(this).css({"background-color":"#e81515","color":"#ffffff"});
                    $("#stokIndukBarang").css({"background-color":"#e81515","color":"#ffffff"});
				}else {
                    $(this).css({"background-color":"#ffffff","color":"#555"});
                    $("#stokIndukBarang").css({"background-color":"#ffffff","color":"#555"});
				}
				$("#stokIndukBarang").focus();
				$("#stokIndukBarang").val(rumus);
				$(this).focus();
            });

			$("#stokIndukBarang").keyup(function () {
                var stokKeseluruhanBarang = $("#stokBarangKeseluruhan").val();
				var stokIndukBarang = $(this).val();
				var nominalIndukSatuan = $("#nominalIndukSatuan").val();
				var rumus = parseFloat(stokIndukBarang)*parseFloat(nominalIndukSatuan);
				rumus = isNaN(rumus) ? 0 : rumus;
                if (rumus > parseFloat(stokKeseluruhanBarang)){
                    Swal.fire({
                        icon: 'warning',
                        title: 'Oops...',
                        text: "Jumlah barang yang anda masukkan melebihi stok yang ada"
                    });
                    $(this).focus();
                    $(this).css({"background-color":"#e81515","color":"#ffffff"});
                    $("#stokBarang").css({"background-color":"#e81515","color":"#ffffff"});
                }else {
                    $(this).css({"background-color":"#ffffff","color":"#555"});
                    $("#stokBarang").css({"background-color":"#ffffff","color":"#555"});
                }
				$("#stokBarang").focus();
				$("#stokBarang").val(rumus);
				$(this).focus();
			});


	});
	
	function isNumberKey(evt){
		var charCode = (evt.which) ? evt.which : evt.keyCode
		 		    if(
		 		    	!(charCode > 31 && (charCode < 48 || charCode > 57))
		 		    	|| (charCode == 46)
		 		    ){
		 		    	return true;	
		 		    }else{
		 		    	
		 		    	return false;
		 		    }
		}
</script>
	
</body>
</html>