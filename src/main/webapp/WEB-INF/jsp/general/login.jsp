<%--
  Created by IntelliJ IDEA.
  User: faiq
  Date: 25/07/2022
  Time: 21:59:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ page import="com.faiq.eka.constant.*" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String resourceLink = "/"; %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Muh Nailur Rohman Faiq">
    <meta name="keyword" content="Dashboard">
    <link rel="shortcut icon" href="<%=resourceLink%>assets/img/eka.jpg">

    <title>EKA STOKIS APPS</title>
    <link href="<%=resourceLink%>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <%--<link href="<%=resourceLink%>css/bootstrap-reset.css" rel="stylesheet">--%>
    <link href="<%=resourceLink%>plugins/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<%=resourceLink%>css/style-login.css" rel="stylesheet">
    <%--<link href="<%=resourceLink%>css/custom-style.css" rel="stylesheet">--%>
    <%--<link href="<%=resourceLink%>css/style-responsive.css" rel="stylesheet" />--%>

    <link rel="stylesheet" type="text/css" href="<%=resourceLink%>assets/gritter/css/jquery.gritter.css" />

    <script src="<%=resourceLink%>plugins/jquery/jquery.js"></script>
    <script src="<%=resourceLink%>plugins/bootstrap/js/bootstrap.min.js"></script>
</head>
<body class="img js-fullheight" style="background-image: url(<%=resourceLink%>img/background.jpg);">
<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 text-center mb-5">
                <h5 class="heading-section">SELAMAT DATANG</h5>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-6 col-lg-4">
                <div class="login-wrap p-0">
                    <h3 class="mb-4 text-center">EKA STOKIS APPS</h3>
                    <spring:url value="<%= ApplConstant.URL_LOGIN %>" var="loginUrl" />
                    <form:form class="cmxform signin-form" method="post" modelAttribute="loginForm" action="${loginUrl}">
                        <%--<form action="#" class="signin-form">--%>
                        <spring:bind path="username">
                            <div class="form-group">
                                <form:input path="username" type="text" class="form-control" placeholder="User ID" autofocus="autofocus" onkeyup='this.value = this.value.toUpperCase()' />
                            </div>
                            <form:errors path="username" class="control-label text-danger" />
                        </spring:bind>
                        <spring:bind path="password">
                            <div class="form-group">
                                <form:input path="password" id="password-field" type="password" class="form-control" placeholder="Password" onkeyup='this.value = this.value.toUpperCase()' />
                                <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                            </div>
                            <form:errors path="password" class="control-label text-danger" />
                        </spring:bind>
                        <div class="form-group">
                            <button type="submit" class="form-control btn btn-primary submit px-3" id="login-button">Login</button>
                        </div>
                        <%--</form>--%>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="<%=resourceLink%>js/main-login.js"></script>

<script type="text/javascript" src="<%=resourceLink%>assets/gritter/js/jquery.gritter.js"></script>
<script src="<%=resourceLink%>assets/gritter/js/gritter.js" type="text/javascript"></script>
<script>
    $(function(){

        <c:if test="${not empty donthaveright}">
        var dataright = "Anda tidak mempunyai wewenang untuk menggunakan applikasi ini";
        $.gritter.add({
            title: 'Privilege Error!',
            text: dataright,
            image: '<%=resourceLink%>img/Favicon2.png',
            sticky: false,
            time: '',
            class_name: 'gritter-light'
        });
        </c:if>

        <c:if test="${not empty error}">
        var data = 'Salah user id atau password!';
        $.gritter.add({
            title: 'Login Error!',
            text: data,
            image: '<%=resourceLink%>img/Favicon2.png',
            sticky: false,
            time: '',
            class_name: 'gritter-light'
        });
        </c:if>

    })
</script>
</body>
</html>