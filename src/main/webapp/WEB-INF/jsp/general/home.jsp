<!DOCTYPE html>
<%@page import="com.faiq.eka.constant.ApplConstant"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication property="principal" var="user" />
<% String resourceLink = "/"; %>

<html lang="en">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="muh nailur rohman faiq">
    <meta name="keyword" content="Dashboard">

    <title>EKA STOKIS APPS</title>
    <jsp:include page="header_import.jsp"></jsp:include>
	<style>
		#backButton {
			border-radius: 4px;
			padding: 8px;
			border: none;
			font-size: 16px;
			background-color: #2eacd1;
			color: white;
			position: absolute;
			top: 30px;
			right: 30px;
			cursor: pointer;
		}
	</style>
</head>
<body class="theme-blue-grey">

<jsp:include page="header.jsp"></jsp:include>
<jsp:include page="menu.jsp">
	<jsp:param name="menuact" value="" />
</jsp:include>
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>DASHBOARD</h2>
        </div>

        <!-- Widgets -->
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            	<a href="<%= ApplConstant.URL_MASTER_BARANG %>"" style="text-decoration: none;">
	                <div class="info-box-3 bg-pink hover-zoom-effect">
	                    <div class="icon">
	                    	<i class="fa fa-archive" aria-hidden="true"></i>
	                        <!-- <i class="material-icons">playlist_add_check</i> -->
	                    </div>
	                    <div class="content">
	                        <div class="text">Total Barang</div>
	                        <%
	                        	Long totalBarang = (Long) request.getAttribute("totalBarang");
	                        %>
	                        <div class="number"><%=totalBarang %></div>
	                    </div>
	                </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a href="<%= ApplConstant.URL_PENERIMA %>" style="text-decoration: none;">
	                <div class="info-box-3 bg-green hover-zoom-effect">
	                    <div class="icon">
	                        <i class="fa fa-user-plus" aria-hidden="true"></i>
	                    </div>
	                    <div class="content">
	                        <div class="text">Penerima Barang</div>
	                        <%
	                        	Integer totalPenerima = (Integer) request.getAttribute("totalPenerima");
	                        %>
	                        <div class="number"><%=totalPenerima %></div>
	                    </div>
	                </div>
                </a>

            </div>
            <!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-orange hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">person_add</i>
                    </div>
                    <div class="content">
                        <div class="text">NEW VISITORS</div>
                        <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div> -->
        </div>
        <!-- #END# Widgets -->
        <div class="row clearfix">
			<div class="row">
				<div class="col-lg-8">
					<div id="barSideChart" style="height: 350px; width: 100%;"></div>
					<button class="btn invisible" id="backButton">&lt; Back</button>
				</div>
				<div class="col-lg-4">
					<div id="lineChart" class="invisible" style="height: 350px; width: 100%;"></div>
				</div>
			</div>
            
        </div>
    </div>
</section>
 	
 	<jsp:include page="bottom_import.jsp"></jsp:include>

<script>
	$(document).ready(function () {
        var currentTime = new Date();
        var currentYear = currentTime.getFullYear();
		chartTotalPengeluranPerTahun(currentYear);

        $("#backButton").click(function() {
            $(this).toggleClass("invisible");
            $("#lineChart").addClass("invisible");
            chartTotalPengeluranPerTahun(currentYear);
        });
    });

	function chartTotalPengeluranPerTahun(tahun){

            var datas = {
                "tahun"     : tahun
            }

            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: "<%= ApplConstant.URL_DASHBOARD_TOTAL_PENGELUARAN%>",
                data: JSON.stringify(datas),
                dataType: 'json',
                cache: false,
                success : function(data){
                    $("body").removeClass("loading");

                    var dataPoints = [];

                    var chart3 = new CanvasJS.Chart("barSideChart", {
                        theme: "light2", //"dark2", // "light1", "light2", "dark1"
                        animationEnabled: true,
                        title: {
                            text: "Total Pengeluaran Tahun "+tahun
                        },
                        exportEnabled: true,
                        axisX: {
                            margin: 10,
                            labelPlacement: "inside",
                            tickPlacement: "inside",
                            labelFontColor: "black",
                        },
                        axisY: {
                            title: "Total Pengeluaran",
                            prefix: "Rp "
                        },
                        data: [{
                            type: "column",
                            click: onClickTotalPerBulan(tahun),
                            showInLegend: true,
                            legendMarkerColor: "black",
                            legendText: "Bulan",
                            dataPoints: dataPoints,
                            toolTipContent: "{label} : Rp {y}"
                        }]
                    });

                    for (var i=0;i<data.length;i++){
                        dataPoints.push({y:parseFloat(data[i].totalPengeluaran),label:data[i].bulan})
                    }

                    chart3.render();
                },
                error : function(e){
                    $("body").removeClass("loading");
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: e.status
                    });

                }
            });

	}

	function onClickTotalPerBulan(tahun) {
        var totalPerbulan = function(e) {
            var bulan = e.dataPoint.label;
            var datas = {
                "tahun"     : tahun,
				"bulan"		: bulan
            }

            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: "<%= ApplConstant.URL_DASHBOARD_TOTAL_PENGELUARAN_BY_BULAN%>",
                data: JSON.stringify(datas),
                dataType: 'json',
                cache: false,
                success : function(data){
                    $("#backButton").toggleClass("invisible");
                    $("body").removeClass("loading");
                    var dataPoints = [];

                    var chart3 = new CanvasJS.Chart("barSideChart", {
                        theme: "light2", //"dark2", // "light1", "light2", "dark1"
                        animationEnabled: true,
                        zoomEnabled: true,
                        title: {
                            text: "Total Pengeluaran Bulan "+bulan+" "+tahun
                        },
                        exportEnabled: true,
                        axisX: {
                            margin: 10,
                            labelPlacement: "inside",
                            tickPlacement: "inside",
                            labelFontColor: "black",
                        },
                        axisY: {
                            title: "Total Pengeluaran",
                            prefix: "Rp "
                        },
                        data: [{
                            type: "column",
                            showInLegend: true,
                            legendMarkerColor: "grey",
                            click: totalPengeluaranPerbarang(bulan,tahun),
                            legendText: "Nama Barang",
                            dataPoints: dataPoints,
                            toolTipContent: "{label} : Rp {y}"
                        }]
                    });

                    for (var i=0;i<data.length;i++){
                        dataPoints.push({y:parseFloat(data[i].totalPengeluaran),label:data[i].namaBarang})
                    }

                    chart3.render();
                },
                error : function(e){
                    $("body").removeClass("loading");
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: e.status
                    });

                }
            });
        }
        return totalPerbulan;
    }
    
    function totalPengeluaranPerbarang(bulan,tahun) {
        var totalPerbarang = function(e) {
            var namaBarang = e.dataPoint.label;
            var datas = {
                "tahun"     : tahun,
                "bulan"		: bulan,
				"namaBarang": namaBarang
            }

            $.ajax({
                type: 'POST',
                contentType: "application/json",
                url: "<%= ApplConstant.URL_DASHBOARD_TOTAL_PENGELUARAN_BY_PERBARANG%>",
                data: JSON.stringify(datas),
                dataType: 'json',
                cache: false,
                success : function(data){
                    $("#lineChart").removeClass("invisible");
					var satuan = data[0].satuan;
                    $("body").removeClass("loading");
                    var dataPoints = [];

                    var chart3 = new CanvasJS.Chart("lineChart", {
                        theme: "light2", //"dark2", // "light1", "light2", "dark1"
                        animationEnabled: true,
                        zoomEnabled: true,
                        title: {
                            text: "Total Pengeluaran "+namaBarang+" "+bulan+" "+tahun
                        },
                        exportEnabled: true,
                        axisX: {
                            margin: 10,
                            labelPlacement: "inside",
                            tickPlacement: "inside",
                            labelFontColor: "black",
                        },
                        axisY: {
                            title: "Total Pengeluaran",
                            suffix: satuan
                        },
                        data: [{
                            type: "line",
                            showInLegend: true,
                            legendMarkerColor: "grey",
                            legendText: "Tanggal",
                            dataPoints: dataPoints,
                            toolTipContent: "{label} : {y} {satuan}"
                        }]
                    });

                    for (var i=0;i<data.length;i++){
                        dataPoints.push({y:parseFloat(data[i].jumlahBarang),label:data[i].tanggalOrder,satuan:data[i].satuan})
                    }

                    chart3.render();
                },
                error : function(e){
                    $("body").removeClass("loading");
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: e.status
                    });

                }
            });
        }
        return totalPerbarang;
    }
</script>
	
</body>
</html>