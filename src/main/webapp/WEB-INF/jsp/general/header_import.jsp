<% String resourceLink = "/"; %>
<!-- Favicon-->
<link rel="icon" href="<%=resourceLink%>assets/img/eka.jpg" type="image/x-icon">

<!-- Google Fonts -->
<!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css"> -->
<!-- Bootstrap Core Css -->
<link href="<%=resourceLink%>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

<!-- Waves Effect Css -->
<link href="<%=resourceLink%>plugins/node-waves/waves.css" rel="stylesheet" />

<!-- Font-Awesome Css -->
<link href="<%=resourceLink%>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

<!-- Animation Css -->
<link href="<%=resourceLink%>plugins/animate-css/animate.css" rel="stylesheet" />

<!-- Morris Chart Css-->
<link href="<%=resourceLink%>plugins/morrisjs/morris.css" rel="stylesheet" />

<!-- Custom Css -->
<link href="<%=resourceLink%>assets/css/materialize.css" rel="stylesheet">
<link href="<%=resourceLink%>assets/css/style.css" rel="stylesheet">

<!-- Bootstrap Select Css -->
<link href="<%=resourceLink%>plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<!-- JQuery DataTable Css -->
<link href="<%=resourceLink%>plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

<!-- Bootstrap DatePicker Css -->
    <link href="<%=resourceLink%>plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />

<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="<%=resourceLink%>assets/css/all-themes.css" rel="stylesheet" />
<!-- Sweetalert Css -->
    <link rel="stylesheet" type="text/css" href="<%=resourceLink%>webjars/sweetalert2/10.12.5/dist/sweetalert2.min.css">
    <%--<link rel="stylesheet" type="text/css" href="<%=resourceLink%>assets/tableexport/dist/css/tableexport.min.js" />--%>
    <link rel="stylesheet" type="text/css" href="<%=resourceLink%>plugins/datatable/Buttons-2.2.3/css/buttons.bootstrap.min.css" />
<style>
	.loading-modal {
		display:    none;
		position:   fixed;
		z-index:    1000;
		top:        0;
		left:       0;
		height:     100%;
		width:      100%;
		background: rgba( 255, 255, 255, .8 ) 
					url('/img/loading.gif') 
					50% 50% 
					no-repeat;
	}
	body.loading .loading-modal {
		overflow: hidden;   
	}
	body.loading .loading-modal {
		display: block;
	}
</style>