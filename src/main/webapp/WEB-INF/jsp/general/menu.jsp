<!DOCTYPE html>
<% String resourceLink = "/"; %>
<%@page import="com.faiq.eka.util.Strings"%>
<%@page import="com.faiq.eka.constant.ApplConstant" %>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%
    String menuact = request.getParameter("menuact");
    String classDashboard = "";
    if(Strings.isNoE(menuact)) classDashboard = "active";
%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authentication property="principal.authorities" var="authorities" />
<c:set var = "isAdmin" value = "${false}"/>
<c:set var = "isDataEntry" value = "${false}"/>
<c:set var = "isViewer" value = "${false}"/>
<c:forEach items="${authorities}" var="authority" varStatus="vs">
    <c:choose>
        <c:when test="${authority.authority == 'Administrator'}">
            <c:set var = "isAdmin" value = "${true}"/>
        </c:when>
        <c:when test="${authority.authority == 'Data Entry'}">
            <c:set var = "isDataEntry" value = "${true}"/>
        </c:when>
        <c:otherwise>
            <c:set var = "isViewer" value = "${true}"/>
        </c:otherwise>
    </c:choose>
</c:forEach>
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="<%=resourceLink%>assets/img/eka.jpg" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><b>Eka Zulfatul Hikmah</b></div>
                <div class="email">ekazulfa08@gmail.com</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="fa fa-caret-down" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></i>
                    <ul class="dropdown-menu pull-right">
                        <%--<li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>--%>
                        <li role="separator" class="divider"></li>
                        <spring:url value="/logout" var="logoutUrl" />
                        <li><a href="${logoutUrl }"><i class="fa fa-user"></i> &nbsp;&nbsp;Sign Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li class="<%=classDashboard%>">
                    <a href="/">
                        <i class="fa fa-home fa-lg m-t-10" aria-hidden="true"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="
						<% if(menuact.equalsIgnoreCase("masterBd")
							||  menuact.equalsIgnoreCase("masterBarang")
							) {%> active <% } %>
						">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="fa fa-object-group fa-lg m-t-10" aria-hidden="true"></i>
                        	<span>Master</span>
                        </a>
                        <ul class="ml-menu">
	                        <li class="<% if(menuact.equalsIgnoreCase("masterBd")) {%> active <% } %>">
			                    <a href="<%= ApplConstant.URL_MASTER_BD %>">Master BD</a>
			                </li>
			                <li class="<% if(menuact.equalsIgnoreCase("masterBarang")) {%> active <% } %>">
			                    <a href="<%= ApplConstant.URL_MASTER_BARANG %>">Master Barang</a>
			                </li>
                        </ul>
                </li>
                <c:if test="${isDataEntry}">
                <li class="
						<% if(menuact.equalsIgnoreCase("keluarMasukBarang")
							||  menuact.equalsIgnoreCase("realisasiManual")
							||  menuact.equalsIgnoreCase("daftarKeluarMasukBarang")
							||  menuact.equalsIgnoreCase("daftarRealisasiManual")
							) {%> active <% } %>
						">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="fa fa-calendar-o fa-lg m-t-10" aria-hidden="true"></i>
                        	<span>Transaksi</span>
                        </a>
                        <ul class="ml-menu">
	                        <li class="<% if(menuact.equalsIgnoreCase("keluarMasukBarang")) {%> active <% } %>">
			                    <a href="<%= ApplConstant.URL_KELUAR_MASUK %>">Keluar Masuk Barang</a>
			                </li>
			                <li class="<% if(menuact.equalsIgnoreCase("daftarKeluarMasukBarang")) {%> active <% } %>">
			                    <a href="<%= ApplConstant.URL_DAFTAR_KELUAR_MASUK %>">Daftar Keluar Masuk Barang</a>
			                </li>
			                <%--<li class="<% if(menuact.equalsIgnoreCase("realisasiManual")) {%> active <% } %>">--%>
			                    <%--<a href="<%= ApplConstant.URL_REALISASI_MANUAL %>">Realisasi Manual</a>--%>
			                <%--</li>--%>
			                <%--<li class="<% if(menuact.equalsIgnoreCase("daftarRealisasiManual")) {%> active <% } %>">--%>
			                    <%--<a href="<%= ApplConstant.URL_DAFTAR_REALISASI_MANUAL %>">Daftar Realisasi Manual</a>--%>
			                <%--</li>--%>
                        </ul>
                </li>
                </c:if>
                <li class="
						<% if(menuact.equalsIgnoreCase("stokOpname")
							||  menuact.equalsIgnoreCase("laporanPerBd")
							||  menuact.equalsIgnoreCase("laporanHarian")
							||  menuact.equalsIgnoreCase("perBarang")
							||  menuact.equalsIgnoreCase("rekapan")
							) {%> active <% } %>
						">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="fa fa-file-text-o fa-lg m-t-10" aria-hidden="true"></i>
                        	<span>Laporan</span>
                        </a>
                        <ul class="ml-menu">
                            <c:if test="${isDataEntry}">
                                <li class="<% if(menuact.equalsIgnoreCase("stokOpname")) {%> active <% } %>">
                                    <a href="<%= ApplConstant.URL_LAPORAN_STOK_OPNAME %>">Stok Opname</a>
                                </li>
                            </c:if>
			                <li class="<% if(menuact.equalsIgnoreCase("laporanPerBd")) {%> active <% } %>">
			                    <a href="<%= ApplConstant.URL_LAPORAN_PER_BD %>">Laporan Per BD</a>
			                </li>
			                <li class="<% if(menuact.equalsIgnoreCase("laporanHarian")) {%> active <% } %>">
			                    <a href="<%= ApplConstant.URL_LAPORAN_HARIAN %>">Laporan Harian</a>
			                </li>
                            <c:if test="${isDataEntry}">
                                <li class="<% if(menuact.equalsIgnoreCase("perBarang")) {%> active <% } %>">
                                    <a href="<%= ApplConstant.URL_LAPORAN_PER_BARANG %>">Per Barang</a>
                                </li>
                            </c:if>
                            <li class="<% if(menuact.equalsIgnoreCase("rekapan")) {%> active <% } %>">
                                <a href="<%= ApplConstant.URL_LAPORAN_REKAPAN %>">Rekapan</a>
                            </li>
                        </ul>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2021<a href="javascript:void(0);"> Kafa Developer</a>.
            </div>
            <div class="version">
                <b>Version: </b> 1.0.0
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>