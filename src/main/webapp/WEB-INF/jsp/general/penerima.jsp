<!DOCTYPE html>
<%@page import="com.faiq.eka.constant.ApplConstant"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String resourceLink = "/"; %>

<html lang="en">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="muh nailur rohman faiq">
    <meta name="keyword" content="Dashboard">

    <title>EKA STOKIS APPS</title>
    <jsp:include page="../general/header_import.jsp"></jsp:include>
</head>
<body class="theme-blue-grey">

<jsp:include page="../general/header.jsp"></jsp:include>
<jsp:include page="../general/menu.jsp">
	<jsp:param name="menuact" value="" />
</jsp:include>
<section class="content">
<jsp:include page="../lookup/lookup-daftar-barang-penerima.jsp"></jsp:include>
    <div class="container-fluid">
        <ol class="breadcrumb breadcrumb-bg-blue">
            <li><a href="javascript:void(0);"><i class="fa fa-home fa-lg m-t-10" aria-hidden="true"></i> Home</a></li>
            <li class="active"><i class="fa fa-archive fa-lg m-t-10" aria-hidden="true"></i> Penerima</li>
        </ol>

        <div class="row clearfix">
            <div class="col-lg-12">
            	<div class="card">
            		<div class="header bg-blue">
                         <h2>
                             Daftar Penerima
                         </h2>
                     </div>
                     <div class="body">
                     	<div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="table-penerima">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Penerima</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                             </table>
                         </div>
                     </div>
            	</div>
            </div>
        </div>
    </div>
</section>
 	
 	<jsp:include page="../general/bottom_import.jsp"></jsp:include>
 	<script>
 		$(document).ready(function(){
 			
 			var config = { 
					"responsive": true,
					"aLengthMenu": [[5, 10, 25], [5, 10, 25]],
					"iDisplayLength":5,
			       	"bLengthChange": true,
			       	/* "bFilter":false, */
			       	"bProcessing": true,
			       	"bServerSide": true,
			       	"sAjaxSource": "<%= ApplConstant.URL_PENERIMA_DATATABLE %>",
			       	"sAjaxDataProp": "data.content",
			       	"sServerMethod": "POST",
				   "fnCreatedRow": function (row, data, index) {
			            $( row ).find('td:eq(0)').html(index + 1);
			        },
			       	"aoColumns": [
			       		{ "mData": null, "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"5%" },
				        { "mData": "penerima", "sClass":"dt-head-center dt-body-left vertical-middle","sWidth":"80%"},
			            { "mData": "penerima", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"15%","mRender":function(data,type,row){
			            	
			 		    	   return "<a href='#!' id='btnDetailPenerima' data='"+data+"' title='Detail' class='col-blue' data-toggle='modal' data-target='#listBarangPenerimaModal'><i class='fa fa-eye' aria-hidden='true'></i></a>"
			 		    	   		  
			 		    	   		  ;
			 		       } },
			        ],
				};
 			
 			oTable = $("#table-penerima").dataTable(config);
 			oTable2 = $("#table-barang-penerima").dataTable();
 			
 			
 			$("#table-penerima tbody").on('click', '#btnDetailPenerima', function(e){
				e.preventDefault();
				var penerima = $(this).attr("data");
				var config2 = { 
						"responsive": true,
						"aLengthMenu": [[5, 10, 25], [5, 10, 25]],
						"iDisplayLength":5,
				       	"bLengthChange": true,
				       	/* "bFilter":false, */
				       	"bProcessing": true,
				       	"bServerSide": true,
				       	"sAjaxSource": "<%= ApplConstant.URL_BARANG_PENERIMA_DATATABLE+"?penerima=" %>"+penerima,
				       	"sAjaxDataProp": "data.content",
				       	"sServerMethod": "POST",
					   "fnCreatedRow": function (row, data, index) {
				            $( row ).find('td:eq(0)').html(index + 1);
				        },
				       	"aoColumns": [
				       		{ "mData": null, "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"5%" },
					        { "mData": "kodeBarang", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"20%"},
					        { "mData": "namaBarang", "sClass":"dt-head-center dt-body-left vertical-middle","sWidth":"60%"},
					        { "mData": "tanggal", "sClass":"dt-head-center dt-body-center vertical-middle","sWidth":"15%"},
				        ],
					};
				
				oTable2.fnClearTable();
          		oTable2.fnDestroy();
          	  	oTable2 = $('#table-barang-penerima').dataTable(config2);
          	  	$('#table-barang-penerima').css("width","100%");
			});
 			
 		});
 		
 		function isNumberKey(evt){
 		    var charCode = (evt.which) ? evt.which : evt.keyCode
 		    if(
 		    	!(charCode > 31 && (charCode < 48 || charCode > 57))
 		    	|| (charCode == 46)
 		    ){
 		    	return true;	
 		    }else{
 		    	
 		    	return false;
 		    }
 		    
 		}
 	</script>
	
</body>
</html>