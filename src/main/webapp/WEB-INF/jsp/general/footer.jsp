<footer class="site-footer ">
	<div class="text-center">
		<strong>2021 &copy; Developed by: <a href="https://www.instagram.com/_faiq.official/" target="_blank">Muh Nailur Rohman Faiq</a></strong>
		<b>Version</b> 0.01              
		<a href="#main-content" class="go-top"><i class="fa fa-angle-up"></i></a>
	</div>
</footer>