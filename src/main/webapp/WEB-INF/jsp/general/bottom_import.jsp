<% String resourceLink = "/"; %>
<!-- js placed at the end of the document so the pages load faster -->

    <!-- Jquery Core Js -->
    <script src="<%=resourceLink%>plugins/jquery/jquery.min.js"></script>
    
    <!-- Bootstrap Core Js -->
    <script src="<%=resourceLink%>plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Select Plugin Js -->
    <script src="<%=resourceLink%>plugins/bootstrap-select/js/bootstrap-select.js"></script>
 
    <!-- Slimscroll Plugin Js -->
    <script src="<%=resourceLink%>plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
    
    <!-- Waves Effect Plugin Js -->
    <script src="<%=resourceLink%>plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<%=resourceLink%>plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<%=resourceLink%>plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="<%=resourceLink%>plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="<%=resourceLink%>plugins/raphael/raphael.min.js"></script>
    <script src="<%=resourceLink%>plugins/morrisjs/morris.js"></script>

    <!-- Custom Js -->
    <script src="<%=resourceLink%>assets/js/admin.js"></script>

    <!-- Demo Js -->
    <script src="<%=resourceLink%>assets/js/demo.js"></script>
    
    <!-- Jquery DataTable Plugin Js -->
    <script src="<%=resourceLink%>plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<%=resourceLink%>plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <%--<script src="<%=resourceLink%>plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>--%>
    <script src="<%=resourceLink%>plugins/datatable/Buttons-2.2.3/js/dataTables.buttons.min.js"></script>
    <script src="<%=resourceLink%>plugins/datatable/Buttons-2.2.3/js/buttons.html5.min.js"></script>
    <script src="<%=resourceLink%>plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<%=resourceLink%>plugins/datatable/JSZip-2.5.0/jszip.min.js"></script>
    <script src="<%=resourceLink%>plugins/datatable/pdfmake-0.1.36/pdfmake.min.js"></script>
    <script src="<%=resourceLink%>plugins/datatable/Buttons-2.2.3/js/buttons.print.min.js"></script>
    <script src="<%=resourceLink%>plugins/datatable/pdfmake-0.1.36/vfs_fonts.js"></script>
    <script src="<%=resourceLink%>plugins/datatable/Buttons-2.2.3/js/buttons.colVis.min.js"></script>
    
    <!-- Bootstrap Datepicker Plugin Js -->
    <script src="<%=resourceLink%>plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    
    <!-- SweetAlert Plugin Js -->
    <script src="<%=resourceLink%>webjars/sweetalert2/10.12.5/dist/sweetalert2.min.js"></script>
    <script type="text/javascript" src="<%=resourceLink%>assets/canvasjs-3.7.14/canvasjs.min.js"></script>
    <%--<script type="text/javascript" src="<%=resourceLink%>assets/canvasjs-3.7.14/jquery.canvasjs.min.js"></script>--%>
    <%--<script type="text/javascript" src="<%=resourceLink%>assets/html2canvas/html2canvas.min.js"></script>--%>
    <script type="text/javascript" src="<%=resourceLink%>assets/jspdf/jspdf.min.js"></script>
    <script type="text/javascript" src="<%=resourceLink%>assets/file-saverjs/FileSaver.min.js"></script>
    <script type="text/javascript" src="<%=resourceLink%>assets/blobjs/Blob.min.js"></script>
    <script type="text/javascript" src="<%=resourceLink%>assets/xlsx/dist/xlsx.core.min.js"></script>
